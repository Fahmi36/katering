<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Validator;

class LandingController extends Controller
{
    function Index()
    {
        return view('landing.index');
    }
    function Login()
    {
        return view('auth.login');
    }
    function Daftar()
    {
        return view('auth.register');
    }
    public function ActionLogin(Request $request)
    {
        $response = array();
		$datalogin = $request->only('email','password');
		$input = $request->all();
		$message = [
			'email' => 'required',
			'password' => 'required',
            'captcha' => 'required|captcha'
		];
		$validator = Validator::make($input,$message);
		if ($validator->fails()) {
			$response['code'] = 400;
			$response['message'] = $validator->errors()->first('*');
            $response['captcha'] = captcha_img();
		}else{
            Auth::attempt($datalogin);
            if (Auth::check()) {
                $response['code'] = 200;
                $response['message'] = 'Berhasil Login';
            }else{
                $response['code'] = 400;
                $response['message'] = 'username atau password salah';
                $response['captcha'] = captcha_img();
            }
        }
        return response()->json($response, $response['code']);
    }
    function ActionRegister(Request $request)
    {
        $response = array();
		$input = $request->all();
		$message = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
			'password' => 'required',
            'captcha' => 'required|captcha'
		];
		$validator = Validator::make($input,$message);
		if ($validator->fails()) {
			$response['code'] = 400;
			$response['message'] = $validator->errors()->first('*');
            $response['captcha'] = captcha_img();
		}else{
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'ulang_password' => $request->password,
                'status'=>'0',
                'role'=>'1',
            ];
            $user = User::create($data);
            if ($user) {
                $response['code'] = 200;
                $response['message'] = 'Berhasil Register';
            }else{
                $response['code'] = 400;
                $response['message'] = 'Server sedang sibuk';
                $response['captcha'] = captcha_img();
            }
        }
        return response()->json($response, $response['code']);
    }
    function ReloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
