<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Food;

class FoodController extends Controller
{
    public function index(Type $var = null)
    {
        //get view list data
        # code...
    }
    public function create(Type $var = null)
    {
        //get view create
        # code...
    }
    
    public function store(Type $var = null)
    {
        //post insert
        # code...
    }
    public function show(Type $var = null)
    {
        //get view detail
        # code...
    }
    public function edit(Type $var = null)
    {
        //get view edit detail
        # code...
    }
    public function update(Type $var = null)
    {
        //put update data
        # code...
    }
    
    public function destoy(Type $var = null)
    {
        //delete 
        # code...
    }
}
