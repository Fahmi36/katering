
        <div class="app-sidebar sidebar-shadow">
            <div class="app-header__logo">
                <!-- <div class="logo-src"></div> -->
                <span>KATERING</span>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button"
                        class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="scrollbar-sidebar">
                <div class="app-sidebar__inner">
                    <ul class="vertical-nav-menu">
                        <li class="app-sidebar__heading">PANEL MONITOR</li>
                        <li>
                            <a href="#" class="mm-active">
                                <i class="metismenu-icon pe-7s-graph3"></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="app-sidebar__heading">Makanan</li>
                        <li>
                            <a href="#" class="mm-active">
                                <i class="metismenu-icon pe-7s-drawer"></i>
                                Tambah Makanan
                            </a>
                        </li>
                        <li>
                            <a href="#" class="mm-active">
                                <i class="metismenu-icon pe-7s-box1"></i>
                                Selesai
                            </a>
                        </li>
                        <li class="app-sidebar__heading">SISTEM</li>
                        <li>
                            <a href="#" class="mm-active">
                                <i class="metismenu-icon pe-7s-refresh-2"></i>
                                Tambah Role
                            </a>
                        </li>
                        <li>
                            <a href="#" class="mm-active">
                                <i class="metismenu-icon pe-7s-refresh-2"></i>
                                Edit Profile
                            </a>
                        </li>
                        <li>
                            <a href="#" class="mm-active">
                                <i class="metismenu-icon pe-7s-refresh-2"></i>
                                Tambah Gudang
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="metismenu-icon pe-7s-back-2"></i>
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="app-main__outer">