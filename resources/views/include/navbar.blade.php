<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <div class="app-header header-shadow">
        <div class="app-header__logo">
            <h2 class="logo-src">KATERING</h2>
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                        data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
        <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                    </span>
                </button>
            </span>
        </div>
        <div class="app-header__content">
            <div class="app-header-right">
                <div class="dropdown">
                    <div class="notif-container notif-active dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="notif-ico-border">
                            <i class="fas fa-bell notif-ico"></i>
                            <span class="have-notif">aasasaa</span>
                        </div>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="list-group list-group-flush">
                            <a href="#" class="list-group-item">
                                Permohonan Baru <span class="badge badge-primary float-right">assasa</span>
                            </a>
                            <a href="#" class="list-group-item">Menunggu Keputusan <span class="badge badge-primary float-right">adada</span></a>
                        </div>
                    </div>
                </div>
                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="btn-group">
                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                        class="p-0 btn">
                                        <img width="42" class="rounded-circle" src="" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="widget-content-left">
                                <div class="widget-heading">
                                </div>
                                <div class="widget-subheading">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-btn-lg pr-0 ml-2">
                    <div class="">
                        <a href="">
                            <div class="notif-ico" style="font-size:2em !important;">
                                <i class="metismenu-icon pe-7s-power"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-main">