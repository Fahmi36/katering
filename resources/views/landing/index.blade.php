  @push('css')
      <style>
        .btncustom{
            background: rgba( 255, 255, 255, 0.25 );
            backdrop-filter: blur( 4px );
            -webkit-backdrop-filter: blur( 4px );
            border-radius: 10px;
            border: 1px solid rgba( 255, 255, 255, 0.18 );
        }
        :root {
            --dark-color: hsl(var(--hue), 100%, 9%);
            --light-color: hsl(var(--hue), 95%, 98%);
            --base: hsl(var(--hue), 95%, 50%);
            --complimentary1: hsl(var(--hue-complimentary1), 95%, 50%);
            --complimentary2: hsl(var(--hue-complimentary2), 95%, 50%);
            
            --font-family: "Poppins", system-ui;
            
            --bg-gradient: linear-gradient(
            to bottom,
            hsl(var(--hue), 95%, 99%),
            hsl(var(--hue), 95%, 84%)
            );
        }
        
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        
        html {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        
        body {
            max-width: 1920px;
            min-height: 100vh;
            display: grid;
            place-items: center;
            padding: 2rem;
            font-family: var(--font-family);
            color: var(--dark-color);
            background: var(--bg-gradient);
        }
        
        .card-modern{
            background: linear-gradient(   45deg   , rgb(255 94 94) 0%, rgb(255 117 24) 68%, rgba(255 117 24) 100%);
            padding: 1.25rem;
            display: flex;
            color: #f1f1f1;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            margin-left: 2rem;
            margin-top: 1.5rem;
            overflow: hidden;
            box-shadow: 0 3px 0px 0 rgb(136 144 195 / 20%), 0 5px 10px 0 rgb(37 44 97 / 15%);
            border: none !important;
            border-radius: 10px !important;
        }
        
        strong {
            font-weight: 600;
        }
        
        .overlay {
            width: 100%;
            max-width: 80%;
            max-height: auto;
            padding: 6rem;
            display: flex;
            align-items: center;
            background: rgba(255, 255, 255, 0.375);
            box-shadow: 0 0.75rem 2rem 0 rgba(0, 0, 0, 0.1);
            border-radius: 2rem;
            border: 1px solid rgba(255, 255, 255, 0.125);
        }
        
        .overlay__inner {
            /*   max-width: 36rem; */
        }
        
        .overlay__title {
            font-size: 1.875rem;
            line-height: 2.75rem;
            font-weight: 700;
            letter-spacing: -0.025em;
            margin-bottom: 1rem;
        }
        
        .text-gradient {
            background-image: linear-gradient(
            45deg,
            var(--base) 25%,
            var(--complimentary2)
            );
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            -moz-background-clip: text;
            -moz-text-fill-color: transparent;
        }
        
        .overlay__description {
            font-size: 1rem;
            line-height: 1.75rem;
            margin: 1rem 0;
        }
        
        .overlay__btns {
            width: 100%;
            /*max-width: 40rem;*/
            display: flex;
        }
        
        .overlay__btn {
            width: 50%;
            height: 3rem;
            display: flex;
            justify-content: center;
            align-items: center;
            font-size: 0.875rem;
            font-weight: 600;
            color: var(--light-color);
            background: var(--dark-color);
            border: none;
            border-radius: 0.5rem;
            transition: transform 150ms ease;
            outline-color: hsl(var(--hue), 95%, 50%);
        }
        
        .overlay__btn:hover {
            transform: scale(1.05);
            cursor: pointer;
        }
        
        .overlay__btn--transparent {
            background: transparent;
            color: var(--dark-color);
            border: 2px solid var(--dark-color);
            border-width: 2px;
            margin-right: 0.75rem;
        }
        
        .overlay__btn-emoji {
            margin-left: 0.375rem;
        }
        
        a {
            text-decoration: none;
            color: var(--dark-color);
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        
        @media only screen and (max-width: 1140px) {
            .overlay {
                padding: 8rem 4rem;
            }
        }
        
        @media only screen and (max-width: 840px) {
            body {
                padding: 1.5rem;
            }
            
            .overlay {
                padding: 4rem;
                height: auto;
            }
            
            .overlay__title {
                font-size: 1.25rem;
                line-height: 2rem;
                margin-bottom: 1.5rem;
            }
            
            .overlay__description {
                font-size: 0.875rem;
                line-height: 1.5rem;
                margin-bottom: 2.5rem;
            }
        }
        
        @media only screen and (max-width: 600px) {
            .overlay {
                padding: 1.5rem;
            }
            
            .overlay__btns {
                flex-wrap: wrap;
                margin-top: 20px;
            }
            
            .overlay__btn {
                width: 100%;
                font-size: 0.75rem;
                margin-right: 0;
            }
            
            .overlay__btn:first-child {
                margin-bottom: 1rem;
            }
        }
        
        @media (max-width: 768px) {
            .card-modern{
                margin-left: 0;
            }
            .col-captcha{
                display: contents !important;
            }
            .overlay {
                max-width: 100%;
            }
            .img-logo-ptsp {
                display: none;
            }
        }
        .col-6{
            width: 100%;
            max-width: 50%;
            /*padding: 0 1rem 0 0;*/
        }
        .form-captcha{
            width: 100%;
        }
        .d-flex{
            display: flex;
            margin-top: 10px;
        }
        .form-control {
            width: 77%;
        }
        #captcha{
            background: #fff;
            padding: 0 10px;
            margin-right: 15px;    
        }
        .img-logo-ptsp {
            position: absolute;
            top: 3%;
            right: 12%;
        }
    </style>
  @endpush
  @section('content')
      <div class="overlay">
        <!-- Overlay inner wrapper -->
        <div class="overlay__inner">
            <!-- Title -->
            
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h1 class="overlay__title">
                        Surat Tanda Registrasi Pekerja - Kolektif<br>
                        <span style="font-size:19px;"><span class="text-gradient">STRP</span> berlaku selama <span class="text-gradient">PPKM</span> Darurat di DKI Jakarta yang berlaku bagi:</span>
                    </h1>
                    <!-- Description -->
                    <p class="overlay__description">
                        <strong>Pekerja Sektor Esensial</strong><br>
                        Keuangan dan perbankan, pasar modal, teknologi informasi dan komunikasi, perhotelan non penanganan karantina COVID-19, industry orientasi ekspor.
                    </p>
                    <hr>
                    <p class="overlay__description">
                        <strong>Pekerja Sektor Kritikal</strong><br>
                        Kesehatan, keamanan dan ketertiban masyarakat, penanganan bencana, energi, logistik, transportasi dan distribusi, makanan dan minuman serta penunjangnya, pupuk dan petrokimia, semen dan bahan bangunan, obyek vital nasional, konstruksi (infrastruktur publik), dan utilitas dasar (listrik, air, dan pengelolaan sampah).
                    </p>
                    <p class="overlay__description">
                        <strong class="text-gradient">Baca selengkapnya pada Instruksi Menteri Dalam Negeri Nomor 18 Tahun 2021</strong>
                    </p>
                    <h1 class="overlay__title" style="margin-bottom:0;">
                        <span style="font-size:19px;"><span class="text-gradient">STRP</span> dikecualikan bagi:</span>
                    </h1>
                    <p class="overlay__description" style="margin-top: 0;">
                        Kementerian / Lembaga / instansi pemerintahan baik pusat maupun daerah (TNI, POLRI, Bank Indonesia, OJK, dll), dan urusan mendesak penangan pandemi (tenaga kesehatan, distribusi gas oksigen, pengantaran peti jenazah, dll).
                    </p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="card-modern">
                        <p class="overlay__desc" style="margin:0;">
                            Untuk mengajukan STRP, Anda harus menyiapkan data dan persyaratan sebagai berikut :
                        </p>
                        <p class="overlay__desc" style="text-align:left !important;"> 
                        </p><ul style="text-align:left !important;padding-left:20px;">
                            <li>Data Penanggung Jawab</li>
                            <li>Data Perusahaan</li>
                            <!-- <li>Surat Tugas Perusahaan (Formulir Data Karyawan yang berisi nama, nomor ktp, foto, dll)</li> -->
                            <li>KTP/KITAP/KITAS Penanggung Jawab</li>
                            <li>Nomor Induk Berusaha (NIB) bagi perusahaan swasta</li>
                            <li>Pengajuan minimal adalah 5 Karyawan</li>
                        </ul>
                        <br>
                        <p class="overlay__desc" style="text-align:left !important;margin:0;">
                            Anda harus mematuhi ketentuan berikut ini.
                        </p><ul style="text-align:left !important;padding-left:20px;">
                            <li>Anda harus melengkapi formulir pengajuan STRP dengan data dan persyaratan yang sebenar-benarnya.</li>
                            <li>Anda harus menggunakan Surat Tanda Registrasi Pekerja (STRP) dengan bijak.</li>
                        </ul>
                        <p></p>
                    </div>
                </div>
            </div>
            
            <div class="img-logo-ptsp">
                <img src="https://perizinan.jakarta.go.id/img/ptsp.png" width="70px" class="img-fluid">
            </div>
            <!-- Buttons -->
            <br>
            <div class="overlay__btns">
                <form class="form-captcha">
                    <div class="d-flex col-captcha">
                        <div id="captcha">
                        </div>
                        <input type="text" class="form-control" placeholder="Captcha" id="cpatchaTextBox"/>
                        <span onclick="createCaptcha()" style="cursor: pointer;margin-left: 20px;line-height: 2;">Ganti kode</span>
                    </div>
                    <div class="d-flex">
                        <button class="overlay__btn overlay__btn--transparent" onclick="validateCaptchaBerkas()">
                            Jika sudah mengajukan, cek berkas
                        </button>
                        
                        <button class="overlay__btn overlay__btn--colors" onclick="validateCaptcha()">
                            <span>Saya mengerti, dan ajukan permohonan</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
  @endsection
 