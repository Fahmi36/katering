<!DOCTYPE html>
<html lang="en" style="--hue:380; --hue-complimentary1:367; --hue-complimentary2:397;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="google-site-verification" content="353Fx0w7TvZtHwFt1VX4gpXHFY82Zmu_Y6VMI3UGOOw" />
    <title>Sistem Management Katering</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('landing/img/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('landing/img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('landing/img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('landing/img/favicon/site.webmanifest')}}">
    <meta name="msapplication-TileColor" content="#fb9006">
    <meta name="msapplication-TileImage" content="{{asset('landing/img/favicon/android-chrome-192x192.png')}}">
    <meta name="theme-color" content="#fb9006">
    <link href="{{asset('landing/css/icon.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('landing/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.css')}}" type="text/css">
    @stack('css')
</head>
<body>
   
     @yield('content')
    <!--JS-->
    <script>
        
    var BASE_URL = "{{ url('/') }}";
    var REQUEST_URL = "<?=Request::url('/')?>";
    var CSRF = "{{ csrf_token() }}";
    </script>
    <script src="{{asset('landing/js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{asset('landing/js/jquery.min.js')}}"></script>
    <script src="{{asset('landing/js/popper.min.js')}}"></script>
    <script src="{{asset('landing/js/bootstrap.js')}}"></script>
    <script src="{{asset('landing/vendors/bs-select/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('landing/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('landing/vendors/swal/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('landing/vendors/print/print.min.js')}}"></script>
    <script src="{{asset('landing/js/BsMultiSelect.js')}}"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
    <script src="{{asset('landing/js/polyfill.min.js')}}"></script>
    <script src="{{asset('landing/js/main.js?id=241019')}}"></script>
    @stack('js')
</body>
</html>