@extends('layout.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-graph3 icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Panel Analitik
                    <div class="page-title-subheading">Hi <b>Nama</b>, selamat datang di aplikasi
                        Permohonan STRP
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card mb-3 widget-content">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Pengajuan</div>
                            <div class="widget-subheading">Dalam proses pengajuan</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-warning" id="countProcess">100
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-3 widget-content">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Disetujui</div>
                            <div class="widget-subheading">Pengajuan yang disetujui</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-success" id="countAccept">100
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-3 widget-content">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Ditolak</div>
                            <div class="widget-subheading">Pengajuan yang ditolak</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-danger" id="countDeny">100
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card mb-3 widget-content">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Karyawan</div>
                            <div class="widget-subheading">Semua jumlah karyawan pada pengajuan</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-info">-</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card mb-3 widget-content">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total</div>
                            <div class="widget-subheading">Semua berkas pengajuan</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-info" id="countAll">100</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection