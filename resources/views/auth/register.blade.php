@extends('layout.guest')
@push('css')
<link href="{{ asset('css/main.css') }}" rel="stylesheet">
<link href="{{ asset('css/front.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href=""><b>Katering</b></a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Daftar<br>
                    
                    <form method="POST" action="javascirpt:void(0);" id="formregister">
                        <div class="input-group mb-3">
                            <input required type="text" class="form-control" name="name"
                            placeholder="Nama pengguna">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user-circle"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input required type="text" class="form-control" name="email"
                            placeholder="Email Pengguna">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user-circle"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input required type="password" class="form-control" name="password" placeholder="Kata sandi">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group">
                            
                            <span id="captImg">{!! captcha_img() !!}</span>
                            <input required type="text" class="ml-1 inp-captcha" name="captcha" placeholder="Kode captcha">
                        </div>
                        <span class="refreshCapt" id="reCapt">ganti kode</span>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{url('/')}}" class="btn btn-primary btn-block">Masuk</a>
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary btn-block">Daftar</button>
                            </div>
                        </div>
                    </form>
                    <p class="mb-0">
                        <a href="#" class="text-center">Lupa password</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('js')
    <script type="text/javascript">
        $('#reCapt').click(function () {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function (data) {
                    $("#captImg").html(data.captcha);
                }
            });
        });
        $("#formregister").submit(function (event) {
            var data = new FormData($(this)[0]);
            $.ajax({
                url: BASE_URL + '/register',
                type: "POST",
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                headers: {'X-CSRF-TOKEN': CSRF},
                beforeSend:function() {
                    $('.btn-submit').attr('style','display:none;');
                    $('.loader').removeAttr('style');
                },
                success: function (response) {
                    if (response.code == 200) {
                        $('.btn-submit').removeAttr('style');
                        $('.loader').attr('style','display:none;');
                        window.location.href = '/'
                        $("#formregister")[0].reset();
                    } else {
                        
                        $('.btn-submit').removeAttr('style');
                        $('.loader').attr('style','display:none;');
                        Swal.fire(
                        'Gagal!',
                        ''+json.message+'',
                        );
                        
                    }
                },
                error: function (e) {
                    $('.btn-submit').removeAttr('style');
                    $('.loader').attr('style','display:none;');
                    if (e.status == 400) {
                        Swal.fire(
                        'Gagal!',
                        ''+e.responseJSON.message+'',
                        );
                    $("#captImg").html(e.responseJSON.captcha);
                    }else{
                        Swal.fire(
                        'Gagal!',
                        ''+e.responseJSON.message+'',
                        );
                        $("#captImg").html(e.responseJSON.captcha);
                    }
                    
                }
            });
            return false;
        });
    </script>
    @endpush