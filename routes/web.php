<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/','LandingController@index');
Route::middleware(['guest'])->group(function () {
    Route::get('/','LandingController@login');
    Route::get('daftar','LandingController@Daftar');
    Route::post('/login','LandingController@ActionLogin')->name('login');
    Route::post('/register','LandingController@ActionRegister')->name('register');
});

Route::middleware(['auth'])->group(function () {
    Route::get('home','DashboardController@index');
    Route::resource('food', FoodController::class);
});

Route::get('reload-captcha','LandingController@ReloadCaptcha');
