require([
    "esri/tasks/Locator",
    "esri/Map",
    "esri/views/MapView",
    "esri/widgets/Search",
    "esri/Graphic",
    "dojo/_base/array",
    "esri/geometry/Point",
    "esri/layers/FeatureLayer",
], function (
    Locator,
    Map,
    MapView,
    Search,
    Graphic,
    arrayUtils,
    Point,
    FeatureLayer,
) {

    var locatorTask = new Locator({
        url: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer",
        allPlaceholder: "Cari Alamat",
    });

    var map = new Map({
        basemap: "osm"
    });

    var makerSymbol = {
        type: "simple-marker",
        color: [226, 119, 40],
        outline: {
            color: [255, 255, 255],
            width: 2
        }
    };


    var template = {

        content: [{
            type: "fields",
            fieldInfos: [{
                    fieldName: "kecamatan",
                    label: "Kecamatan",
                },
                {
                    fieldName: "KELURAHAN",
                    label: "Kelurahan",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "sub_blok",
                    label: "Id Sublok",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "zona",
                    label: "Zona",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "sub_zona",
                    label: "Sub Zona",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "bersyarat",
                    label: "Perizinan Bersyarat",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "diizinkan",
                    label: "Perizinan di Izinkan",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "terbatas",
                    label: "Perizinan Terbatas",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "terbatas_bersyarat",
                    label: "Perizinan Terbatas Bersyarat",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "kdb",
                    label: "Koefisien Dasar Bangunan",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "klb",
                    label: "Koefisien Lantai Bangunan",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "kb",
                    label: "Ketinggian Bangunan",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "kdh",
                    label: "Koefisien Daerah Hijau",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "kode_blok",
                    label: "Kode Blok",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "sub_blok",
                    label: "Sub Blok",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "kode_ops_wilayah",
                    label: "Kode Ops Wilayah",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "KODE_OPS_BARU",
                    label: "Kode Ops Baru",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                }
            ]
        }]
    };
    var templatepulau = {

        content: [{
            type: "fields",
            fieldInfos: [{
                    fieldName: "KECAMATAN",
                    label: "Kecamatan",
                },
                {
                    fieldName: "KELURAHAN",
                    label: "Kelurahan",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "SUB_BLOK",
                    label: "Id Sublok",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "ZONA",
                    label: "Zona",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "SUB_ZONA",
                    label: "Sub Zona",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "KODE_BLOK",
                    label: "Kode Blok",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "SUB_BLOK",
                    label: "Sub Blok",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
                {
                    fieldName: "ID_BARU",
                    label: "Kode Ops Wilayah",
                    format: {
                        digitSeparator: true,
                        places: 0
                    }
                },
            ]
        }]
    };


    // if ($('#lat').val() == '') {
    var view = new MapView({
        container: "gisMap",
        map: map,
        center: [106.819, -6.173021],
        zoom: 15,
    });

    var parksLayer = new FeatureLayer({
        url: "https://tataruang.jakarta.go.id/server/rest/services/peta_operasional/Peta_Ops_V2_View/FeatureServer/3",
        popupTemplate: template,
        opacity: 0.3,
    });
    // } else {
    // var view = new MapView({
    //     container: "gisMap",
    //     map: map,
    //     center: [parseFloat($('#lng').val()), parseFloat($('#lat').val())],
    //     zoom: 17
    // });
    // var point = {
    //     type: "point",
    //     longitude: parseFloat($('#lng').val()),
    //     latitude: parseFloat($('#lat').val()),
    // };

    // var pointGraphic = new Graphic({
    //     geometry: point,
    //     symbol: makerSymbol
    // })
    // view.graphics.add(pointGraphic);

    // var parksLayer = new FeatureLayer({
    //     url: "https://tataruang.jakarta.go.id/server/rest/services/peta_operasional/Peta_Ops_V2_View/FeatureServer/3",

    //     popupTemplate: template,
    //     opacity: 0.3,
    // });
    // var parksLayerpulau = new FeatureLayer({
    //     url: "https://tataruang.jakarta.go.id/server/rest/services/DCKTRP/Rencana_Kota_Pulau_Seribu/FeatureServer/0",
    //     popupTemplate: templatepulau,
    //     opacity: 0.3,
    // });

    parksLayer.load().then(function () {
        map.addMany([parksLayer, parksLayerpulau]);
    }).catch(function (error) {
        map.addMany([parksLayer, parksLayerpulau]);
    });
    // }

    var parksLayerpulau = new FeatureLayer({
        url: "https://tataruang.jakarta.go.id/server/rest/services/DCKTRP/Rencana_Kota_Pulau_Seribu/FeatureServer/0",
        popupTemplate: templatepulau,
        opacity: 0.3,
    });

    parksLayerpulau.load().then(function () {
        map.add(parksLayerpulau);
    }).catch(function (error) {
        map.add(parksLayerpulau);
    });

    var searchWidget = new Search({
        view: view,
        placeholder: "Cari Alamat",
        container: 'searchDiv'
    }, 'searchAlamatIRK');
    view.ui.add(searchWidget, {position: "top-right"});
    searchWidget.watch('activeSource', function(evt){
        evt.placeholder = "Cari alamat disini";
      });

    searchWidget.on("select-result", function (event) {
        view.graphics.removeAll();
    });


    view.on("click", function (event) {
        view.graphics.removeAll();
        var lat = Math.round(event.mapPoint.latitude * 1000000) / 1000000;
        var lon = Math.round(event.mapPoint.longitude * 1000000) / 1000000;

        localStorage.setItem('kordinat', lat + ',' + lon);
        localStorage.setItem('lat', lat);
        localStorage.setItem('lng', lon);

        $("#map_lat").val(lat);
        $("#map_lng").val(lon);

        view.popup.open({
            location: event.mapPoint
        });

        view.hitTest(event.screenPoint).then(({
            results
        }) => {
            clickpoint(results);

            locatorTask.locationToAddress(event.mapPoint).then(function (
                response) {
                $("#map_alamat").val(response.address);
                localStorage.setItem('alamat', response.address);
                view.popup.title = response.address;
            }).catch(function (err) {
                view.popup.content =
                    "Tidak ada lokasi yang ditemukan";
            });
        }).catch(function (error) {
            view.popup.content = 'Zona tidak di ketahui , Silakan pilih lokasi terdekat';
        });
        var points = [
            [lon, lat]
        ];

        arrayUtils.forEach(points, function (point) {
            var graphic = new Graphic(new Point(point), makerSymbol);
            view.graphics.add(graphic);
        });
    });

    function queryArcgis() {
        parksLayer.load().then(function () {
            var query1 = parksLayer.createQuery();
            query1.where = "KODE_OPS_BARU IS NOT NULL";
            return parksLayer.queryFeatures(query1);
        }).catch(function (error) {
            var query = parksLayer.createQuery();
            query.where = "KODE_OPS_BARU IS NOT NULL";
            return parksLayer.queryFeatures(query);
        });
    }

    function clickpoint(evt) {

        if (evt == null || evt == '') {
            alert('Kordinat yang anda pilih tidak memiliki data atau tidak masuk kedalam layer')
            return false;
        } else {
            if (evt[0].graphic.attributes['KODE_OPS_BARU'] == null || evt[0].graphic.attributes['KODE_OPS_BARU'] == ''){
                alert('Terjadi kesalahan, Kode Ops tidak ada di lokasi ini')
                return false;
            }else {
                $("#kode_ops").val(evt[0].graphic.attributes['KODE_OPS_BARU']);
                console.log('Kode ops ditemukan yaitu: '+evt[0].graphic.attributes['KODE_OPS_BARU'])
            }
        }
    }

});