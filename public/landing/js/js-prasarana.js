var api_url = 'http://rest-api.pkkmart.com/';
// var api_url = 'http://192.168.100.3/rest-api/';

$(document).ready(function(){
    // checkHistoryStep();
    // modalOpenDetail();

    getKategoriIzin('3');

    //Step Pertama
    $('#btnConfJenisIzin').on('click', function () {
        katIzin = $('#kat_permohonan').val(),
            dataAppli = [
                { kategoriIzin: katIzin }
            ];
        localStorage.setItem("dataReqPermission", JSON.stringify(dataAppli));

        localStorage.setItem("step", "secondStep");
        localStorage.setItem("prevStep", "firstStep");
        customNextStep($('#firstStep'), $('#secondStep'));
    });

    //Step Kedua
    $('#btnMapData').on('click', function () {
        if (localStorage.getItem('b_izinkan') === null){
            Swal.fire({
                type: 'error',
                title: 'Terjadi Kesalahan',
                text: 'Harap pilih lokasi bangunan pada peta.'
            })
        }else{
            checkResume();
            localStorage.setItem("step", "thirdStep");
            localStorage.setItem("prevStep", 'secondStep');
            customNextStep($('#secondStep'), $('#thirdStep'));
        }
    });

    //PRINT FUNCTION
    $('#printOutSumm').on('click', function () {
        var divToPrint = document.getElementById('dataTabelPrint');
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table {\n' +
            '    width: 100%;\n' +
            '    margin-bottom: 1rem;\n' +
            '    color: #212529;\n' +
            '}' +
            'table th, table td {' +
            '    padding: .75rem;\n' +
            '    vertical-align: top;\n' +
            '    border-top: 1px solid #dee2e6;' +
            '}' +
            'table thead th {\n' +
            '    vertical-align: bottom;\n' +
            '    border-bottom: 2px solid #dee2e6;\n' +
            '}' +
            'table tbody tr:nth-of-type(odd) {\n' +
            '    background-color: rgba(0,0,0,.05);\n' +
            '}' +
            'th {\n' +
            '    text-align: inherit;\n' +
            '}' +
            '</style>' +
            '<div style="text-align: center;color:#333;">' +
            '<h2>Ringkasan Permohonan Perizinan</h2></div>';
        htmlToPrint += divToPrint.outerHTML;
        newWin = window.open("");
        newWin.document.write(htmlToPrint);
        newWin.print();
        newWin.close();
    });

//    Step keempat
    $('#btnConfSumm').on('click', function () {
        var type = localStorage.getItem('typeStart'),
            dataUser = localStorage.getItem('dataReqPermission'),
            dataRecList = '',
            dataHTML = '';
        // if(localStorage.getItem('typeStart')=="buat_izin"){
        //     izinPrasaranaCekSpasial();
        //     return;
        // }

        $.ajax({
            url: api_url + "MatriksCtrl/getRekomendasiIzin/",
            method: 'post',
            data: {dataUser: dataUser,subzona: localStorage.getItem('subzona'),idjenisizin: 3},
            dataType: 'json',
            beforeSend: function(){

            },
            success: function(data){
                if (data.success) {
                    var classCol;
                    if(data.row.length==1){
                        classCol = "col-md-12";
                    }else if(data.row.length=="2"){
                        classCol = "col-md-6";
                    }else if(data.row.length=="3"){
                        classCol = "col-md-4";
                    }else if(data.row.length=="4" || data.row.length > "4"){
                        classCol = "col-md-4";
                    }
                    var izinDataDetail = [];
                    for(var i = 0; i < data.row.length; i++) {
                        var a = data.row[i];
                        var abc = a.itbx_detail;
                        if(abc!=null){

                            var cde = abc.split(":");
                        }
                        izinDataDetail.push({idIzin: data.row.idIzin,itbx: a.itbx});
                        dataITBX = '',
                            itbxKet = '',
                            ITBX = '';

                        if (a.itbx == 'I'){ITBX='I : <span class="">di Izinkan</span>';}
                        else if(a.itbx == 'T'){ITBX='T : <span class="">Izin Terbatas</span>';}
                        else if(a.itbx == 'B'){ITBX='B : <span class="">Izin Bersyarat</span>';}
                        else if(a.itbx == 'X'){ITBX='X : <span class="">Izin Terbatas Bersyarat</span>';}
                        if(abc==null){
                            dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                        }else{

                            if(cde.length>1){

                                var itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>";
                                if(cde.length==2){
                                    itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length)+"</li>";
                                }
                                var itbxDoubleSpasial = "<li class='licardITBX'>"+cde[1].substr(cde[1].length - 2, 1) + " :" + cde[2]+"</li>";
                            }

                            if (cde.length == 1) {
                                dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                            }else if(cde.length==2){
                                dataITBX += itbxSingleSpasial;
                            }
                            else if(cde.length==3){
                                dataITBX += itbxSingleSpasial+
                                    itbxDoubleSpasial;
                            }else if(cde.length==4){
                                dataITBX += itbxSingleSpasial+
                                    itbxDoubleSpasial+
                                    "<li class='licardITBX'>"+cde[2].substr(cde[2].length - 2, 1) + " :" + cde[3]+"</li>";
                            }
                        }

                        dataHTML += '<div class="'+classCol+' mb-3">'+
                            '<div class="card text-center">\n' +
                            // '  <div class="card-header font-weight-bold">\n' + ITBX + '</div>\n' +
                            '  <div class="card-header font-weight-bold" onclick="clickPrasarana('+a.idIzin+')"><input type="checkbox" class="selIzinNew" value="'+a.idIzin+'"> Izin Prasarana</div>\n' +
                            '  <div class="card-body text-left">\n'+
                            '     <b>Jenis Izin</b> <br><p class="card-text text-center"><ul class="ulcardITBX"></b>'+a.jenis_perizinan+'</b></ul></p>\n'+
                            '     <b>Perizinan berdasarkan Zona '+localStorage.getItem('subzona')+'</b><br><p class="card-text text-center"><ul class="ulcardITBX">'+dataITBX+'</ul></p>\n';


                        if(a.itbx=="X"){
                            dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Keterangan : Prasarana ini tidak dapat diajukan di zona yang dipilih</b></li></ul></p>\n';
                        }else{
                            dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Prasarana ini dapat diajukan di zona yang dipilih</li></ul></p>\n';
                        }
                        dataHTML += "<div class='text-center'><button class='btn btn-info btnModalInfo' data-idIzin='"+a.idIzin+"' data-title='"+a.jenis_perizinan+"' data-jam='"+a.tipe_penyelesaian+"' data-angka='"+a.waktu_penyelesaian+"' data-status='"+a.status_izin_perizinan+"'>Lihat Detail</button></div>";
                        // '    <p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Perizinan : '+a.jenis_perizinan+'</li></ul></p>\n' +

                        dataHTML += '  </div>\n' +
                            '</div>' +
                            '</div>';
                    }

                    localStorage.setItem('izinDataDetail',izinDataDetail);

                    $('#recList').html(dataHTML);

                    $(".btnModalInfo").click(function(e){
                        e.preventDefault();
                        var bt = $(this),
                            idPerizinan = bt.data('idIzin'),
                            title = bt.data('title'),
                            berapa = bt.data('angka'),
                            jam = bt.data('jam'),
                            stat = bt.data('status');

                        modalOpenDetail(idPerizinan, title, berapa, jam, stat);
                    })

                }else{
                    $("#recList").html('<div class="card text-center"><div class="card-body text-center">Tidak dapat mengajukan izin apapun berdasarkan informasi yang Anda masukan</div></div>');
                }

                localStorage.setItem('step', 'fourthStep');
                customNextStep($('#thirdStep'), $('#fourthStep'));
            },
            error: function(xhr){
                Swal.fire({
                    type: 'error',
                    title: 'Terjadi Kesalahan',
                    text: xhr.status +" " + xhr.statusText
                })
            }
        });

        $('#recList').html(dataRecList);

        localStorage.setItem("step", "fourthStep");
        localStorage.setItem("prevStep", 'thirdStep');
        customNextStep($('#thirdStep'), $('#fourthStep'));
    });

    //STEP KEEMPAT
    $('#btnToPlea').on('click', function () {

        localStorage.setItem("step", "fifthStep");
        customNextStep($('#fourthStep'), $('#fifthStep'));
    });

    //STEO KEENAM
    $('.btnAppliData').on('click', function () {
        var btnType = $(this).data('type'),
            nonForm,
            setAppli,
            dataAppli = JSON.parse(localStorage.getItem("dataReqPermission"));

        if(btnType==="perorangan"){
            nonForm = $(".compForm, .govForm, .npwpI, .nipGov, .terkaitGov, .dirutComp");
            setAppli = 1;
        }else if(btnType==="perusahaan"){
            nonForm = $(".selfForm, .govForm, .npwpNik, .nipGov, .terkaitGov");
            setAppli = 2;
        }else if(btnType==="pemerintahan" || btnType==="yayasan"){
            nonForm = $(".selfForm, .compForm, .npwpNik, .npwpI, .dirutComp, .selectSelfComp");
            setAppli = 3;
        }
        nonForm.hide();

        dataAppli[0]['idPemohon'] = setAppli;
        localStorage.setItem("dataReqPermission", JSON.stringify(dataAppli));
        localStorage.setItem("typeIzin", btnType);

        localStorage.setItem("step", "seventhStep");
        localStorage.setItem("prevStep", "sixthStep");
        customNextStep($('#sixthStep'), $('#seventhStep'));
    });

    //STEP KETUJUH
    $("#btnRegisPermission").on('click', function () {
        var dataRegis = JSON.parse(localStorage.getItem("dataReqPermission")),
            setRegis;
        Swal.fire({
            text: "Apakah anda sudah pastikah bahwa data yang anda masukan benar ?",
            showCancelButton: true,
            confirmButtonColor: '#ff6704',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, sudah',
            confirmButtonClass: 'next',
            cancelButtonText: 'Belum'
        }).then((result) => {
            if (result.value) {
                var typePemohon;

                if(dataRegis[0]['idPemohon']==1){//self
                    typePemohon = "self";
                }else if(dataRegis[0]['idPemohon']==2){//company
                    typePemohon = "comp";
                    dataRegis[0]['dirut'] = $("#dirutComp").val();
                    dataRegis[0]['npwpdirut'] = $("#npwpdirutComp").val();
                    dataRegis[0]['jenisIdentitas'] = "3";
                }else if(dataRegis[0]['idPemohon']==3){//gov
                    dataRegis[0]['jenisIdentitas'] = "4";
                    dataRegis[0]['izinterkait'] = $("#terkaitGov").val();
                    typePemohon = "gov";
                }
                var npwpVal = $("."+typePemohon+"Npwp").val();

                dataRegis[0]['namaPemohon'] = $("."+typePemohon+"Form").val();
                dataRegis[0]['no_identitas'] = npwpVal;
                if(dataRegis[0]['idPemohon']==1){//cek buat jenis identitas perorangan NPWP / NIK
                    if(npwpVal.length==15){
                        dataRegis[0]['jenisIdentitas'] = "2";
                    }else{
                        dataRegis[0]['jenisIdentitas'] = "1";
                    }
                }
                dataRegis[0]['phone'] = $("#phoneF").val();
                dataRegis[0]['email'] = $("#emailF").val();
                var btnMengurus = $(".btn-pelayanan"),
                    mengurusSelect;
                btnMengurus.each(function(el,i){
                    if(i.classList[2]=='active'){
                        if(i.dataset.title=="Y"){
                            mengurusSelect = "1";
                        }else{
                            mengurusSelect = "2";
                        }
                    }
                })
                dataRegis[0]['mengurus'] = mengurusSelect;

                localStorage.setItem("dataReqPermission", JSON.stringify(dataRegis));

                $.ajax({
                    url: api_url + 'matriksCtrl/savePermohonanIzin',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        subzona: localStorage.getItem('subzona'),
                        dataRegist: localStorage.getItem('dataReqPermission'),
                        izinDataDetail: localStorage.getItem('izinDataDetail'),
                        klb:localStorage.getItem('klb'),
                        kdb:localStorage.getItem('kdb'),
                        kdh:localStorage.getItem('kdh'),
                        idArahan:localStorage.getItem('idArahan'),
                    },
                    beforeSend:function() {
                        $('#fourthStep').html(loader);
                    },
                    success:function(data) {
                        $('#backPemohon').show();
                        sendEmailPrasarana(data.token, data.npwp);
                    }
                });
            }
        })
    });

    //Back
    $(".previous").on('click', function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });
    //NEXT
    $(".next").on('click', function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale('+scale+')',
                    // 'position': 'absolute',
                    'display' : 'none'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    // NEXT IZIN
    $('.doneIzinProf').on('click', function () {
        localStorage.setItem("step", "fifthStep");
        localStorage.setItem("prevStep", "fourthStep");
        customNextStep($('#fourthStep'), $('#fifthStep'));
    });

});

function modalOpenDetail(a,b,c,d,e) {
    var getBersyarat = localStorage.getItem('b_bersyarat'),
        getIzinkan = localStorage.getItem('b_izinkan'),
        getTerbatas = localStorage.getItem('b_terbatas'),
        getTerbatasB = localStorage.getItem('b_terbatasbersyarat'),
        content = '',
        tableTr = "",
        ctTable = '',
        jam = '',
        waktu = '';

    if (c !== null) {jam = c}
    else {jam = '00'}
    if (d !== null) {waktu = d}
    else {waktu = 'Tak tersedia'}

    $.ajax({
        url: api_url + 'cronJobs/getPersyaratan?idIzin=19',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var tableTr = "";
            for (var i in data) {
                tableTr += "<tr><td>" + data[i].no_persyaratan + "</td><td>" + data[i].persyaratan + "</td></tr>";
                if (data[i].child.length > 0) {
                    for (var ch in data[i].child) {
                        tableTr += "<tr><td></td><td>" + data[i].child[ch].persyaratan + "</td></tr>";
                    }
                }
            }
            ctTable += tableTr;

            content += '<div class="row">' +
                '<div class="col-md-12">' +
                '<div class="accordion" id="accordionExample">\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingOne">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">\n' +
                '          Waktu Perizinan' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '\n' +
                '    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">\n' +
                '      <div class="card-body">' +
                '       <div class="row">\n' +
                '    <div class="col-md-12">\n' +
                '    <div class="text-center border\n' +
                ' p-2">\n' +
                '        <span class="d-block h5">Izin Selesai Dalam</span>\n' +
                '    <span class="d-block h3">'+jam+'</span>\n' +
                '    <span class="h4">'+waktu+'</span>\n' +
                '    </div>\n' +
                '    </div>' +
                '</div>' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingTwo">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">\n' +
                '          Kegiatan Diizinkan (ITBX)' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">\n' +
                '      <div class="card-body">\n' +

                '<table class="table table-striped table-bordered text-left" id="dataTabelPrint" border="1" cellpadding="3">\n' +
                '            <thead>\n' +
                '            <tr>\n' +
                '                <th scope="col">#</th>\n' +
                '                <th scope="col">Kriteria</th>\n' +
                '                <th scope="col">Data Anda</th>\n' +
                '            </tr>\n' +
                '            </thead>' +
                '            <tbody>' +
                '<tr>' +
                '<th scope="row">1</th>\n' +
                '<td>Bangunan Diizinkan</td>\n' +
                '<td><div id="accBuilding" class="ml-3">' + getIzinkan + '</div></td>\n' +
                '</tr>\n' +
                '<tr>\n' +
                '<th scope="row">2</th>\n' +
                '<td>Bangunan Bersyarat</td>\n' +
                '<td><div id="CondBuilding" class="ml-3">' + getBersyarat + '</div></td>\n' +
                '</tr>\n' +
                '<tr>\n' +
                '<th scope="row">3</th>\n' +
                '<td>Bangunan Terbatas</td>\n' +
                '<td><div id="limitedBuilding" class="ml-3">' + getTerbatas + '</div></td>\n' +
                '</tr>\n' +
                '<tr>\n' +
                '<th scope="row">4</th>\n' +
                '<td>Bangunan Terbatas Bersyarat</td>\n' +
                '<td><div id="limitcondBuilding" class="ml-3">' + getTerbatasB + '</div></td>\n' +
                '</tr>' +
                '            </tbody>\n' +
                '        </table>' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingThree">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">\n' +
                '          Persyaratan' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">\n' +
                '      <div class="card-body">\n' +
                '<table class="table table-striped table-bordered text-left" id="dataTabelPrint" border="1" cellpadding="3">' +
                '<thead>' +
                '<tr>' +
                '                <th scope="col">No.</th>\n' +
                '                <th scope="col">Persyaratan</th>\n' +
                '</tr></thead>' +
                '<tbody>' +
                ctTable +
                '</tbody>' +
                '</table>' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingThree">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">\n' +
                '          Tahap Izin' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">\n' +
                '      <div class="card-body">\n' +
                '        -' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>' +
                '</div></div>';
            var title = b+' ( '+e+' )';
            $('#izinContents').html(content);
            $('#izinTitles').html(title);


            $('#modalDetailIzin').modal('show');
        }
    });
}

function clickPrasarana(id) {
    var izinChecked = $('.selIzinNew').map(function() {
            if(this.checked)
                return this.value;
        }).get(),
        dataReq = JSON.parse(localStorage.getItem('dataReqPermission'));

    dataReq[0].izinData = izinChecked;
    localStorage.setItem("dataReqPermission", JSON.stringify(dataReq));
}

function checkResume() {
    var dataIzin = JSON.parse(localStorage.getItem('dataReqPermission')),
        mapData5 = localStorage.getItem('alamat'),
        mapData6 = localStorage.getItem('zona'),
        mapData7 = localStorage.getItem('subzona'),
        mapData8 = localStorage.getItem('idsubblok'),
        mapData9 = localStorage.getItem('kordinat');

    // if (dataIzin[0].jenisIzin == 1) { dataJenis = 'Perorangan' }
    // else if (dataIzin[0].jenisIzin == 2) { dataJenis = 'Utama' }

    $('#katIzin').html(dataIzin[0].kategoriIzin);
    $('#addressUser').html(mapData5);
    $('#ZonaUser').html(mapData6);
    $('#subzonaUser').html(mapData7);
    $('#idsubblok').html(mapData8);
    $('#kordinat').html(mapData9);
}

function checkHistoryStep() {
    var stepNum = localStorage.getItem("step");

    if (stepNum==null) {
        $("#firstStep").attr('style', 'display: block; left: 0%; opacity: 1;');
        return true;
    }else {
        checkResume();
        Swal.fire({
            title: 'Pemberitahuan',
            text: "Apakah anda ingin melanjutkan proses sebelumya?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#ff6921',
            cancelButtonColor: '#575757',
            confirmButtonText: 'Ya, Lanjutkan',
            cancelButtonText: 'Ulangi',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                // var array = ["first","second","third","fourth","sixth","seventh","last"];
                $(".stepContainer .stepWizard:not(:first-of-type)").map(function(el,i){

                    var stepNum = localStorage.getItem("step");
                    if(i.getAttribute("id")==stepNum){
                        $(".stepContainer #firstStep")[0].setAttribute("style","transition:ease 0.5s all;webkit-transition:ease 0.5s all;display: none; left: 0%; opacity: 0;");
                        i.setAttribute("style","display: block; left: 0%; opacity: 1;transition:ease 0.5s all;webkit-transition:ease 0.5s all;");
                    }else{
                        i.setAttribute("style","display: none; left: 0%; opacity: 0;");
                    }
                    // debugger;

                });

                // buat hide step tertentu
                var btnType = localStorage.getItem("typeIzin"),nonForm;
                if(btnType!==null || btnType!=="null"){
                    if(btnType==="perorangan"){
                        nonForm = $(".compForm, .govForm, .npwpI, .nipGov, .terkaitGov, .dirutComp");
                    }else if(btnType==="perusahaan"){
                        nonForm = $(".selfForm, .govForm, .npwpNik, .nipGov, .terkaitGov");
                    }else if(btnType==="pemerintahan" || btnType==="yayasan"){
                        nonForm = $(".selfForm, .compForm, .npwpNik, .npwpI, .dirutComp, .selectSelfComp");
                    }
                    nonForm.hide();
                }

                // buat set form field-field nya
                var datanya = JSON.parse(localStorage.getItem('dataReqPermission')),
                    typenya;
                if(btnType=="perorangan"){
                    typenya = "self";
                }else if(btnType=="perusahaan"){
                    typenya = "comp";
                    $("#dirutComp").val(datanya[0].dirut);
                    $("#npwpdirutComp").val(datanya[0].npwpdirut);
                }else{
                    $("#terkaitGov").val(datanya[0].izinterkait);
                    typenya = "gov";
                }
                $("."+typenya+"Form").val(datanya[0].namaPemohon);
                $("."+typenya+"Npwp").val(datanya[0].no_identitas);
                $("#phoneF").val(datanya[0].phone);
                $("#emailF").val(datanya[0].email);
                var mengurusSendiri = datanya[0].mengurus;
                $(".btn-pelayanan").each(function(el,i){
                    if(mengurusSendiri=="2"){
                        if(i.dataset.title=="N"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }else{
                        if(i.dataset.title=="Y"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }
                })


            }else{
                localStorage.clear();

                location.href='../';
            }
        })
    }
}

function getKategoriIzin(idIzin){
    $.ajax({
        url: api_url + "MatriksCtrl/getKategoriIzin",
        method: 'GET',
        data: {idIzin: idIzin},
        dataType: 'json',
        success:function(dt){
            if(dt.rowCount>0){
                $("#kat_permohonan").html('');
                for(var i in dt.row){
                    const katIzin = dt.row[i].kategori_izin;
                    if(katIzin!=null){
                        $("#kat_permohonan").append('<option value="'+katIzin+'">'+katIzin+'</option>');
                    }
                }
            }else{

            }
        }
    })
}

function directLogin() {
    $('#go-register').click();
}




function izinPrasaranaCekSpasial(){
    var dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        subzona = localStorage.getItem('subzona'),
        dataHTML = '';
    $.ajax({
        url: api_url + "/MatriksCtrl/matriksSpasial",
        method: "get",
        data: {subzona: subzona, dataReqPermission: localStorage.getItem('dataReqPermission')},
        dataType: 'json',
        success:function(data){
            if (data.success) {
                var classCol;
                if(data.data.length==1){
                    classCol = "col-md-12";
                }else if(data.data.length=="2"){
                    classCol = "col-md-6";
                }else if(data.data.length=="3"){
                    classCol = "col-md-4";
                }else if(data.data.length=="4"){
                    classCol = "col-md-3";
                }
                var izinDataDetail=[];
                for(var i = 0; i < data.data.length; i++) {
                    var a = data.data[i];
                    var abc = a.itbx_detail;
                    if(abc!=null){

                        var cde = abc.split(":");
                    }
                    izinDataDetail.push({'idIzin': a.idIzin,'itbx': a.itbx});
                    dataITBX = '',
                        itbxKet = '',
                        ITBX = '';

                    if (a.itbx == 'I'){ITBX='I : <span class="">di Izinkan</span>';}
                    else if(a.itbx == 'T'){ITBX='T : <span class="">Izin Terbatas</span>';}
                    else if(a.itbx == 'B'){ITBX='B : <span class="">Izin Bersyarat</span>';}
                    else if(a.itbx == 'X'){ITBX='X : <span class="">Izin Terbatas Bersyarat</span>';}
                    if(abc==null){
                        dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                    }else{

                        if (cde.length == 1) {
                            dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                        }
                        else if(cde.length==3){
                            dataITBX += "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>"+
                                "<li class='licardITBX'>"+cde[1].substr(cde[1].length -2, 1) + " :" + cde[2]+"</li>";
                        }else if(cde.length==4){
                            dataITBX += "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>"+
                                "<li class='licardITBX'>"+cde[1].substr(cde[1].length - 2, 1) + " :" + cde[2]+"</li>"+
                                "<li class='licardITBX'>"+cde[2].substr(cde[2].length - 2, 1) + " :" + cde[3]+"</li>";
                        }
                    }

                    dataHTML += '<div class="'+classCol+' mb-3">'+
                        '<div class="card text-center">\n' +
                        // '  <div class="card-header font-weight-bold">\n' + ITBX + '</div>\n' +
                        '  <div class="card-header font-weight-bold">Izin Prasarana / Infrastruktur</div>\n' +
                        '  <div class="card-body text-left">\n'+
                        '     <b>Jenis Izin</b> <br><p class="card-text text-center"><ul class="ulcardITBX"></b>'+a.jenis_perizinan+'</b></ul></p>\n'+
                        '     <b>Perizinan berdasarkan Zona '+localStorage.getItem('subzona')+'</b><br><p class="card-text text-center"><ul class="ulcardITBX">'+dataITBX+'</ul></p>\n';


                    if(a.itbx=="X"){
                        dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Izin ini <b>tidak dapat</b> diajukan di zona yang dipilih</b></li></ul></p>\n';
                    }else{
                        dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Izin ini <b>dapat</b> diajukan di zona yang dipilih</li></ul></p>\n';
                    }
                    // '    <p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Perizinan : '+a.jenis_perizinan+'</li></ul></p>\n' +

                    dataHTML += '  </div>\n' +
                        '</div>' +
                        '</div>';
                }
                localStorage.setItem('izinDataDetail',JSON.stringify(izinDataDetail));
                $('#recList').html(dataHTML);

                localStorage.setItem('step', 'seventhStep');
                customNextStep($('#sixthStep'), $('#seventhStep'));
            }else{
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!'
                })
            }
        },
        error:function(xhr){
            Swal.fire(
                'Oops',
                xhr.status + " " + xhr.statusText
            )
        }
    })
}

function savePermohonanPrasarana(){
    var type = localStorage.getItem('typeStart'),
        dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        dataArahan = JSON.parse(localStorage.getItem('dataArahan'));

    $.ajax({
        url: api_url + 'matriksCtrl/savePermohonanIzin',
        type: 'POST',
        dataType: 'json',
        data: {
            subzona: localStorage.getItem('subzona'),
            dataRegist: localStorage.getItem('dataReqPermission'),
            izinDataDetail: localStorage.getItem('izinDataDetail'),
            klb:localStorage.getItem('klb'),
            kdb:localStorage.getItem('kdb'),
            kdh:localStorage.getItem('kdh'),
            idArahan:localStorage.getItem('idArahan')},
        beforeSend:function() {
            // Swal.showLoading();
        },
        success:function(data) {
            $('#backPemohon').show();
            finalContent = "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email "+ dataUser[0].email +"</div>" +
                "<button type='button' class='btn rounded-0 btn--main' onclick='directLoginNew();'>Selesai</button>";
            sendEmailPrasarana(data.token, data.npwp);

            $('#finalContent').html(finalContent);
        }
    });

}


function sendEmailPrasarana(dtoken, dnpwp) {
    $.ajax({
        url: api_url + 'users/sendMailExceptIMB/',
        type: 'POST',
        dataType: 'json',
        data: {npwp: dnpwp, token: dtoken, pendaftaran: 'pendaftaran'},
        beforeSend: function () {
        },
        success: function (data) {
            // if(data.success){

            Swal.fire({
                type: 'success',
                title: 'Data dikirim ke email anda.',
                showConfirmButton: false,
                timer: 3000
            })
            // if (localStorage.getItem('typeStart') != 'buat_izin'){
            directLoginNew();
            // }
            // }
        }
    })
}

function directLoginNew(){
    localStorage.setItem('step', 'cekIzin');
    location.href='index.html';
}

// function sendEmailPrasarana(dtoken, dnpwp) {
//     $.ajax({
//         url: api_url + 'users/sendMail',
//         type: 'POST',
//         dataType: 'json',
//         data: {npwp: dnpwp, token: dtoken, pendaftaran: 'pendaftaran'},
//         beforeSend: function () {
//         },
//         success: function (data) {
//             Swal.fire({
//                 type: 'success',
//                 title: 'Data dikirim ke email anda.',
//                 showConfirmButton: false,
//                 timer: 3000
//             })
//             // if (localStorage.getItem('typeStart') != 'buat_izin'){
//             directLogin();
//             // }
//         }
//     })
// }