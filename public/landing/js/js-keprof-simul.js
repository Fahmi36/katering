var api_url = 'http://rest-api.pkkmart.com/';
// var api_url = 'http://localhost/rest-api/';
// var api_url = 'http://10.15.36.40/api/rest-api/';

$(document).ready(function(){
    $('.btnRefresh').on('click', function() {
        location.reload();
    });
    
    // PRINT
    $('#printOutDetail').on('click', function() {
        $('.printAct').addClass('show');
        print();
    });


    
    $(".previous").on('click', function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        previous_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                scale = 0.8 + (1 - now) * 0.2;
                left = ((1-now) * 50)+"%";
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            easing: 'easeInOutBack'
        });
    });
    
    
    //Step Pertama
    $('#btnConfJenisIzin').on('click', function () {
        var jenPemohon = $('#jen_pemohon').val(),
            dok_sebelumnya = $('#dok_sebelumnya').val(),
            reqData = JSON.parse(localStorage.getItem("dataReqPermission"));
            
        reqData[0]['jenisPemohon'] = jenPemohon;
        reqData[0]['dokSebelumya'] = dok_sebelumnya;
        localStorage.setItem("dataReqPermission", JSON.stringify(reqData));
    
        localStorage.setItem("step", "secondStep");
        localStorage.setItem("prevStep", "firstStep");
        customNextStep($('#firstStep'), $('#secondStep'));
    });
    
    //Step Kedua
    $('#btnConfJenisIzinRenc').on('click', function () {
        var jenIzin = $('#jen_permohonan').val(),
            katIzin = $('#kat_permohonan').val(),
            adminProf = $('#kat_adminkeprof').val(),
            reqData = JSON.parse(localStorage.getItem("dataReqPermission"));
            
        reqData[0]['jenisIzin'] = jenIzin;
        reqData[0]['kategoriIzin'] = katIzin;
        reqData[0]['adminProf'] = adminProf;
        localStorage.setItem("dataReqPermission", JSON.stringify(reqData));

        localStorage.setItem("step", "thirdStep");
        localStorage.setItem("prevStep", "secondStep");
        customNextStep($('#secondStep'), $('#thirdStep'));
    });
    
    $('#kat_permohonan').on("change", function () {
        var pilihanKosong = "";
        if ($(this).val() === "Administrasi Keprofesian"){
            pilihanKosong += '<div class="form-group mb-0 row">' +
            '                   <label for="kat_adminkeprof" class="col-md-4 col-form-label">Kategori Administrasi Keprofesian</label>' +
            '                   <div class="col-md-8">' +
            '                       <select class="w-100 text-center form__field inpSelectData" id="kat_adminkeprof">' +
            '                           <option hidden>Kategori Permohonan Izin</option>' +
            '                           <option value="Dokter">Dokter</option>' +
            '                           <option value="Tenaga Kesehatan">Tenaga Kesehatan</option>' +
            '                           <option value="Profesi Kesehatan Lain">Profesi Kesehatan Lain</option>' +
            '                           <option value="Pekerjaan Umum dan Tata Ruang">Pekerjaan Umum dan Tata Ruang</option>' +
            '                           <option value="Distributor Alat Kesehatan/Farmasi">Distributor Alat Kesehatan/Farmasi</option>' +
            '                           <option value="Ketenagakerjaan">Ketenagakerjaan</option>' +
            '                           <option value="Distributor Pakan/Obat Hewan">Distributor Pakan/Obat Hewan</option>' +
            '                           <option value="Distributor Daging">Distributor Daging</option>' +
            '                       </select>' +
            '                   </div>' +
            '               </div>';
            $('#selectAdminKeprof').html(pilihanKosong);
        }else{
            $('#selectAdminKeprof').empty();
        }
    });
    
    //Step Ketiga
    $('#btnMapData').on('click', function () {
        if (localStorage.getItem('b_izinkan') === null){
            Swal.fire({
                type: 'error',
                title: 'Terjadi Kesalahan',
                text: 'Harap pilih lokasi bangunan pada peta.'
            })
        }else{
            checkResume();
            localStorage.setItem("step", "fourthStep");
            localStorage.setItem("prevStep", 'thirdStep');
            customNextStep($('#thirdStep'), $('#fourthStep'));
        }
    });
    
    //Step Keempat
    $('#btnConfSumm').on('click', function () {
        var type = localStorage.getItem('typeStart'),
        dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        dataRecList = '',
        dataHTML = ''
        contents = '',
        recList = '',
        idPemohon = dataUser[0].idPemohon,
        arahancard = $('#arahanRumus'),
        columncard = $('#cardRec'),
        columnlist = $('#recList'),
        arahankdh = $('#arahanKDH'),
        arahanitbx = $('#arahanTableITBX'),
        luasTanah = parseFloat(dataUser[0].luas_tanah_renc),
        klbPeta = parseFloat(localStorage.getItem('klb')),
        kdbPeta = parseFloat(localStorage.getItem('kdb')),
        finalContent = "";
        
        //Hitung Rumus
        // KDB = Luas Tanah x KDB yang ditentukan (%)
        var kdb = luasTanah * (kdbPeta / 100);
        
        // KLB = Luas Tanah x KLB yang ditentukan
        var klb = luasTanah * klbPeta;
        
        // Jml Lantai
        // Di Ambil Angka depannya saja, walapun 4,9 tetap di ambil 4
        var jmlLantai = klb / kdb;
        jmlLantai = jmlLantai.toString();
        splitLantai = jmlLantai.split(".");
        
        if(splitLantai.length>0){
            jmlLantai = splitLantai[0];
        }
        
        var ctArahan = "<span>Dengan ketentuan KLB yaitu 'Luas Tanah x KLB Peta', Luas Tanah <b>" +luasTanah+ "m<sup>2</sup></b>, dapat membangun bangunan dengan luas lantai maksimal hingga <b>"+ klb + "m<sup>2</sup></span></p>";
        
        // if(isNaN(jmlLantai)){
        //     ctArahan = "Tidak dapat membangun bangunan pada lokasi yang dipilih";
        //     $('#recList').html('<div class="text-center font-weight-bold text-danger">Data yang Anda masukkan salah pada bagian (Subzona '+ localStorage.getItem('subzona') +')</div>');
        // }
        arahancard.html(ctArahan);
        
        // KDH
        var htmlKDH = '';
        htmlKDH += '<div>'+
        '<p>Dengan ketentuan KDH yaitu "(KDH * Luas Tanah)/100", Daerah Hijau yang harus dipenuhi minimal sebesar'+
        '<span class="font-weight-bold d-block h5">'+(localStorage.getItem('kdh') * luasTanah)/100+'m&sup2;</span>'+
        '</p>'+
        '</div>';
        arahankdh.html(htmlKDH);
        
        columncard.html(contents);
        
        
        
        // TABEL ITBX
        var getBersyarat = localStorage.getItem('b_bersyarat'),
        getIzinkan = localStorage.getItem('b_izinkan'),
        getTerbatas = localStorage.getItem('b_terbatas'),
        getTerbatasB = localStorage.getItem('b_terbatasbersyarat'),
        tabelItbx = '';
        
        var gSyarat = getBersyarat.split(','),
        gIzinkan = getIzinkan.split(','),
        gTerbatas = getTerbatas.split(','),
        gTerbatasB = getTerbatasB.split(',');
        
        var listSyarat = "<ol>";
        for (var ia in gSyarat) {
            listSyarat += "<li>"+gSyarat[ia]+"</li>";
        }
        listSyarat += "</ol>";
        
        var listIzinkan = "<ol>";
        for (var ib in gIzinkan) {
            listIzinkan += "<li>"+gIzinkan[ib]+"</li>";
        }
        listIzinkan += "</ol>";
        
        var listTerbatas = "<ol>";
        for (var ic in gTerbatas) {
            listTerbatas += "<li>"+gTerbatas[ic]+"</li>";
        }
        listTerbatas += "</ol>";
        
        var listTerbatasB = "<ol>";
        for (var id in gTerbatasB) {
            listTerbatasB +="<li>"+gTerbatasB[id]+"</li>";
        }
        listTerbatasB += "</ol>";
        
        tabelItbx += '<div class="table-responsive">\n' +
        '<table class="table table-striped table-bordered text-left" id="dataTabelPrint" border="1" cellpadding="3">\n' +
        '            <thead>\n' +
        '            <tr>\n' +
        '                <th scope="col">#</th>\n' +
        '                <th scope="col">Kriteria</th>\n' +
        '                <th scope="col">Data Anda</th>\n' +
        '            </tr>\n' +
        '            </thead>' +
        '            <tbody>' +
        '<tr>' +
        '<th scope="row">1</th>\n' +
        '<td>Bangunan Diizinkan</td>\n' +
        '<td><div id="accBuilding" class="ml-3">' + listSyarat + '</div></td>\n' +
        '</tr>\n' +
        '<tr>\n' +
        '<th scope="row">2</th>\n' +
        '<td>Bangunan Bersyarat</td>\n' +
        '<td><div id="CondBuilding" class="ml-3">' + listIzinkan + '</div></td>\n' +
        '</tr>\n' +
        '<tr>\n' +
        '<th scope="row">3</th>\n' +
        '<td>Bangunan Terbatas</td>\n' +
        '<td><div id="limitedBuilding" class="ml-3">' + listTerbatas + '</div></td>\n' +
        '</tr>\n' +
        '<tr>\n' +
        '<th scope="row">4</th>\n' +
        '<td>Bangunan Terbatas Bersyarat</td>\n' +
        '<td><div id="limitcondBuilding" class="ml-3">' + listTerbatasB + '</div></td>\n' +
        '</tr>' +
        '            </tbody>\n' +
        '        </table>' +
        '                            </div>\n';
        
        $('#itbxPeta').html(tabelItbx);
        
        getDataKondisi();
        localStorage.setItem('step', 'fifthStep');
        localStorage.setItem('prevStep', 'fourthStep');
        customNextStep($('#fourthStep'), $('#fifthStep'));
    });
});

function checkResume() {
    var dataIzin = JSON.parse(localStorage.getItem('dataReqPermission')),
        dataJenis = '',
        mapData5 = localStorage.getItem('alamat'),
        mapData6 = localStorage.getItem('zona'),
        mapData7 = localStorage.getItem('subzona'),
        mapData8 = localStorage.getItem('idsubblok'),
        mapData9 = localStorage.getItem('kordinat');

    // if (dataIzin[0].jenisIzin == 1) { dataJenis = 'Perorangan' }
    // else if (dataIzin[0].jenisIzin == 2) { dataJenis = 'Utama' }

    $('#jenPemohon').html(dataIzin[0].jenisPemohon);
    $('#dokSebelumnya').html(dataIzin[0].dokSebelumya);
    $('#jenPermohonan').html(dataIzin[0].jenisIzin);
    $('#katPermohonan').html(dataIzin[0].kategoriIzin);
    $('#katAdminkeprof').html(dataIzin[0].adminProf);
    $('#addressUser').html(mapData5);
    $('#ZonaUser').html(mapData6);
    $('#subzonaUser').html(mapData7);
    $('#idsubblok').html(mapData8);
    $('#kordinat').html(mapData9);
}

function checkHistoryStep() {
    var stepNum = localStorage.getItem("step");
    
    if (stepNum==null) {
        $("#firstStep").attr('style', 'display: block; left: 0%; opacity: 1;');
        return true;
    }else {
        checkResume();
        Swal.fire({
            title: 'Pemberitahuan',
            text: "Apakah anda ingin melanjutkan proses sebelumya?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#ff6921',
            cancelButtonColor: '#575757',
            confirmButtonText: 'Ya, Lanjutkan',
            cancelButtonText: 'Ulangi',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                // var array = ["first","second","third","fourth","sixth","seventh","last"];
                $(".stepContainer .stepWizard:not(:first-of-type)").map(function(el,i){
                    
                    var stepNum = localStorage.getItem("step");
                    if(i.getAttribute("id")==stepNum){
                        $(".stepContainer #firstStep")[0].setAttribute("style","transition:ease 0.5s all;webkit-transition:ease 0.5s all;display: none; left: 0%; opacity: 0;");
                        i.setAttribute("style","display: block; left: 0%; opacity: 1;transition:ease 0.5s all;webkit-transition:ease 0.5s all;");
                    }else{
                        i.setAttribute("style","display: none; left: 0%; opacity: 0;");
                    }
                    // debugger;
                    
                });
                
                // buat hide step tertentu
                var btnType = localStorage.getItem("typeIzin"),nonForm;
                if(btnType!==null || btnType!=="null"){
                    if(btnType==="perorangan"){
                        nonForm = $(".compForm, .govForm, .npwpI, .nipGov, .terkaitGov, .dirutComp");
                    }else if(btnType==="perusahaan"){
                        nonForm = $(".selfForm, .govForm, .npwpNik, .nipGov, .terkaitGov");
                    }else if(btnType==="pemerintahan" || btnType==="yayasan"){
                        nonForm = $(".selfForm, .compForm, .npwpNik, .npwpI, .dirutComp, .selectSelfComp");
                    }
                    nonForm.hide();
                }
                
                // buat set form field-field nya
                var datanya = JSON.parse(localStorage.getItem('dataReqPermission')),
                typenya;
                if(btnType=="perorangan"){
                    typenya = "self";
                }else if(btnType=="perusahaan"){
                    typenya = "comp";
                    $("#dirutComp").val(datanya[0].dirut);
                    $("#npwpdirutComp").val(datanya[0].npwpdirut);
                }else{
                    $("#terkaitGov").val(datanya[0].izinterkait);
                    typenya = "gov";
                }
                $("."+typenya+"Form").val(datanya[0].namaPemohon);
                $("."+typenya+"Npwp").val(datanya[0].no_identitas);
                $("#phoneF").val(datanya[0].phone);
                $("#emailF").val(datanya[0].email);
                var mengurusSendiri = datanya[0].mengurus;
                $(".btn-pelayanan").each(function(el,i){
                    if(mengurusSendiri=="2"){
                        if(i.dataset.title=="N"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }else{
                        if(i.dataset.title=="Y"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }
                })
                
                
            }else{
                localStorage.clear();
                
                location.href='../';
            }
        })
    }
}

function getDataKondisi() {
    var dataUser = JSON.parse(localStorage.getItem('dataReqPermission'));
    // luasTanah = parseFloat(dataUser[0].luas_tanah),
    // klbPeta = parseFloat(localStorage.getItem('klb')),
    // kdbPeta = parseFloat(localStorage.getItem('kdb'));
    // //Hitung Rumus
    // // KDB = Luas Tanah x KDB yang ditentukan (%)
    // var kdb = luasTanah * (kdbPeta / 100);
    
    // // KLB = Luas Tanah x KLB yang ditentukan
    // var klb = luasTanah * klbPeta;
    
    // // Jml Lantai
    // // Di Ambil Angka depannya saja, walapun 4,9 tetap di ambil 4
    // var jmlLantai = klb / kdb;
    // jmlLantai = jmlLantai.toString();
    // splitLantai = jmlLantai.split(".");
    
    // if(splitLantai.length>0){
    //     jmlLantai = splitLantai[0];
    // }
    // var ctArahan = "Dengan Luas Tanah <b>" + luasTanah + "m</b> pada lokasi yang dipilih hanya bisa maksimal melakukan pembangunan gedung <b>" +  jmlLantai + "</b> lantai.";
    
    // if(isNaN(jmlLantai)){
    //     ctArahan = "Tidak dapat membangun bangunan pada lokasi yang dipilih";
    //     $('#recList').html('<div class="text-center mx-auto font-weight-bold">Tidak Ada Rekomendasi</div>');
    //     return
    // }
    
    $.ajax({
        // url: api_url + 'CronJobs/cekKondisiKawasan',
        url: api_url + 'Perizinan/getNewMatriks',
        type: 'GET',
        dataType: 'json',
        data:{kondisi: localStorage.getItem('dataReqPermission'),subzona:localStorage.getItem('subzona')},
        beforeSend:function() {
            // Swal.showLoading();
        },
        success:function(data) {
            var dataHTML = '';
            var classCol;
            if (data.success) {
                var classCol;
                if(data.row.length==1){
                    classCol = "col-md-12";
                }else if(data.row.length=="2"){
                    classCol = "col-md-6";
                }else if(data.row.length=="3"){
                    classCol = "col-md-4";
                }else if(data.row.length=="4" || data.row.length > "4"){
                    classCol = "col-md-4";
                }
                var izinDataDetail = [];
                for(var i = 0; i < data.row.length; i++) {
                    var a = data.row[i];
                    tahapIzinStat = a.tahapanIzin;
                    var abc = a.itbx_detail;
                    if(abc!=null){
                        
                        var cde = abc.split(":");
                    }
                    izinDataDetail.push({idIzin: data.row.idIzin,itbx: a.itbx});
                    dataITBX = '',
                    itbxKet = '',
                    ITBX = '';
                    
                    if (a.itbx == 'I'){ITBX='I : Diizinkan';}
                    else if(a.itbx == 'T'){ITBX='T : Diizinkan Terbatas';}
                    else if(a.itbx == 'B'){ITBX='B : Diizinkan Bersyarat';}
                    else if(a.itbx == 'TB'){ITBX='TB : Diizinkan Terbatas Bersyarat';}
                    else if(a.itbx == 'X'){ITBX='X : Tidak Diizinkan';}
                    if(abc==null){
                        dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                    }else{
                        
                        if(cde.length>1){
                            
                            var itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>";
                            if(cde.length==2){
                                itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length)+"</li>";
                            }
                            var itbxDoubleSpasial = "<li class='licardITBX'>"+cde[1].substr(cde[1].length - 2, 1) + " :" + cde[2]+"</li>";
                        }
                        
                        if (cde.length == 1) {
                            dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                        }else if(cde.length==2){
                            dataITBX += itbxSingleSpasial;
                        }
                        else if(cde.length==3){
                            dataITBX += itbxSingleSpasial+
                            itbxDoubleSpasial;
                        }else if(cde.length==4){
                            dataITBX += itbxSingleSpasial+
                            itbxDoubleSpasial+
                            "<li class='licardITBX'>"+cde[2].substr(cde[2].length - 2, 1) + " :" + cde[3]+"</li>";
                        }
                    }
                    
                    dataHTML += '<div class="'+classCol+' mb-3">'+
                    '<div class="card text-center" style="height:100%;">\n' +
                    // '  <div class="card-header font-weight-bold">\n' + ITBX + '</div>\n' +
                    // '  <div class="card-header font-weight-bold" onclick="clickKeprofesian('+a.idIzin+')">' +
                    // <input type="checkbox" class="selIzinNew" value="'+a.idIzin+'">
                    // 'Izin Keprofesian</div>\n' +
                    '  <div class="card-body text-left">\n'+
                    '     <b>Jenis Izin</b> <br><p class="card-text text-center"><ul class="ulcardITBX"></b>'+a.nama_izin_terbit+'</b></ul></p>\n';
                    // '     <b>Perizinan berdasarkan Zona '+localStorage.getItem('subzona')+'</b>' +
                    // '<br><p class="card-text text-center"><ul class="ulcardITBX">'+dataITBX+'</ul></p>\n';
                    
                    
                    // if(a.itbx=="X"){
                    //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Keterangan : Profesi ini tidak dapat diajukan di zona yang dipilih</b></li></ul></p>\n';
                    // }else{
                    //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Profesi ini dapat diajukan di zona yang dipilih</li></ul></p>\n';
                    // }
                    var descIzin = a.deskripsi_izin,
                            dataJam = a.total_waktu,
                            dataBiaya = a.biaya
                            ;
                    if(descIzin==null){
                        descIzin = a.nama_izin_terbit;
                    }
                    if(dataJam==null){
                        if(a.waktu_penyelesaian!=null){
                            dataJam = a.waktu_penyelesaian + " " + a.tipe_waktu;
                        }else{
                            dataJam = "Informasi Belum Tersedia";
                        }
                    }
                    if(dataBiaya==null){
                        if(a.biayaizin!=null){
                            dataBiaya = parseFloat(a.biayaizin);
                        }else{
                            dataBiaya = "Informasi Belum Tersedia";
                        }
                    }
                    dataHTML += "</div><div class='card-footer'><div class='text-center'><button class='btn btn-info btnModalInfo' data-idIzin='"+a.idjenisizin+"' data-title='"+a.nama_izin_terbit+"' data-desc='"+descIzin+"' data-lokizin='"+a.lokasi_kewenangan+"' data-kesesuaian='"+a.kesesuaian_citata+"' data-jam='"+dataJam+"' data-biaya='"+dataBiaya+"' data-itbx='"+ITBX+"' data-idmatriks='"+a.idmatriks+"'>Lihat Detail</button></div>";
                    // '    <p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Perizinan : '+a.jenis_perizinan+'</li></ul></p>\n' +
                    
                    dataHTML += '  </div>\n' +
                    '</div>' +
                    '</div>';
                }
                
                
                var classCol;
                if(data.dataopr.length==1){
                    classCol = "col-md-12";
                }else if(data.dataopr.length=="2"){
                    classCol = "col-md-6";
                }else if(data.dataopr.length=="3"){
                    classCol = "col-md-4";
                }else if(data.dataopr.length=="4" || data.dataopr.length > "4"){
                    classCol = "col-md-4";
                }
                var izinDataDetail = [];
                for(var i = 0; i < data.dataopr.length; i++) {
                    var a = data.dataopr[i];
                    tahapIzinStat = a.tahapanIzin;
                    var abc = a.itbx_detail;
                    if(abc!=null){
                        
                        var cde = abc.split(":");
                    }
                    izinDataDetail.push({idIzin: data.dataopr.idIzin,itbx: a.itbx});
                    dataITBX = '',
                    itbxKet = '',
                    ITBX = '';
                    
                    if (a.itbx == 'I'){ITBX='I : Diizinkan';}
                    else if(a.itbx == 'T'){ITBX='T : Diizinkan Terbatas';}
                    else if(a.itbx == 'B'){ITBX='B : Diizinkan Bersyarat';}
                    else if(a.itbx == 'TB'){ITBX='TB : Diizinkan Terbatas Bersyarat';}
                    else if(a.itbx == 'X'){ITBX='X : Tidak Diizinkan';}
                    if(abc==null){
                        dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                    }else{
                        
                        if(cde.length>1){
                            
                            var itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>";
                            if(cde.length==2){
                                itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length)+"</li>";
                            }
                            var itbxDoubleSpasial = "<li class='licardITBX'>"+cde[1].substr(cde[1].length - 2, 1) + " :" + cde[2]+"</li>";
                        }
                        
                        if (cde.length == 1) {
                            dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                        }else if(cde.length==2){
                            dataITBX += itbxSingleSpasial;
                        }
                        else if(cde.length==3){
                            dataITBX += itbxSingleSpasial+
                            itbxDoubleSpasial;
                        }else if(cde.length==4){
                            dataITBX += itbxSingleSpasial+
                            itbxDoubleSpasial+
                            "<li class='licardITBX'>"+cde[2].substr(cde[2].length - 2, 1) + " :" + cde[3]+"</li>";
                        }
                    }
                    
                    
                    dataHTML += '<div class="'+classCol+' mb-3">'+
                    '<div class="card text-center" style="height:100%;">\n' +
                    // '  <div class="card-header font-weight-bold">\n' + ITBX + '</div>\n' +
                    // '  <div class="card-header font-weight-bold" onclick="clickKeprofesian('+a.idIzin+')">' +
                    // <input type="checkbox" class="selIzinNew" value="'+a.idIzin+'">
                    // 'Izin Keprofesian</div>\n' +
                    '  <div class="card-body text-left">\n'+
                    '     <b>Jenis Izin</b> <br><p class="card-text text-center"><ul class="ulcardITBX"></b>'+a.nama_izin_terbit+'</b></ul></p>\n';
                    // '     <b>Perizinan berdasarkan Zona '+localStorage.getItem('subzona')+'</b>' +
                    // '<br><p class="card-text text-center"><ul class="ulcardITBX">'+dataITBX+'</ul></p>\n';
                    
                    
                    // if(a.itbx=="X"){
                    //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Keterangan : Profesi ini tidak dapat diajukan di zona yang dipilih</b></li></ul></p>\n';
                    // }else{
                    //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Profesi ini dapat diajukan di zona yang dipilih</li></ul></p>\n';
                    // }
                    var descIzin = a.deskripsi_izin,
                            dataJam = a.total_waktu,
                            dataBiaya = a.biaya
                            ;
                    if(descIzin==null){
                        descIzin = a.nama_izin_terbit;
                    }
                    if(dataJam==null){
                        if(a.waktu_penyelesaian!=null){
                            dataJam = a.waktu_penyelesaian + " " + a.tipe_waktu;
                        }else{
                            dataJam = "Informasi Belum Tersedia";
                        }
                    }
                    if(dataBiaya==null){
                        if(a.biayaizin!=null){
                            dataBiaya = parseFloat(a.biayaizin);
                        }else{
                            dataBiaya = "Informasi Belum Tersedia";
                        }
                    }
                    dataHTML += "</div><div class='card-footer'><div class='text-center'><button class='btn btn-info btnModalInfo' data-idIzin='"+a.idjenisizin+"' data-title='"+a.nama_izin_terbit+"' data-desc='"+descIzin+"' data-lokizin='"+a.lokasi_kewenangan+"' data-kesesuaian='"+a.kesesuaian_citata+"' data-jam='"+dataJam+"' data-biaya='"+dataBiaya+"' data-itbx='"+ITBX+"' data-idmatriks='"+a.idmatriks+"'>Lihat Detail</button></div>";
                    // '    <p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Perizinan : '+a.jenis_perizinan+'</li></ul></p>\n' +
                    
                    dataHTML += '  </div>\n' +
                    '</div>' +
                    '</div>';
                }
                
                localStorage.setItem('izinDataDetail',izinDataDetail);
                
                $('#recList').html(dataHTML);
                
                $(".btnModalInfo").click(function(e){
                    e.preventDefault();
                    $('#textTitlePrint').html($(this).data('title'));
                    
                    
                    var bt = $(this),
                    idPerizinan = bt.data('idizin'),
                    title = bt.data('title'),
                    desc = bt.data('desc').replace("[|]",","),
                    lok_kewenangan = bt.data('lokizin'),
                    kesesuaian_citata = bt.data('kesesuaian'),
                    jam = bt.data('jam'),
                    biaya = bt.data('biaya'),
                    itbx,
                    zona = localStorage.getItem('subzona'),
                    stat;
                    $.ajax({
                        url: api_url + 'MatriksCtrl/findById',
                        data: {id:idPerizinan,subzona:localStorage.getItem('subzona'),jenis_izin:5},
                        method: 'GET',
                        dataType: 'json',
                        success:function(data){
                            if(data.rowCount!=0){
                                var el = data.row[0];
                                
                                idmatriks = bt.data('idmatriks'),
                                idPerizinan = el.idIzin;
                                othername = el.nama_lain_perizinan;
                                itbx = el.itbx;
                                var ITBX;
                                var bangunanizin = [];
                                if (itbx == 'I'){ITBX='I : Diizinkan';}
                                else if(itbx == 'T'){ITBX='T : Diizinkan Terbatas';}
                                else if(itbx == 'B'){ITBX='B : Diizinkan Bersyarat';}
                                else if(itbx == 'TB'){ITBX='TB : Diizinkan Terbatas Bersyarat';}
                                else if(itbx == 'X'){ITBX='X : Tidak Diizinkan';}
                                
                                $.ajax({
                                    url: api_url + 'Perizinan/itbxSpasial',
                                    data: {idizin:idPerizinan,subzona:localStorage.getItem('subzona'),jenis_izin:3},
                                    method: 'GET',
                                    dataType: 'json',
                                    success:function(data){
                                        var dats = data.row;
                                        var it = [],t = [],b = [];
                                        for(var i = 0; i < dats.length; i++) {
                                            // debugger;
                                            // console.log(dats[i]);
                                            // bangunanizin = dats[i].itbx_detail;
                                            if(dats[i].itbx == 'B') {
                                                b.push(dats[i].itbx_detail);
                                            }
                                            if(dats[i].itbx == 'I') {
                                                it.push(dats[i].itbx_detail);
                                            }
                                            if(dats[i].itbx == 'T') {
                                                t.push(dats[i].itbx_detail);
                                            }
                                        }
                                        // modalOpenDetail(idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya,'anjing',);
                                        getTahapan(idmatriks,idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya,b,it,t);
                                    }
                                });
                            }else{
                                swal.fire({
                                    type: 'error',
                                    title: "Oops",
                                    text: 'Cannot fetch data detail'
                                });
                            }
                        }
                    })
                })
                
            }else{
                $("#recList").html('<div class="card text-center w-100">' +
                '<div class="card-body text-center">' +
                'Data yang Anda masukkan tidak cocok dengan kriteria Izin Mendirikan Bangunan, Silakan masukkan data dengan kriteria sebagai berikut :' +
                '<ul class="list-group list-group-flush">\n' +
                '  <li class="list-group-item">' +
                '       <span>IMB A</span> : Bangunan Non-rumah tinggal jumlah lantai > 8 lantai' +
                '   </li>\n' +
                '  <li class="list-group-item">' +
                '       <span>IMB B</span> : Bangunan Non-rumah tinggal jumlah lantai < 8 lantai; Rumah Tinggal Pemugaran Cagar Budaya Golongan A' +
                '   </li>\n' +
                '  <li class="list-group-item">' +
                '       <span>IMB C</span> : Bangunan Rumah tinggal luas tanah ≥ 100 m² , kondisi tanah tidak harus kosong, dan jumlah lantai s.d. 3 lantai; Pemugaran Cagar Budaya Golongan B dan C' +
                '   </li>\n' +
                '  <li class="list-group-item">' +
                '       <span>IMB D</span> : Bangunan Rumah tinggal luas tanah < 100 m², kondisi tanah kosong atau di atasnya terdapat bangunan tua yang akan dibongkar, dan jumlah lantai s.d 2 lantai' +
                '   </li>\n' +
                '</ul>' +
                '</div>' +
                '</div>');
                
            }
        }
    });
}

function getTahapan(id,idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya,b,it,t) {
    var getLocalData = JSON.parse(localStorage.getItem('dataReqPermission'));
    var dt = '';
    if(getLocalData!=null){
        dt = {idmatriks:id,luas_tanah:getLocalData[0].luas_tanah,luas_tanah_renc:getLocalData[0].luas_tanah_renc,idIzin:getLocalData[0].idIzin}
    }
    $.ajax({
        url: api_url + 'Perizinan/getTahapanIzin?idmatriks='+id,
        dataType: 'json',
        method: 'GET',
        data:dt,
        success:function(data) {
            modalOpenDetail(idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya,data.data,b,it,t);
        }
    })
}

function detailTahapIzin(kode,title,desc,jam,lok_kewenangan,kesesuaian_citata,biaya,idmatriks,jenisnya) {
    $.ajax({
        url: api_url + 'MatriksCtrl/findById',
        data: {code:kode,subzona:localStorage.getItem('subzona'),jenis_izin:jenisnya},
        method: 'GET',
        async: false,
        dataType: 'json',
        success:function(data){
            if(data.rowCount!=0){
                var el = data.row[0];
                idPerizinan = el.idIzin;
                othername = el.nama_lain_perizinan;
                zona = localStorage.getItem('subzona');
                itbx = el.itbx;
                othername = '';
                var ITBX;
                if (itbx == 'I'){ITBX='I : Diizinkan';}
                else if(itbx == 'T'){ITBX='T : Diizinkan Terbatas';}
                else if(itbx == 'B'){ITBX='B : Diizinkan Bersyarat';}
                else if(itbx == 'TB'){ITBX='TB : Diizinkan Terbatas Bersyarat';}
                else if(itbx == 'X'){ITBX='X : Tidak Diizinkan';}
                
                $.ajax({
                    url: api_url + 'Perizinan/itbxSpasial',
                    data: {idizin:idPerizinan,subzona:localStorage.getItem('subzona'),jenis_izin:5},
                    method: 'GET',
                    dataType: 'json',
                    success:function(data){
                        var dats = data.row;
                        var it = [],t = [],b = [];
                        for(var i = 0; i < dats.length; i++) {
                            // debugger;
                            // console.log(dats[i]);
                            // bangunanizin = dats[i].itbx_detail;
                            if(dats[i].itbx == 'B') {
                                b.push(dats[i].itbx_detail);
                            }
                            if(dats[i].itbx == 'I') {
                                it.push(dats[i].itbx_detail);
                            }
                            if(dats[i].itbx == 'T') {
                                t.push(dats[i].itbx_detail);
                            }
                        }
                        getTahapan(idmatriks,idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya,b,it,t);
                    }
                });
                
            }else{
                swal.fire({
                    type: 'error',
                    title: "Oops",
                    text: 'Cannot fetch data detail'
                });
            }
        }
    })
}

function modalOpenDetail(idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya,tahapIzinNya, itb,it,t) {
    // debugger
    var getBersyarat = localStorage.getItem('b_bersyarat'),
    getIzinkan = localStorage.getItem('b_izinkan'),
    getTerbatas = localStorage.getItem('b_terbatas'),
    getTerbatasB = localStorage.getItem('b_terbatasbersyarat'),
    content = '',
    ctTable = '',
    // jam = 'Informasi belum tersedia',
    textTitleWaktu = '';
    
    $.ajax({
        url: api_url + 'cronJobs/getPersyaratan?idIzin='+idPerizinan,
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var tableTr = "";
            var no = 1;
            for (var i in data) {
                // nonew = (data[i].child.length > 0)?no:nochild;
                if(data[i].child.length>0){
                    nonya = "<td></td>";
                }else{
                    nonya = "<td>" + no + "</td>";
                }
                tableTr += "<tr>"+nonya+"<td>" + data[i].persyaratan ;
                if (data[i].child.length > 0) {
                    tableTr += " : ";
                    for (var ch in data[i].child) {
                        var substrsymbol = data[i].child[ch].persyaratan.replace("?","•");
                        var substrsymbolnew = substrsymbol.replace("#","•");
                        tableTr += "<br>" + substrsymbolnew + "<br>";
                    }
                    // no = '';
                }else{
                    no += 1;
                }
                tableTr += "</td></tr>";
            }
            var contentTahapIzin = '',
            tahapBeluma = [];
            tahapBelums = [];
            // debugger;
            if(tahapIzinNya!='anjing'){
                for (var i in tahapIzinNya) {
                    var datTahapIzin = tahapIzinNya[i];
                    if (datTahapIzin.status_tahap == 1) {
                        tahapBelums.push(datTahapIzin);
                    }else if (datTahapIzin.status_tahap == 0){
                        tahapBeluma.push(datTahapIzin);
                    }
                    
                }
            }else{
                for (var i in tahapIzinStat) {
                    var datTahapIzin = tahapIzinStat[i];
                    if (datTahapIzin.status_tahap == 1) {
                        tahapBelums.push(datTahapIzin);
                    }else if (datTahapIzin.status_tahap == 0){
                        tahapBeluma.push(datTahapIzin);
                    }
                    
                }
            }
            
            contentTahapIzin += '<div class="row mt-3"><div class="col-md-6 mb-4"><div class="isiTxtIzin mb-2">Izin tahap sebelumnya yg perlu dilengkapi</div><ul class="list-group list-group-flush">\n';
            // debugger;
            for(var xx in tahapBeluma){
                if(tahapBeluma[xx].data.length>0){
                    
                    contentTahapIzin += '  <button type="button" class="list-group-item list-group-item-action" onclick="detailTahapIzin(\''+tahapBeluma[xx].data[0].kode_izin+'\',\''+tahapBeluma[xx].data[0].nama_izin_terbit+'\',\''+tahapBeluma[xx].data[0].deskripsi_izin+'\',\''+tahapBeluma[xx].data[0].total_waktu+'\',\''+tahapBeluma[xx].data[0].lokasi_kewenangan+'\',\''+tahapBeluma[xx].data[0].kesesuaian_citata+'\',\''+tahapBeluma[xx].data[0].biaya+'\',\''+tahapBeluma[xx].data[0].idmatriks+'\',\''+tahapBeluma[xx].data[0].id_tujuan_izin+'\')">'+tahapBeluma[xx].nama_izin_tahap+'</button>\n';
                }
            }
            contentTahapIzin += '</ul></div>';
            
            contentTahapIzin += '<div class="col-md-6 mb-4"><div class="isiTxtIzin mb-2">Izin yang dapat dilakukan selanjutnya</div><ul class="list-group list-group-flush">\n' ;
            for(var x in tahapBelums){
                if(tahapBelums[x].data.length>0){
                    
                    contentTahapIzin += '  <button type="button" class="list-group-item list-group-item-action" onclick="detailTahapIzin(\''+tahapBelums[x].data[0].kode_izin+'\',\''+tahapBelums[x].data[0].nama_izin_terbit+'\',\''+tahapBelums[x].data[0].deskripsi_izin+'\',\''+tahapBelums[x].data[0].total_waktu+'\',\''+tahapBelums[x].data[0].lokasi_kewenangan+'\',\''+tahapBelums[x].data[0].kesesuaian_citata+'\',\''+tahapBelums[x].data[0].biaya+'\',\''+tahapBelums[x].data[0].idmatriks+'\',\''+tahapBelums[x].data[0].id_tujuan_izin+'\')">'+tahapBelums[x].nama_izin_tahap+'</button>\n';
                }
            }
            contentTahapIzin += '</ul></div></div>';
            
            // if(othername != null || othername != ''){
            //     var imb = othername.split(' '),
            //     imbfix = imb[0].toLowerCase();
                
            //     $('#imgAlurIzin').attr('src', 'assets/img/alur/'+imbfix+'/'+imbfix+'.png');
            //     $('#urlImgnya').attr('href', 'assets/img/alur/'+imbfix+'/'+imbfix+'.png');
            // }else {
            //     $('#imgAlurIzin').html('<div>Gambar tidak tersedia.</div>');
            // }
            // var imb = '',imbfix='',othername.split(' '), imbfix = imb[0].toLowerCase()
            var getLocalItem = JSON.parse(localStorage.getItem('dataReqPermission')),
            perubahan = getLocalItem[0].luas_tanah_renc - getLocalItem[0].luas_tanah;
            
            
            ctTable += tableTr;
            if(kesesuaian_citata=='Y'){
                kesCitata = "Sesuai";
            }else{
                kesCitata = "Tidak Sesuai";
            }
            
            var bangunanizin = '';
            var bangunanB = 'Bangunan Bersyarat : ';
            var bangunanI = 'Bangunan Diizinkan : ';
            var bangunanT = 'Bangunan Terbatas Bersyarat : ';
            
            for(var i in itb){
                bangunanB += itb[i]+',';
                
            }
            bangunanB.substr(0,bangunanB.length -1);
            
            for(var i in it){
                bangunanI += it[i]+',';
                
            }
            bangunanI.substr(0,bangunanI.length -1);
            
            for(var i in t){
                bangunanT += t[i]+',';
                
            }
            bangunanT.substr(0,bangunanT.length -1);
            
            if(t.length<1){
                bangunanT += " - ";
            }
            if(itb.length<1){
                bangunanB += " - ";
            }
            if(it.length<1){
                bangunanI += " - ";
            }
            bangunanizin += bangunanI + "<br>";
            bangunanizin += bangunanT + "<br>";
            bangunanizin += bangunanB + "<br>";
            
            var gSyarat = getBersyarat.split(','),
            gIzinkan = getIzinkan.split(','),
            gTerbatas = getTerbatas.split(','),
            gTerbatasB = getTerbatasB.split(',');
            
            var listSyarat = "<ol>";
            for (var ia in gSyarat) {
                listSyarat += "<li>"+gSyarat[ia]+"</li>";
            }
            listSyarat += "</ol>";
            
            var listIzinkan = "<ol>";
            for (var ib in gIzinkan) {
                listIzinkan += "<li>"+gIzinkan[ib]+"</li>";
            }
            listIzinkan += "</ol>";
            
            var listTerbatas = "<ol>";
            for (var ic in gTerbatas) {
                listTerbatas += "<li>"+gTerbatas[ic]+"</li>";
            }
            listTerbatas += "</ol>";
            
            var listTerbatasB = "<ol>";
            for (var id in gTerbatasB) {
                listTerbatasB +="<li>"+gTerbatasB[id]+"</li>";
            }
            listTerbatasB += "</ol>";
            
            content += '<div class="row">\n' +
            '    <div class="col-md-12">\n' +
            '        <div class="accordion" id="accordionExample">\n' +
            '            <div class="card">\n' +
            '                <div class="card-header" id="headingOne">\n' +
            '                    <h2 class="mb-0">\n' +
            '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">\n' +
            '                            Deskripsi Izin\n' +
            '                        </button>\n' +
            '                    </h2>\n' +
            '                </div>\n' +
            '                <div id="collapseOne" class="collapse printAct" aria-labelledby="headingOne" data-parent="#accordionExample">\n' +
            '                    <div class="card-body">\n' +
            '                        <div class="row">\n' +
            '                            <div class="col-md-12">\n' +
            '                                <div class="text-center border\n' +
            '                                p-2">\n' +
            '                                   <p class="isiTxtIzin"><span>'+desc +'</span></p>' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            \n' +
            '            <div class="card">\n' +
            '                <div class="card-header" id="headingTwo">\n' +
            '                    <h2 class="mb-0">\n' +
            '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">\n' +
            '                            Lokasi Pengurusan Izin\n' +
            '                        </button>\n' +
            '                    </h2>\n' +
            '                </div>\n' +
            '                <div id="collapseTwo" class="collapse printAct" aria-labelledby="headingTwo" data-parent="#accordionExample">\n' +
            '                    <div class="card-body">\n' +
            '                        <div class="row">\n' +
            '                            <div class="col-md-12">\n' +
            '                                <div class="text-center border\n' +
            '                                p-2">\n' +
            '                                   <p class="isiTxtIzin"><span>Terdapat di '+lok_kewenangan +'</span></p>' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            \n' +
            '            <div class="card">\n' +
            '                <div class="card-header" id="headingThree">\n' +
            '                    <h2 class="mb-0">\n' +
            '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">\n' +
            '                            Persyaratan\n' +
            '                        </button>\n' +
            '                    </h2>\n' +
            '                </div>\n' +
            '                <div id="collapseThree" class="collapse printAct" aria-labelledby="headingThree" data-parent="#accordionExample">\n' +
            '                    <div class="card-body">\n' +
            '                        <div class="row">\n' +
            '                            <div class="col-md-12">\n' +
            '                                <div class="text-center">\n' +
            '<table class="table table-striped table-bordered text-left" id="dataTabelPrint" border="1" cellpadding="3">' +
            '<thead>' +
            '<tr>' +
            '<th scope="col">No.</th>\n' +
            '<th scope="col">Persyaratan</th>\n' +
            '</tr>' +
            '</thead>' +
            '<tbody>' +
            ctTable +
            '</tbody>' +
            '</table>' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            \n' +
            '            <div class="card">\n' +
            '                <div class="card-header" id="headingEight">\n' +
            '                    <h2 class="mb-0">\n' +
            '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">\n' +
            '                            Tahapan Izin\n' +
            '                        </button>\n' +
            '                    </h2>\n' +
            '                </div>\n' +
            '                <div id="collapseEight" class="collapse printAct" aria-labelledby="headingEight" data-parent="#accordionExample">\n' +
            '                    <div class="card-body">\n' +
            '                        <div class="row">\n' +
            '                            <div class="col-md-12">\n' +
            '                                <div class="text-center border p-2">\n' +
            '                                   <div class="mb-2 isiTxtIzin">Alur Proses Buat Perizinan</div>' +
            '                                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imgTahapIzin">Lihat gambar</button>' +
            contentTahapIzin +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            \n' +
            '            <div class="card">\n' +
            '                <div class="card-header" id="headingNine">\n' +
            '                    <h2 class="mb-0">\n' +
            '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">\n' +
            '                            Durasi & Biaya\n' +
            '                        </button>\n' +
            '                    </h2>\n' +
            '                </div>\n' +
            '                <div id="collapseNine" class="collapse printAct" aria-labelledby="headingNine" data-parent="#accordionExample">\n' +
            '                    <div class="card-body">\n' +
            '                        <div class="row">\n' +
            '                            <div class="col-md-12">\n' +
            '                                <div class="text-center border\n' +
            '                                p-2">\n' +
            '                                   <p class="isiTxtIzin"><span>Waktu Penyelesaian : '+jam+'</span><span class="d-block">Biaya yang diperlukan : '+biaya+'</span></p>' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            \n' +
            '            <div class="card">\n' +
            '                <div class="card-header" id="headingTen">\n' +
            '                    <h2 class="mb-0">\n' +
            '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="true" aria-controls="collapseTen">\n' +
            '                            Kontak Kami\n' +
            '                        </button>\n' +
            '                    </h2>\n' +
            '                </div>\n' +
            '                <div id="collapseTen" class="collapse printAct" aria-labelledby="headingTen" data-parent="#accordionExample">\n' +
            '                    <div class="card-body">\n' +
            '                        <div class="row">\n' +
            '                            <div class="col-md-12">\n' +
            '                                <div class="text-center border\n' +
            '                                p-2">\n' +
            '                                   <p class="isiTxtIzin"><span>SEGERA HADIR</span></p>' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';
            
            $('#izinContents').html(content);
            $('#izinTitles').html(title);
            
            // chartIzin(title);
            $('#modalDetailIzin').modal('show');
        }
    });
}    

function print() {
    printJS({
    printable: 'izinContents',
    type: 'html',
    targetStyles: ['*']
 })
}