var api_url = 'http://rest-api.pkkmart.com/';
// var api_url = 'http://localhost/rest-api/';

$(document).ready(function(){
    // checkHistoryStep();
    // modalOpenDetail();

    // getKategoriIzin('4');

    //Step Pertama
    $('#btnConfJenisIzin').on('click', function () {
        var jenIzin = $('#jen_permohonan').val(),
            katIzin = $('#kat_permohonan').val(),
            reqData = JSON.parse(localStorage.getItem("dataReqPermission"));
            
        reqData[0]['jenisIzin'] = jenIzin;
        reqData[0]['kategoriIzin'] = katIzin;
        localStorage.setItem("dataReqPermission", JSON.stringify(reqData));

        localStorage.setItem("step", "secondStep");
        localStorage.setItem("prevStep", "firstStep");
        customNextStep($('#firstStep'), $('#secondStep'));
    });

    //Step Kedua
    $('#btnMapData').on('click', function () {
        if (localStorage.getItem('b_izinkan') === null){
            Swal.fire({
                type: 'error',
                title: 'Terjadi Kesalahan',
                text: 'Harap pilih lokasi bangunan pada peta.'
            })
        }else{
            checkResume();
            localStorage.setItem("step", "thirdStep");
            localStorage.setItem("prevStep", 'secondStep');
            customNextStep($('#secondStep'), $('#thirdStep'));
        }
    });

    //PRINT FUNCTION
    $('#printOutDetail').on('click', function () {
        var divToPrint = document.getElementById('izinContents');
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table {\n' +
            '    width: 100%;\n' +
            '    margin-bottom: 1rem;\n' +
            '    color: #212529;\n' +
            '}' +
            'table th, table td {' +
            '    padding: .75rem;\n' +
            '    vertical-align: top;\n' +
            '    border-top: 1px solid #dee2e6;' +
            '}' +
            'table thead th {\n' +
            '    vertical-align: bottom;\n' +
            '    border-bottom: 2px solid #dee2e6;\n' +
            '}' +
            'table tbody tr:nth-of-type(odd) {\n' +
            '    background-color: rgba(0,0,0,.05);\n' +
            '}' +
            'th {\n' +
            '    text-align: inherit;\n' +
            '}' +
            '</style>' +
            '<div style="text-align: center;color:#333;">' +
            '<h2 id="textTitlePrint">Detail Permohonan Izin</h2></div>';
        htmlToPrint += divToPrint.outerHTML;
        newWin = window.open("", 'blank_');
        newWin.document.write(htmlToPrint);
        newWin.print();
        newWin.close();
    });

//    Step keempat
    $('#btnConfSumm').on('click', function () {
        var type = localStorage.getItem('typeStart'),
            dataUser = localStorage.getItem('dataReqPermission'),
            dataRecList = '',
            dataHTML = '';

        $.ajax({
            url: api_url + "MatriksCtrl/getRekomendasiIzin/",
            method: 'post',
            data: {dataUser: dataUser,subzona: localStorage.getItem('subzona'),jenisIzin: 4},
            dataType: 'json',
            beforeSend: function(){

            },
            success: function(data){
                if (data.success) {
                    var classCol;
                    if(data.row.length==1){
                        classCol = "col-md-12";
                    }else if(data.row.length=="2"){
                        classCol = "col-md-6";
                    }else if(data.row.length=="3"){
                        classCol = "col-md-4";
                    }else if(data.row.length=="4" || data.row.length > "4"){
                        classCol = "col-md-4";
                    }
                    var izinDataDetail = [];
                    for(var i = 0; i < data.row.length; i++) {
                        var a = data.row[i];
                        var abc = a.itbx_detail;
                        if(abc!=null){

                            var cde = abc.split(":");
                        }
                        izinDataDetail.push({idIzin: data.row.idIzin,itbx: a.itbx});
                        dataITBX = '',
                            itbxKet = '',
                            ITBX = '';

                        if (a.itbx == 'I'){ITBX='I : Diizinkan';}
                        else if(a.itbx == 'T'){ITBX='T : Diizinkan Terbatas';}
                        else if(a.itbx == 'B'){ITBX='B : Diizinkan Bersyarat';}
                        else if(a.itbx == 'TB'){ITBX='TB : Diizinkan Terbatas Bersyarat';}
                        else if(a.itbx == 'X'){ITBX='X : Tidak Diizinkan';}
                        if(abc==null){
                            dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                        }else{

                            if(cde.length>1){

                                var itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>";
                                if(cde.length==2){
                                    itbxSingleSpasial = "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length)+"</li>";
                                }
                                var itbxDoubleSpasial = "<li class='licardITBX'>"+cde[1].substr(cde[1].length - 2, 1) + " :" + cde[2]+"</li>";
                            }

                            if (cde.length == 1) {
                                dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                            }else if(cde.length==2){
                                dataITBX += itbxSingleSpasial;
                            }
                            else if(cde.length==3){
                                dataITBX += itbxSingleSpasial+
                                    itbxDoubleSpasial;
                            }else if(cde.length==4){
                                dataITBX += itbxSingleSpasial+
                                    itbxDoubleSpasial+
                                    "<li class='licardITBX'>"+cde[2].substr(cde[2].length - 2, 1) + " :" + cde[3]+"</li>";
                            }
                        }

                        dataHTML += '<div class="'+classCol+' mb-3">'+
                            '<div class="card text-center" style="height:100%;">\n' +
                            // '  <div class="card-header font-weight-bold">\n' + ITBX + '</div>\n' +
                            // '  <div class="card-header font-weight-bold" onclick="clickKeprofesian('+a.idIzin+')">' +
                            // <input type="checkbox" class="selIzinNew" value="'+a.idIzin+'">
                            // 'Izin Keprofesian</div>\n' +
                            '  <div class="card-body text-left">\n'+
                            '     <b>Jenis Izin</b> <br><p class="card-text text-center"><ul class="ulcardITBX"></b>'+a.jenis_perizinan+'</b></ul></p>\n';
                            // '     <b>Perizinan berdasarkan Zona '+localStorage.getItem('subzona')+'</b>' +
                            // '<br><p class="card-text text-center"><ul class="ulcardITBX">'+dataITBX+'</ul></p>\n';


                        // if(a.itbx=="X"){
                        //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Keterangan : Profesi ini tidak dapat diajukan di zona yang dipilih</b></li></ul></p>\n';
                        // }else{
                        //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Profesi ini dapat diajukan di zona yang dipilih</li></ul></p>\n';
                        // }
                        dataHTML += "</div><div class='card-footer'><div class='text-center'><button class='btn btn-info btnModalInfo' data-idIzin='"+a.idIzin+"' data-title='"+a.jenis_perizinan+"' data-jam='"+a.tipe_penyelesaian+"' data-angka='"+a.waktu_penyelesaian+"' data-status='"+a.status_izin_perizinan+"' data-itbx='"+ITBX+"'>Lihat Detail</button></div>";
                        // '    <p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Perizinan : '+a.jenis_perizinan+'</li></ul></p>\n' +

                        dataHTML += '  </div>\n' +
                            '</div>' +
                            '</div>';
                    }

                    localStorage.setItem('izinDataDetail',izinDataDetail);

                    $('#recList').html(dataHTML);
                    
                    $(".btnModalInfo").click(function(e){
                        e.preventDefault();
                        $('#textTitlePrint').html($(this).data('title'));
                        var bt = $(this),
                            idPerizinan = bt.data('idizin'),
                            title = bt.data('title'),
                            berapa = bt.data('angka'),
                            jam = bt.data('jam'),
                            itbx = bt.data('itbx'),
                            zona = localStorage.getItem('subzona'),
                            stat = bt.data('status');

                        modalOpenDetail(idPerizinan, title, berapa, jam, itbx, zona, stat);


                    })

                }else{
                    $("#recList").html('<div class="card text-center w-100"><div class="card-body text-center">Tidak dapat mengajukan izin apapun berdasarkan informasi yang Anda masukan</div></div>');
                }


                localStorage.setItem('step', 'fourthStep');
                customNextStep($('#thirdStep'), $('#fourthStep'));
            },
            error: function(xhr){
                Swal.fire({
                    type: 'error',
                    title: 'Terjadi Kesalahan',
                    text: xhr.status +" " + xhr.statusText
                })
            }
        });

        $('#recList').html(dataRecList);

        localStorage.setItem("step", "fourthStep");
        localStorage.setItem("prevStep", 'thirdStep');
        customNextStep($('#thirdStep'), $('#fourthStep'));
    });

    //STEP KELIMA
    $('#btnToPlea').on('click', function () {
        // var type
        savePermohonanUsaha();


        localStorage.setItem("step", "twelfthStep");
        customNextStep($('#tenthStep'), $('#twelfthStep'));
    });
    
    $('.btnRefresh').on('click', function() {
        location.reload();
    });

    //Back
    $(".previous").on('click', function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    // NEXT IZIN
    $('.doneIzinProf').on('click', function () {
        localStorage.setItem("step", "fifthStep");
        localStorage.setItem("prevStep", "fourthStep");
        customNextStep($('#fourthStep'), $('#fifthStep'));
    });

});

function modalOpenDetail(a,b,c,d,e,f,g) {
    var getBersyarat = localStorage.getItem('b_bersyarat'),
        getIzinkan = localStorage.getItem('b_izinkan'),
        getTerbatas = localStorage.getItem('b_terbatas'),
        getTerbatasB = localStorage.getItem('b_terbatasbersyarat'),
        content = '',
        tableTr = "",
        ctTable = '',
        jam = 'Informasi belum tersedia',
        waktu = '-';
        textTitleWaktu ='';

    if (c) {
        jam = c;
        textTitleWaktu = '<span class="d-block h5">Izin Selesai Dalam</span><br>';
    }else{
        jam = "Informasi belum tersedia";
    }
    if (d) {
        waktu = d;
    }
    else{
        waktu = "";
    }

    $.ajax({
        url: api_url + 'cronJobs/getPersyaratan?idIzin='+a,
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var tableTr = "";
            var no = 1,
            nochild = '';
            for (var i in data) {
                // nonew = (data[i].child.length > 0)?no:nochild;
                if(data[i].child.length>0){
                    nonya = "<td></td>";
                }else{
                    nonya = "<td>" + no + "</td>";
                }
                tableTr += "<tr>"+nonya+"<td>" + data[i].persyaratan ;
                if (data[i].child.length > 0) {
                    tableTr += " : ";
                    for (var ch in data[i].child) {
                        var substrsymbol = data[i].child[ch].persyaratan.replace("?","•");
                        var substrsymbol = data[i].child[ch].persyaratan.replace("#","•");
                        tableTr += "<br>" + substrsymbol + "<br>";
                    }
                    // no = '';
                }else{
                    no += 1;
                }
                tableTr += "</td></tr>";
                // if (data[i].child.length > 0) {
                //     for (var ch in data[i].child) {
                //         tableTr += "<tr><td></td><td>" + data[i].child[ch].persyaratan + "</td></tr>";
                //     }
                // }
            }
            ctTable += tableTr;

            content += '<div class="row">' +
                '<div class="col-md-12">' +
                '<div class="accordion" id="accordionExample">\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingOne">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">\n' +
                '          Waktu Perizinan' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '\n' +
                '    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">\n' +
                '      <div class="card-body">' +
                '       <div class="row">\n' +
                '    <div class="col-md-12">\n' +
                '    <div class="text-center border\n' +
                ' p-2">\n' +
                '        '+textTitleWaktu+'\n' +
                '    <span style="text-align: center;font-size: 14pt;font-weight: 500;letter-spacing: 1px;color: #59848c;">'+jam+'</span>\n' +
                '    <br><span style="text-align: center;font-size: 12pt;font-weight: 500;letter-spacing: 1px;color: #59848c;">'+waktu+'</span>\n' +
                '    </div>\n' +
                '    </div>' +
                '</div>' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingTwo">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">\n' +
                '          Kegiatan Diizinkan (ITBX)' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">\n' +
                '      <div class="card-body">\n' +
                '<div><p style="text-align: center;font-size: 14pt;font-weight: 500;letter-spacing: 1px;color: #59848c;"><span>Perizinan berdasarkan Zona : '+f+'</span><span class="d-block">'+e+'</span></p></div>' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingThree">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">\n' +
                '          Persyaratan' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">\n' +
                '      <div class="card-body">\n' +
                '<table class="table table-striped table-bordered text-left" id="dataTabelPrint" border="1" cellpadding="3">' +
                '<thead>' +
                '<tr>' +
                '                <th scope="col">No.</th>\n' +
                '                <th scope="col">Persyaratan</th>\n' +
                '</tr></thead>' +
                '<tbody>' +
                ctTable +
                '</tbody>' +
                '</table>' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '  <div class="card">\n' +
                '    <div class="card-header" id="headingFour">\n' +
                '      <h2 class="mb-0">\n' +
                '        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">\n' +
                '          Tahap Izin' +
                '        </button>\n' +
                '      </h2>\n' +
                '    </div>\n' +
                '    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">\n' +
                '      <div class="card-body">\n' +
                '        <div id="tahapanIzinGraph"></div><div id="keteranganTahapIzin"></div>' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>' +
                '</div></div>';
            $('#izinContents').html(content);
            $('#izinTitles').html(b);

            chartIzin(b);
            $('#modalDetailIzin').modal('show');
        }
    });
}

function clickUsaha(id) {
    var izinChecked = $('.selIzinNew').map(function() {
            if(this.checked)
                return this.value;
        }).get();
    var izinDataDetail = [];
    debugger;
    var dataReq = JSON.parse(localStorage.getItem('dataReqPermission'));

    dataReq[0].izinData = izinChecked;
    izinDataDetail.push({idIzin: izinChecked});

    localStorage.setItem('izinDataDetail',JSON.stringify(izinDataDetail));
    localStorage.setItem("dataReqPermission", JSON.stringify(dataReq));
}
function savePermohonanUsaha(){
    debugger;
    var type = localStorage.getItem('typeStart'),
        dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        dataArahan = JSON.parse(localStorage.getItem('dataArahan'));

    $.ajax({
        url: api_url + 'matriksCtrl/savePermohonanIzin',
        type: 'POST',
        dataType: 'json',
        data: {
            subzona: localStorage.getItem('subzona'),
            dataRegist: localStorage.getItem('dataReqPermission'),
            izinDataDetail: localStorage.getItem('izinDataDetail'),
            klb:localStorage.getItem('klb'),
            kdb:localStorage.getItem('kdb'),
            kdh:localStorage.getItem('kdh'),
            idArahan:localStorage.getItem('idArahan')},
        beforeSend:function() {
            // Swal.showLoading();
        },
        success:function(data) {
            $('#backPemohon').show();
            finalContent = "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email "+ dataUser[0].email +"</div>" +
                "<button type='button' class='btn rounded-0 btn--main' onclick='directLoginNew();'>Selesai</button>";
            sendEmailUsaha(data.token, data.npwp);

            $('#finalContent').html(finalContent);
        }
    });

}


function sendEmailUsaha(dtoken, dnpwp) {
    $.ajax({
        url: api_url + 'users/sendMailExceptIMB/',
        type: 'POST',
        dataType: 'json',
        data: {npwp: dnpwp, token: dtoken, pendaftaran: 'pendaftaran'},
        beforeSend: function () {
        },
        success: function (data) {
            // if(data.success){

            Swal.fire({
                type: 'success',
                title: 'Data dikirim ke email anda.',
                showConfirmButton: false,
                timer: 3000
            })
            // if (localStorage.getItem('typeStart') != 'buat_izin'){
            directLoginNew();
            // }
            // }
        }
    })
}

function clickKeprofesian(id) {
    var izinChecked = $('.selIzinNew').map(function() {
            if(this.checked)
                return this.value;
        }).get(),
        dataReq = JSON.parse(localStorage.getItem('dataReqPermission'));

    dataReq[0].izinData = izinChecked;
    localStorage.setItem("dataReqPermission", JSON.stringify(dataReq));
}

function checkResume() {
    var dataIzin = JSON.parse(localStorage.getItem('dataReqPermission')),
        dataJenis = '',
        mapData5 = localStorage.getItem('alamat'),
        mapData6 = localStorage.getItem('zona'),
        mapData7 = localStorage.getItem('subzona'),
        mapData8 = localStorage.getItem('idsubblok'),
        mapData9 = localStorage.getItem('kordinat');

    // if (dataIzin[0].jenisIzin == 1) { dataJenis = 'Perorangan' }
    // else if (dataIzin[0].jenisIzin == 2) { dataJenis = 'Utama' }

    $('#katIzin').html(dataIzin[0].kategoriIzin);
    $('#jenIzin').html(dataIzin[0].jenisIzin);
    $('#addressUser').html(mapData5);
    $('#ZonaUser').html(mapData6);
    $('#subzonaUser').html(mapData7);
    $('#idsubblok').html(mapData8);
    $('#kordinat').html(mapData9);
}

function checkHistoryStep() {
    var stepNum = localStorage.getItem("step");

    if (stepNum==null) {
        $("#firstStep").attr('style', 'display: block; left: 0%; opacity: 1;');
        return true;
    }else {
        checkResume();
        Swal.fire({
            title: 'Pemberitahuan',
            text: "Apakah anda ingin melanjutkan proses sebelumya?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#ff6921',
            cancelButtonColor: '#575757',
            confirmButtonText: 'Ya, Lanjutkan',
            cancelButtonText: 'Ulangi',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                // var array = ["first","second","third","fourth","sixth","seventh","last"];
                $(".stepContainer .stepWizard:not(:first-of-type)").map(function(el,i){

                    var stepNum = localStorage.getItem("step");
                    if(i.getAttribute("id")==stepNum){
                        $(".stepContainer #firstStep")[0].setAttribute("style","transition:ease 0.5s all;webkit-transition:ease 0.5s all;display: none; left: 0%; opacity: 0;");
                        i.setAttribute("style","display: block; left: 0%; opacity: 1;transition:ease 0.5s all;webkit-transition:ease 0.5s all;");
                    }else{
                        i.setAttribute("style","display: none; left: 0%; opacity: 0;");
                    }
                    // debugger;

                });

                // buat hide step tertentu
                var btnType = localStorage.getItem("typeIzin"),nonForm;
                if(btnType!==null || btnType!=="null"){
                    if(btnType==="perorangan"){
                        nonForm = $(".compForm, .govForm, .npwpI, .nipGov, .terkaitGov, .dirutComp");
                    }else if(btnType==="perusahaan"){
                        nonForm = $(".selfForm, .govForm, .npwpNik, .nipGov, .terkaitGov");
                    }else if(btnType==="pemerintahan" || btnType==="yayasan"){
                        nonForm = $(".selfForm, .compForm, .npwpNik, .npwpI, .dirutComp, .selectSelfComp");
                    }
                    nonForm.hide();
                }

                // buat set form field-field nya
                var datanya = JSON.parse(localStorage.getItem('dataReqPermission')),
                    typenya;
                if(btnType=="perorangan"){
                    typenya = "self";
                }else if(btnType=="perusahaan"){
                    typenya = "comp";
                    $("#dirutComp").val(datanya[0].dirut);
                    $("#npwpdirutComp").val(datanya[0].npwpdirut);
                }else{
                    $("#terkaitGov").val(datanya[0].izinterkait);
                    typenya = "gov";
                }
                $("."+typenya+"Form").val(datanya[0].namaPemohon);
                $("."+typenya+"Npwp").val(datanya[0].no_identitas);
                $("#phoneF").val(datanya[0].phone);
                $("#emailF").val(datanya[0].email);
                var mengurusSendiri = datanya[0].mengurus;
                $(".btn-pelayanan").each(function(el,i){
                    if(mengurusSendiri=="2"){
                        if(i.dataset.title=="N"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }else{
                        if(i.dataset.title=="Y"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }
                })


            }else{
                localStorage.clear();

                location.href='../';
            }
        })
    }
}

function getKategoriIzin(idIzin,type_izin){
    $.ajax({
        url: api_url + "MatriksCtrl/getKategoriIzin",
        method: 'GET',
        data: {idIzin: idIzin,type_izin:type_izin},
        dataType: 'json',
        success:function(dt){
            if(dt.rowCount>0){
                $("#kat_permohonan").html('');
                for(var i in dt.row){
                    const katIzin = dt.row[i].kategori_izin;
                    if(katIzin!=null){
                        $("#kat_permohonan").append('<option value="'+katIzin+'">'+katIzin+'</option>');
                    }
                }
            }else{

            }
        }
    })
}


function chartIzin(izin) {
    $.ajax({
        url: api_url + 'CronJobs/cekTahapan?typeizin=4&nama='+izin,
        type: 'GET',
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            if(data.length<1){
                $("#tahapanIzinGraph").html("<p style='text-align: center;font-size: 14pt;font-weight: 500;letter-spacing: 1px;color: #59848c;'><span>Tidak ada tahapan izin yang harus diselesaikan sebelumnya</span></p>");
            }else{
                $("#keteranganTahapIzin").html("<p style='text-align: center;font-size: 14pt;font-weight: 500;letter-spacing: 1px;color: #59848c;'><span>Warna Hijau : izin yang harus diselesaikan terlebih dahulu</span></p>");
            }

            genchart(data);

            // $("#tahapIzinUL").append("<li>"+data[0].tahapanfinal+"</li>");
            // var ht = "<li>Izin yang harus diselesaikan sebelumnya <ul>";
            // for(var i in data[0].tahapanul){
            //     const el = data[0].tahapanul[i];
            //     ht += "<li>"+el+"</li>";
            // }
            // ht += "</ul></li>";
            // $("#tahapIzinUL").append(ht);
        }
    })
}

function genchart(data) {
    var pushNodes = [{
        id: data[0].tahapanfinal,
        title: data[0].tahapanfinal,
        name: data[0].tahapanfinal,
        // image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2018/11/12132317/Grethe.jpg'
    }];

    for(var i in data[0].tahapanul){
        const el = data[0].tahapanul[i];
        
        var obj = {};
        obj['id'] = el;
        obj['name'] = el;
        obj['title'] = el;
        obj['layout'] = 'hanging';
        obj['column'] = data[0].tahapanul.length;
        pushNodes.push(obj);
    }
    Highcharts.chart('tahapanIzinGraph', {

        chart: {
          height: 300,
          inverted: true
        },
      
        title: {
            text: 'Tahapan ' + data[0].tahapanfinal
        },
      
        series: [{
          type: 'organization',
          name: 'Tahapan Izin',
          keys: ['from', 'to'],
          data: data[0].tahapannew,
          levels: [{
            level: 0,
            color: '#980104',
            dataLabels: {
            //   color: 'black'
                fontSize: 12,
            },
            height: 25
          }, {
            level: 1,
            color: '#359154',
            dataLabels: {
                fontSize: 12,
                //   color: 'black'
            },
            height: 25
          }, {
            level: 2,
            color: '#980104'
          }, {
            level: 4,
            color: '#359154'
          }],
          nodes: [pushNodes],
          colorByPoint: false,
          color: '#007ad0',
          dataLabels: {
            fontSize: 12,
            color: 'white'
          },
          borderColor: 'white',
          nodeWidth: 65
        }],
        tooltip: {
          outside: false
        },
        exporting: {
          allowHTML: true,
          sourceWidth: 800,
          sourceHeight: 600
        }
      
      });
}