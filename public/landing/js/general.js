var api_url = 'http://rest-api.pkkmart.com/';
// var api_url = 'http://192.168.100.9/rest-api/';

$(document).ready(function(){
    loadIzin();
    checkHistoryStep();

    //Back Step
    $('#backIzin').on('click', function () {

    });

    //STEP SATU
    $('.btnAppliData').on('click', function () {
        var btnType = $(this).data('type'),
            nonForm,
            setAppli,
            dataAppli = JSON.parse(localStorage.getItem("dataReqPermission"));

        if(btnType==="perorangan"){
            nonForm = $(".compForm, .govForm, .npwpI, .nipGov, .terkaitGov, .dirutComp");
            setAppli = 1;
        }else if(btnType==="perusahaan"){
            nonForm = $(".selfForm, .govForm, .npwpNik, .nipGov, .terkaitGov");
            setAppli = 2;
        }else if(btnType==="pemerintahan" || btnType==="yayasan"){
            nonForm = $(".selfForm, .compForm, .npwpNik, .npwpI, .dirutComp, .selectSelfComp");
            setAppli = 3;
        }
        nonForm.hide();

        dataAppli[0]['idPemohon'] = setAppli;
        localStorage.setItem("dataReqPermission", JSON.stringify(dataAppli));
        localStorage.setItem("typeIzin", btnType);

        if (localStorage.getItem('typeStart') == 'simulasi') {
            customNextStep($('#firstStep'), $('#thirdStep'));
            localStorage.setItem("step", 'thirdStep');
        }else{
            customNextStep($('#firstStep'), $('#secondStep'));
            localStorage.setItem("step", 'secondStep');
        }
        localStorage.setItem("prevStep", 'firstStep');
    });

    //STEP DUA
    $("#btnRegisPermission").on('click', function () {
        var dataRegis = JSON.parse(localStorage.getItem("dataReqPermission"));
        Swal.fire({
            text: "Apakah anda sudah pastikah bahwa data yang anda masukan benar ?",
            showCancelButton: true,
            confirmButtonColor: '#ff6704',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, sudah',
            confirmButtonClass: 'next',
            cancelButtonText: 'Belum'
        }).then((result) => {
            if (result.value) {
                var typePemohon;

                if(dataRegis[0]['idPemohon']==1){//self
                    typePemohon = "self";
                }else if(dataRegis[0]['idPemohon']==2){//company
                    typePemohon = "comp";
                    dataRegis[0]['dirut'] = $("#dirutComp").val();
                    dataRegis[0]['npwpdirut'] = $("#npwpdirutComp").val();
                    dataRegis[0]['jenisIdentitas'] = "3";
                }else if(dataRegis[0]['idPemohon']==3){//gov
                    dataRegis[0]['jenisIdentitas'] = "4";
                    dataRegis[0]['izinterkait'] = $("#terkaitGov").val();
                    typePemohon = "gov";
                }
                var npwpVal = $("."+typePemohon+"Npwp").val();

                dataRegis[0]['namaPemohon'] = $("."+typePemohon+"Form").val();
                dataRegis[0]['no_identitas'] = npwpVal;
                if(dataRegis[0]['idPemohon']==1){//cek buat jenis identitas perorangan NPWP / NIK
                    if(npwpVal.length==15){
                        dataRegis[0]['jenisIdentitas'] = "2";
                    }else{
                        dataRegis[0]['jenisIdentitas'] = "1";
                    }
                }
                dataRegis[0]['phone'] = $("#phoneF").val();
                dataRegis[0]['email'] = $("#emailF").val();
                var btnMengurus = $(".btn-pelayanan"),
                    mengurusSelect;
                btnMengurus.each(function(el,i){
                    if(i.classList[2]=='active'){
                        if(i.dataset.title=="Y"){
                            mengurusSelect = "1";
                        }else{
                            mengurusSelect = "2";
                        }
                    }
                })
                dataRegis[0]['mengurus'] = mengurusSelect;

                localStorage.setItem("dataReqPermission", JSON.stringify(dataRegis));


                // if (localStorage.getItem('typeStart') == 'simulasi'){
                //     $.ajax({
                //         url: api_url + 'matriksCtrl/savePermohonan',
                //         type: 'POST',
                //         dataType: 'json',
                //         data: {dataRegist: localStorage.getItem('dataReqPermission'),
                //             klb:localStorage.getItem('klb'),
                //             kdb:localStorage.getItem('kdb'),
                //             kdh:localStorage.getItem('kdh'),
                //             idArahan:localStorage.getItem('idArahan')},
                //         beforeSend:function() {
                //             $('#fourthStep').html(loader);
                //         },
                //         success:function(data) {
                //             $('#backPemohon').show();
                //             sendEmail(data.token, data.npwp);
                //         }
                //     });
                // }else{
                    localStorage.setItem("step", "thirdStep");
                    localStorage.setItem("prevStep", "secondStep");
                    customNextStep($('#secondStep'), $('#thirdStep'));
                // }
            }
        })
    });

    //STEP EMPAT
    $('#btnMapData').on('click', function () {
        var summData = JSON.parse(localStorage.getItem('dataReqPermission')),
            mapData1 = localStorage.getItem('b_bersyarat'),
            bsyarat = "",

            mapData2 = localStorage.getItem('b_izinkan'),
            bizin = "",

            mapData3 = localStorage.getItem('b_terbatas'),
            bterbatas = "",

            mapData4 = localStorage.getItem('b_terbatasbersyarat'),
            btasrat = "",

            mapData5 = localStorage.getItem('alamat'),
            alamat = "",

            mapData6 = localStorage.getItem('zona'),
            zona = "",

            mapData7 = localStorage.getItem('subzona'),
            subzona = "",

            mapData8 = localStorage.getItem('idsubblok'),
            idsubblok = "",

            mapData9 = localStorage.getItem('kordinat'),
            kordinat = "",

            mapData10 = localStorage.getItem('klb'),
            klb = "",

            mapData11 = localStorage.getItem('kdb'),
            kdb = "",

            mapData12 = localStorage.getItem('kdh'),
            kdh = "",


            fungsi;
        if(summData[0].izinProfesi!=undefined){
            $('.lahan').hide();
        }else{
            $('#reqLands').html(summData[0].luas_tanah + " m<sup>2</sup>");
            $('#reqConditions').html(summData[0].kondisi_lahan);
            $('#reqPersils').html(summData[0].jumlah_bidang_tanah_ser);
            $('#reqLandLenght').html(summData[0].jumlah_luas_tanah_ser + " m<sup>2</sup>");
            $('#reqObjPBB').html(summData[0].jumlah_obj_tanah_pbb);
            $('#reqLenghtPBB').html(summData[0].jumlah_luas_tanah_pbb + " m<sup>2</sup>");

            if (summData[0].kondisi_lahan == "Ada Bangunan"){
                if (summData[0].fungsi == 1){ fungsi = "Rumah Tinggal"; }else{ fungsi = "Non-Rumah Tinggal"; }

                $('#reqFunction').html(fungsi);
                $('#reqFloortCount').html(summData[0].jml_lantai);
                $('#reqSince').html(summData[0].digunakan_sejak);
                $('#reqBasementCount').html(summData[0].jml_base);
                $('#reqBaseLenght').html(summData[0].luas_base);
                $('#reqAllFLoor').html(summData[0].luas_lantai);
            }else{
                $('#reqFunction').html(summData[0].renc_fungsi);
                $('#reqFloortCount').html(summData[0].renc_jumlah_lantai);
                $('#reqSince').html(summData[0].renc_pada_tahun);
                $('#reqBasementCount').html(summData[0].renc_jml_base);
                $('#reqBaseLenght').html(summData[0].renc_luas_base);
                $('#reqAllFLoor').html(summData[0].renc_sel_lantai);
            }
        }

        bizin += "<ul>";
        mapData2.split(",").forEach(function(n, i) { bizin += "<li>"+ n +"</li>"; });
        bizin += "</ul>";
        $('#accBuilding').html(bizin);

        bterbatas += "<ul>";
        mapData3.split(",").forEach(function(n, i) { bterbatas += "<li>"+ n +"</li>"; });
        bterbatas += "</ul>";
        $('#limitedBuilding').html(bterbatas);

        bsyarat += "<ul>";
        mapData1.split(",").forEach(function(n, i) { bsyarat += "<li>"+ n +"</li>"; });
        bsyarat += "</ul>";
        $('#CondBuilding').html(bsyarat);

        btasrat += "<ul>";
        mapData4.split(",").forEach(function(n, i) { btasrat += "<li>"+ n +"</li>"; });
        btasrat += "</ul>";
        $('#limitcondBuilding').html(btasrat);


        $('#addressUser').html(mapData5);
        $('#ZonaUser').html(mapData6);
        $('#subzonaUser').html(mapData7);
        $('#idsubblok').html(mapData8);
        $('#kordinat').html(mapData9);
        $('#klb').html(mapData10);
        $('#kdb').html(mapData11);
        $('#kdh').html(mapData12);


        if (localStorage.getItem('b_izinkan') === null){
            Swal.fire({
                type: 'error',
                title: 'Terjadi Kesalahan',
                text: 'Harap pilih lokasi bangunan pada peta.'
            })
        }else{
            localStorage.setItem("step", "fifthStep");
            localStorage.setItem("prevStep", 'fourthStep');
            customNextStep($('#fourthStep'), $('#fifthStep'));
        }
    });

    //STEP LIMA
    $('#btnConfSumm').on('click', function () {
        var type = localStorage.getItem('typeStart'),
            dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
            dataArahan = JSON.parse(localStorage.getItem('dataArahan')),
            idarahanSpasial =[],
            datas = dataArahan,
            npwp = dataUser[0].no_identitas,
            token = localStorage.getItem('newToken');
        contents = '',
            recList = '',
            idPemohon = dataUser[0].idPemohon,
            arahancard = $('#arahanRumus'),
            columncard = $('#cardRec'),
            columnlist = $('#recList'),
            luasTanah = parseFloat(dataUser[0].luas_tanah),
            klbPeta = parseFloat(localStorage.getItem('klb')),
            kdbPeta = parseFloat(localStorage.getItem('kdb')),
            finalContent = "";
        localStorage.setItem("prevStep", "ninthStep");
        if(dataUser[0].izinProfesi!=undefined){
            izinProfesiCekSpasial();
            return;
        }
        dataArahan.map(function(el,i){
            idarahanSpasial.push(el.arahan);
            idarahanSpasial.join();
        })

        //Hitung Rumus
        // KDB = Luas Tanah x KDB yang ditentukan (%)
        var kdb = luasTanah * (kdbPeta / 100);

        // KLB = Luas Tanah x KLB yang ditentukan
        var klb = luasTanah * klbPeta;

        // Jml Lantai
        // Di Ambil Angka depannya saja, walapun 4,9 tetap di ambil 4
        var jmlLantai = klb / kdb;
        jmlLantai = jmlLantai.toString();
        splitLantai = jmlLantai.split(".");

        if(splitLantai.length>0){
            jmlLantai = splitLantai[0];
        }
        var ctArahan = "Dengan Luas Tanah <b>" + luasTanah + "m</b> pada lokasi yang dipilih hanya bisa maksimal melakukan pembangunan gedung <b>" +  jmlLantai + "</b> lantai.";

        if(isNaN(jmlLantai)){
            ctArahan = "Tidak dapat membangun bangunan pada lokasi yang dipilih";
        }


        arahancard.html(ctArahan);
        // if (type == 'buat_izin') {
        // finalContent += "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email "+ dataUser[0].email +"</div>" +
        //     "<button type='button' class='btn rounded-0 btn--main' onclick='directLogin();'>Selesai</button>";
        // $.ajax({
        //     url: api_url + 'matriksCtrl/savePermohonan',
        //     type: 'POST',
        //     dataType: 'json',
        //     data: {dataRegist: localStorage.getItem('dataReqPermission'),idArahan:localStorage.getItem('idArahan')},
        //     beforeSend:function() {
        //         Swal.showLoading();
        //     },
        //     success:function(data) {
        //         $('#backPemohon').show();
        //         finalContent += "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email "+ dataUser[0].email +"</div>" +
        //             "<input type='button' class='previous action-button-previous' value='Batalkan'/>" +
        //             "<button type='button' class='btn rounded-0 btn--main' onclick='directLogin();'>Selesai</button>";
        //         sendEmail(data.token, data.npwp);
        //     }
        // });
        //     localStorage.setItem("step", "twelfthStep");
        //     customNextStep($('#ninthStep'), $('#twelfthStep'));
        // }else{
        $('#backPemohon').hide();
        var dataArahanSpasial = [];
        $.ajax({
            url: api_url + 'MatriksCtrl/spasialCek',
            method: 'GET',
            dataType: 'json',
            data: {idarahan: idarahanSpasial,subzona:localStorage.getItem('subzona')},
            success:function(data){
                dataArahanSpasial.push(data);
                var labelITBX = "Data Spasial Tidak Ada", classITBX;
                if (datas.length == '1'){

                    localStorage.setItem("arahan", datas[0].arahan);
                    localStorage.setItem("idArahan", datas[0].id);
                    if(dataArahanSpasial[0][0].nama_lain_perizinan==datas[0].arahan){
                        if(dataArahanSpasial[0][0].status_itbx=="X"){
                            classITBX = "text-decoration:line-through";
                        }

                        labelITBX = "<label class='label label-default'>"+dataArahanSpasial[0][0].status_itbx+"</label>\n"

                    }
                    recList += "<div class='col-md-12'>\n" +labelITBX +
                        "                            <div data-arahan='"+datas[0].arahan+"' style='cursor: pointer;'>\n" +
                        "                                <span style='"+classITBX+"' class='d-block text-dark font-weight-bold h2 mb-0' id='imbTitle'>"+ datas[0].arahan +"</span>\n" +
                        "                            </div>\n" +
                        "                        </div>";

                    var ar = datas[0].arahan.replace(/\s/g, '');
                    contents += "<div class=\"card\">" +
                        "<div class=\"card-header\" id=\"heading"+ar+"\">\n" +
                        "<h2 class=\"mb-0\">\n" +
                        "<button class=\"btn permissionCheck\" data-arahan='"+datas[0].arahan+"' type=\"button\" data-toggle=\"collapse\" data-target='#"+ar+"' aria-expanded=\"true\" aria-controls=\"collapseOne\">Daftar Persyaratan Untuk<span class='d-block font-weight-bold font-size-lg'>"+datas[0].arahan+"</span></button>\n" +
                        "</h2>\n" +
                        "</div>\n" +
                        "<div id='"+ar+"' class=\"collapse\" aria-labelledby=\"headingOne"+ar+"\" data-parent=\"#cardRec\">\n" +
                        "<div class=\"card-body contentPersyaratan\">\n" +
                        "</div>\n" +
                        "</div>\n" +
                        "</div>";
                    $.ajax({
                        url: api_url + 'users/attachPdf',
                        type: 'GET',
                        dataType: 'json',
                        data:{idarahan:datas[0].id},
                        beforeSend:function() {
                            // Swal.showLoading();
                        },
                        success:function(data) {
                            // console.log(data);
                        }
                    });

                }else{
                    var idarahanArray = [];
                    $.each(datas, function (i, item) {
                        idarahanArray.push(item.id);
                        if(dataArahanSpasial[0][i]!=null){

                            if(dataArahanSpasial[0][i].nama_lain_perizinan==datas[i].arahan){
                                if(dataArahanSpasial[0][i].status_itbx=="X"){
                                    classITBX = "text-decoration:line-through";
                                }

                                labelITBX = "<label class='label label-default'>"+dataArahanSpasial[0][i].status_itbx+"</label>\n"

                            }

                        }
                        recList += "\n" +
                            "                        <div class='col-md-6'>\n"  +labelITBX +
                            "                            <div data-arahan='"+item.arahan+"' style='cursor: pointer;'>\n" +
                            "                                <span style='"+classITBX+"' class='d-block text-dark font-weight-bold h2 mb-0' id='imbTitle'>"+ item.arahan +"</span>\n" +
                            "                            </div>" +
                            "                        </div>";

                        var ar = item.arahan.replace(/\s/g, '');
                        contents += "<div class=\"card\">" +
                            "<div class=\"card-header\" id=\"heading"+ar+"\">\n" +
                            "<h2 class=\"mb-0\">\n" +
                            "<button class=\"btn permissionCheck\" data-arahan='"+item.arahan+"' type=\"button\" data-toggle=\"collapse\" data-target='#"+ar+"' aria-expanded=\"true\" aria-controls=\"collapseOne\">Daftar Persyaratan Untuk<span class='d-block font-weight-bold font-size-lg'>"+item.arahan+"</span></button>\n" +
                            "</h2>\n" +
                            "</div>\n" +
                            "<div id='"+ar+"' class=\"collapse\" aria-labelledby=\"headingOne"+ar+"\" data-parent=\"#cardRec\">\n" +
                            "<div class=\"card-body contentPersyaratan\">\n" +
                            "</div>\n" +
                            "</div>\n" +
                            "</div>";
                    });
                    var idarahanJoin = idarahanArray.join();
                    localStorage.setItem("idArahan", idarahanJoin);

                    var splitArahan = idarahanJoin.split(',');

                    for(var i = 0; i < splitArahan.length; i++)
                    {
                        $.ajax({
                            url: api_url + 'users/attachPdf',
                            type: 'GET',
                            dataType: 'json',
                            data:{idarahan:splitArahan[i]},
                            beforeSend:function() {
                                // Swal.showLoading();
                            },
                            success:function(data) {
                                // console.log(data);
                            }
                        });
                    }
                }

                columncard.html(contents);
                columnlist.html(recList);


                $(".permissionCheck").click(function(e){
                    e.preventDefault();
                    var arahan = $(this).data('arahan');
                    checkPersyaratan(arahan);
                });
            }
        });

        finalContent += "<div>Apakah anda ingin melanjutkan permohonan izin ini?</div>" +
            "<input type='button' class='previous action-button-previous resetDataLocal' value='Batalkan'/>" ;
        if(type=="buat_izin"){
            finalContent += "<button type='button' class='btn rounded-0 btn--main' onclick='savePermohonanVerifikasi();'>Lanjutkan</button>";
        }else{

            finalContent += "<button type='button' class='btn rounded-0 btn--main' onclick='directPemohon();'>Lanjutkan</button>";
        }

        localStorage.setItem("step", "sixthStep");
        customNextStep($('#fifthStep'), $('#sixthStep'));
        // }
        $('#finalContent').html(finalContent);
    });


    //Back
    $(".previous").on('click', function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    function customPrevStep(curr_step, prev_step) {
        if(animating) return false;
        animating = true;

        current_fs = curr_step;
        previous_fs = prev_step;

        //de-activate current step on progressbar
        // $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    }

    //Next
    $(".next").on('click', function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale('+scale+')',
                    // 'position': 'absolute',
                    'display' : 'none'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    function customNextStep(curr_step, next_step) {
        if(animating) return false;
        animating = true;

        current_fs = curr_step;
        next_fs = next_step;

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale('+scale+')',
                    // 'position': 'absolute',
                    'display' : 'none'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    }

    //Other Utillites

    //Load List Izin
    function loadIzin() {
        var idIzin = JSON.parse(localStorage.getItem('dataReqPermission')),
            listContent = '';
        $.ajax({
            url: api_url + 'Perizinan/list?idIzin='+idIzin[0].idIzin,
            type: 'GET',
            dataType: 'json',
            timeout: '30000',
            beforeSend:function() {
                // Swal.showLoading();
            },
            success: function (data) {
                for (var i in data.row) {
                    const n = data.row[i];
                    var btnBg = '';
                    if (n.status_izin_perizinan == 'Baru') {
                        btnBg = 'btnBg-appl-new';
                    }else if (n.status_izin_perizinan == 'Perubahan'){
                        btnBg = 'btnBg-appl-change';
                    }else if (n.status_izin_perizinan == 'Perpanjangan'){
                        btnBg = 'btnBg-appl-renew';
                    }
                    listContent += '<div class="col-md-3 mb-2 applsearchCon">\n' +
                        '<div class="button-labels applContainer btnBg-appl-new" onclick="clickKeprofesian('+n.id+')">\n' +
                        '<input type="checkbox" class="selIzinNew" value="'+n.id+'" id="selIzin'+n.id+'">\n' +
                        '<label for="selIzin'+n.id+'" class="applName">'+n.jenis_perizinan+'</label>\n' +
                        '</div>\n' +
                        '</div>';
                    // listContent += '<div class="row">\n' +
                    //     '        <div class="col-md-12">\n' +
                    //     '            <div class="mb-3 mt-1 border-bottom text-left">\n' +
                    //     '                Perizinan Perubahan\n' +
                    //     '            </div>\n' +
                    //     '            <div class="row">\n' +
                    //     '                <div class="col-md-3 mb-2 applsearchCon">\n' +
                    //     '                    <div class="button-labels applContainer btnBg-appl-new" onclick="clickKeprofesian(\'+n.id+\')">\n' +
                    //     '                        <input type="checkbox" class="selIzinNew" value="\'+n.id+\'" id="selIzin\'+n.id+\'">\n' +
                    //     '                        <label for="selIzin\'+n.id+\'" class="applName">\'+n.jenis_perizinan+\'</label>\n' +
                    //     '                    </div>\n' +
                    //     '                </div>\n' +
                    //     '            </div>\n' +
                    //     '        </div>\n' +
                    //     '    </div>';
                }
                $('.contentApplList').html(listContent);
            }
        });

        //    SEARCH ACTION
        $('.searchApplProf').on('keyup', function () {
            var input, filter, ul, li, a, i, txtValue;
            input = $('.searchApplProf');
            filter = input.val().toUpperCase();
            // ul = $('.applsearchCon');
            li = $('.applsearchCon');
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("label")[0];
                txtValue = a.textContent || a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        });

        //FINAL
        $('#btnToPlea').on('click', function () {
            savePermohonanVerifikasi();

            localStorage.setItem("step", "sixthStep");
            localStorage.setItem("prevStep", "fifthStep");
            customNextStep($('#sixthStep'), $('#seventhStep'));
        });

        //NEXT ACTION
        $('.doneIzinProf').on('click', function () {
            localStorage.setItem("step", "fourthStep");
            localStorage.setItem("prevStep", "thirdStep");
            customNextStep($('#thirdStep'), $('#fourthStep'));
        });

        //PRINT FUNCTION
        $('#printOutSumm').on('click', function () {

            var divToPrint = document.getElementById('dataTabelPrint');
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table {\n' +
                '    width: 100%;\n' +
                '    margin-bottom: 1rem;\n' +
                '    color: #212529;\n' +
                '}' +
                'table th, table td {' +
                '    padding: .75rem;\n' +
                '    vertical-align: top;\n' +
                '    border-top: 1px solid #dee2e6;' +
                '}' +
                'table thead th {\n' +
                '    vertical-align: bottom;\n' +
                '    border-bottom: 2px solid #dee2e6;\n' +
                '}' +
                'table tbody tr:nth-of-type(odd) {\n' +
                '    background-color: rgba(0,0,0,.05);\n' +
                '}' +
                'th {\n' +
                '    text-align: inherit;\n' +
                '}' +
                '</style>' +
                '<div style="text-align: center;color:#333;">' +
                '<h2>Ringkasan Permohonan Perizinan</h2></div>';
            htmlToPrint += divToPrint.outerHTML;
            newWin = window.open("");
            newWin.document.write(htmlToPrint);
            newWin.print();
            newWin.close();
        });
    }
})


function izinProfesiCekSpasial(){
    var dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        subzona = localStorage.getItem('subzona'),
        dataHTML = '';
    $.ajax({
        url: api_url + "/MatriksCtrl/matriksSpasial",
        method: "get",
        data: {subzona: subzona, dataReqPermission: localStorage.getItem('dataReqPermission')},
        dataType: 'json',
        success:function(data){
            if (data.success) {
                var classCol;
                if(data.data.length==1){
                    classCol = "col-md-12";
                }else if(data.data.length=="2"){
                    classCol = "col-md-6";
                }else if(data.data.length=="3"){
                    classCol = "col-md-4";
                }else if(data.data.length=="4"){
                    classCol = "col-md-3";
                }
                for(var i = 0; i < data.data.length; i++) {
                    var a = data.data[i];
                    var abc = a.itbx_detail;
                    if(abc!=null){

                        var cde = abc.split(":");
                    }
                        dataITBX = '',
                        itbxKet = '',
                        ITBX = '';

                    if (a.itbx == 'I'){ITBX='I : <span class="">di Izinkan</span>';}
                    else if(a.itbx == 'T'){ITBX='T : <span class="">Izin Terbatas</span>';}
                    else if(a.itbx == 'B'){ITBX='B : <span class="">Izin Bersyarat</span>';}
                    else if(a.itbx == 'X'){ITBX='X : <span class="">Izin Terbatas Bersyarat</span>';}
                    if(abc==null){
                        dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                    }else{

                        if (cde.length == 1) {
                            dataITBX += '<li class="licardITBX">'+ITBX+'</li>';
                        }
                        else if(cde.length==3){
                            dataITBX += "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>"+
                                        "<li class='licardITBX'>"+cde[1].substr(cde[1].length -2, 1) + " :" + cde[2]+"</li>";
                        }else if(cde.length==4){
                            dataITBX += "<li class='licardITBX'>"+cde[0].substr(0,1) + " :" + cde[1].substr(0,cde[1].length - 3)+"</li>"+
                                        "<li class='licardITBX'>"+cde[1].substr(cde[1].length - 2, 1) + " :" + cde[2]+"</li>"+
                                        "<li class='licardITBX'>"+cde[2].substr(cde[2].length - 2, 1) + " :" + cde[3]+"</li>";
                        }
                    }

                    dataHTML += '<div class="'+classCol+' mb-3">'+
                        '<div class="card text-center">\n' +
                        // '  <div class="card-header font-weight-bold">\n' + ITBX + '</div>\n' +
                        '  <div class="card-header font-weight-bold">Izin Prasarana / Infrastruktur</div>\n' +
                        '  <div class="card-body text-left">\n'+
                        '     <b>Jenis Izin</b> <br><p class="card-text text-center"><ul class="ulcardITBX"></b>'+a.jenis_perizinan+'</b></ul></p>\n'+
                        '     <b>Perizinan berdasarkan Zona '+localStorage.getItem('subzona')+'</b><br><p class="card-text text-center"><ul class="ulcardITBX">'+dataITBX+'</ul></p>\n';
                        

                        if(a.itbx=="X"){
                            dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Izin ini <b>tidak dapat</b> diajukan di zona yang dipilih</b></li></ul></p>\n';
                        }else{
                            dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Izin ini <b>dapat</b> diajukan di zona yang dipilih</li></ul></p>\n';
                        }
                        // '    <p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Perizinan : '+a.jenis_perizinan+'</li></ul></p>\n' +
                        
                        dataHTML += '  </div>\n' +
                        '</div>' +
                    '</div>';
                }

                $('#recList').html(dataHTML);

                localStorage.setItem('step', 'sixthStep');
                customNextStep($('#fifthStep'), $('#sixthStep'));
            }else{
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!'
                })
            }
        },
        error:function(xhr){
            Swal.fire(
                'Oops',
                xhr.status + " " + xhr.statusText
            )
        }
    })
}

function savePermohonanVerifikasi(){
    var type = localStorage.getItem('typeStart'),
        dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        dataArahan = JSON.parse(localStorage.getItem('dataArahan'));

    $.ajax({
        url: api_url + 'matriksCtrl/savePermohonan',
        type: 'POST',
        dataType: 'json',
        data: {
            dataRegist: localStorage.getItem('dataReqPermission'),
            klb:localStorage.getItem('klb'),
            kdb:localStorage.getItem('kdb'),
            kdh:localStorage.getItem('kdh'),
            idArahan:localStorage.getItem('idArahan')},
        beforeSend:function() {
            // Swal.showLoading();
        },
        success:function(data) {
            $('#backPemohon').show();
            finalContent = "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email "+ dataUser[0].email +"</div>" +
                "<button type='button' class='btn rounded-0 btn--main' onclick='directLogin();'>Selesai</button>";
            sendEmail(data.token, data.npwp);
        }
    });

    $('#finalContent').html(finalContent);
}

function checkHistoryStep() {
    var stepNum = localStorage.getItem("step");

    if (stepNum==null) {
        $("#firstStep").attr('style', 'display: block; left: 0%; opacity: 1;');
        return true;
    }else {
        Swal.fire({
            title: 'Pemberitahuan',
            text: "Apakah anda ingin melanjutkan proses sebelumya?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#ff6921',
            cancelButtonColor: '#575757',
            confirmButtonText: 'Ya, Lanjutkan',
            cancelButtonText: 'Ulangi',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                // var array = ["first","second","third","fourth","sixth","seventh","last"];
                $(".stepContainer .stepWizard:not(:first-of-type)").map(function(el,i){

                    var stepNum = localStorage.getItem("step");
                    if(i.getAttribute("id")==stepNum){
                        $(".stepContainer #firstStep")[0].setAttribute("style","transition:ease 0.5s all;webkit-transition:ease 0.5s all;display: none; left: 0%; opacity: 0;");
                        i.setAttribute("style","display: block; left: 0%; opacity: 1;transition:ease 0.5s all;webkit-transition:ease 0.5s all;");
                    }else{
                        i.setAttribute("style","display: none; left: 0%; opacity: 0;");
                    }
                    // debugger;

                });

                // buat hide step tertentu
                var btnType = localStorage.getItem("typeIzin"),nonForm;
                if(btnType!==null || btnType!=="null"){
                    if(btnType==="perorangan"){
                        nonForm = $(".compForm, .govForm, .npwpI, .nipGov, .terkaitGov, .dirutComp");
                    }else if(btnType==="perusahaan"){
                        nonForm = $(".selfForm, .govForm, .npwpNik, .nipGov, .terkaitGov");
                    }else if(btnType==="pemerintahan" || btnType==="yayasan"){
                        nonForm = $(".selfForm, .compForm, .npwpNik, .npwpI, .dirutComp, .selectSelfComp");
                    }
                    nonForm.hide();
                }

                // buat set form field-field nya
                var datanya = JSON.parse(localStorage.getItem('dataReqPermission')),
                    typenya;
                if(btnType=="perorangan"){
                    typenya = "self";
                }else if(btnType=="perusahaan"){
                    typenya = "comp";
                    $("#dirutComp").val(datanya[0].dirut);
                    $("#npwpdirutComp").val(datanya[0].npwpdirut);
                }else{
                    $("#terkaitGov").val(datanya[0].izinterkait);
                    typenya = "gov";
                }
                $("."+typenya+"Form").val(datanya[0].namaPemohon);
                $("."+typenya+"Npwp").val(datanya[0].no_identitas);
                $("#phoneF").val(datanya[0].phone);
                $("#emailF").val(datanya[0].email);
                var mengurusSendiri = datanya[0].mengurus;
                $(".btn-pelayanan").each(function(el,i){
                    if(mengurusSendiri=="2"){
                        if(i.dataset.title=="N"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }else{
                        if(i.dataset.title=="Y"){
                            i.classList.value = "btn btn-pelayanan active";
                        }else{
                            i.classList.value = "btn btn-pelayanan notActive";
                        }
                    }
                })


            }else{
                localStorage.clear();

                location.href='../';
            }
        })
    }
}