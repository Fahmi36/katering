// var api_url = 'http://rest-api.pkkmart.com/';
// var api_url = 'http://192.168.100.16/rest-api/';
var api_url = 'https://rest.perizinan-dev.com/';

var tahapIzinStat = [];
$(document).ready(function () {
    $('.btnRefresh').on('click', function () {
        location.reload();
    });

    // PRINT
    $('#printOutDetail').on('click', function () {
        $('.printAct').addClass('show');
        print();
    });

    $("#btnLandData").on('click', function () {
        var dataRegis = JSON.parse(localStorage.getItem("dataReqPermission")),
            setRegis;
        $('#inpLandLenghtR').val($('#inpLandLenght').val());
        $('#frmizinNoR').val($('#frmizinNo').val());
        $('#frmizinSIPPTR').val($('#frmizinSIPPT').val());
        $('#frmizinIMBR').val($('#frmizinIMB').val());
        $('#frmizinSLFR').val($('#frmizinSLF').val());
        $('#inpBuildHeightR').val($('#inpBuildHeight').val());
        $('#inpBuktiTanahR').val($('#inpBuktiTanah').val());
        $('#inpLandLenghtRencR').val($('#inpLandLenghtRenc').val());
        $('#inpBuktiTanahRencR').val($('#inpBuktiTanahRenc').val());
        $('#inpBuildHeightRencR').val($('#inpBuildHeightRenc').val());
        $('#inpFungsiBangunanR').val($('#inpFungsiBangunan').val());
        $('#inpFungsiBangunanRencR').val($('#inpFungsiBangunanRenc').val());


        var izinDimiliki = [];
        $.each($("input[name='checkFrmIzin']:checked"), function () {
            izinDimiliki.push({
                izin: $(this).attr('id'),
                val: $(this).val()
            });
        });

        var dataInput = {
            luas_tanah: $("#inpLandLenght").val(),
            jml_lantai: $("#inpBuildHeight").val(),
            hak_guna: $("#inpBuktiTanah").val(),
            izin_dimiliki: izinDimiliki,
            fungsi_bangunan: $("#inpFungsiBangunan").val(),
            luas_tanah_renc: $("#inpLandLenghtRenc").val(),
            hak_guna_renc: $("#inpBuktiTanahRenc").val(),
            jml_lantai_renc: $("#inpBuildHeightRenc").val(),
            fungsi_bangunan_renc: $("#inpFungsiBangunanRenc").val(),
        };

        dataRegis[0].luas_tanah = dataInput.luas_tanah;
        dataRegis[0].luas_tanah_renc = dataInput.luas_tanah_renc;
        dataRegis[0].hak_guna = dataInput.hak_guna;
        dataRegis[0].izin_dimiliki = dataInput.izin_dimiliki;
        dataRegis[0].fungsi_bangunan = dataInput.fungsi_bangunan;
        dataRegis[0].jml_lantai = dataInput.jml_lantai;
        dataRegis[0].hak_guna_renc = dataInput.hak_guna_renc;
        dataRegis[0].jml_lantai_renc = dataInput.jml_lantai_renc;
        dataRegis[0].fungsi_bangunan_renc = dataInput.fungsi_bangunan_renc;

        localStorage.setItem("dataReqPermission", JSON.stringify(dataRegis));

        localStorage.setItem("step", "secondStep");
        localStorage.setItem("prevStep", "firstStep");
        customNextStep($('#firstStep'), $('#secondStep'));
        // }
    });

    $('#btnConfSumm').on('click', function () {
        var type = localStorage.getItem('typeStart'),
            dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
            dataRecList = '',
            dataHTML = ''
        contents = '',
            recList = '',
            idPemohon = dataUser[0].idPemohon,
            arahancard = $('#arahanRumus'),
            columncard = $('#cardRec'),
            columnlist = $('#recList'),
            arahankdh = $('#arahanKDH'),
            arahanitbx = $('#arahanTableITBX'),
            luasTanah = parseFloat(dataUser[0].luas_tanah),
            luasTanahRenc = parseFloat(dataUser[0].luas_tanah_renc),
            klbPeta = parseFloat(localStorage.getItem('klb')),
            kdbPeta = parseFloat(localStorage.getItem('kdb')),
            finalContent = "";

        //Hitung Rumus
        // KDB = Luas Tanah x KDB yang ditentukan (%)
        var kdb = luasTanah * (kdbPeta / 100);

        // KLB = Luas Tanah x KLB yang ditentukan
        var klb = luasTanah * klbPeta;

        // Jml Lantai
        // Di Ambil Angka depannya saja, walapun 4,9 tetap di ambil 4
        var jmlLantai = klb / kdb;
        jmlLantai = jmlLantai.toString();
        splitLantai = jmlLantai.split(".");

        if (splitLantai.length > 0) {
            jmlLantai = splitLantai[0];
        }

        var ctArahan = "<span>Dengan ketentuan KLB yaitu 'Luas Tanah x KLB Peta', Luas Tanah <b>" + luasTanah + "m<sup>2</sup></b>, dapat membangun bangunan dengan luas lantai maksimal hingga <b>" + klb + "m<sup>2</sup></span></p>";

        // if(isNaN(jmlLantai)){
        //     ctArahan = "Tidak dapat membangun bangunan pada lokasi yang dipilih";
        //     $('#recList').html('<div class="text-center font-weight-bold text-danger">Data yang Anda masukkan salah pada bagian (Subzona '+ localStorage.getItem('subzona') +')</div>');
        // }
        arahancard.html(ctArahan);

        // KDH
        var htmlKDH = '';
        htmlKDH += '<div>' +
            '<p>Dengan ketentuan KDH yaitu "(KDH * Luas Tanah)/100", Daerah Hijau yang harus dipenuhi minimal sebesar' +
            '<span class="font-weight-bold d-block h5">' + (localStorage.getItem('kdh') * luasTanah) / 100 + 'm&sup2;</span>' +
            '</p>' +
            '</div>';
        arahankdh.html(htmlKDH);

        columncard.html(contents);



        // TABEL ITBX
        var getBersyarat = localStorage.getItem('b_bersyarat'),
            getIzinkan = localStorage.getItem('b_izinkan'),
            getTerbatas = localStorage.getItem('b_terbatas'),
            getTerbatasB = localStorage.getItem('b_terbatasbersyarat'),
            tabelItbx = '';

        var gSyarat = getBersyarat.split(','),
            gIzinkan = getIzinkan.split(','),
            gTerbatas = getTerbatas.split(','),
            gTerbatasB = getTerbatasB.split(',');

        var listSyarat = "<ol>";
        for (var ia in gSyarat) {
            listSyarat += "<li>" + gSyarat[ia] + "</li>";
        }
        listSyarat += "</ol>";

        var listIzinkan = "<ol>";
        for (var ib in gIzinkan) {
            listIzinkan += "<li>" + gIzinkan[ib] + "</li>";
        }
        listIzinkan += "</ol>";

        var listTerbatas = "<ol>";
        for (var ic in gTerbatas) {
            listTerbatas += "<li>" + gTerbatas[ic] + "</li>";
        }
        listTerbatas += "</ol>";

        var listTerbatasB = "<ol>";
        for (var id in gTerbatasB) {
            listTerbatasB += "<li>" + gTerbatasB[id] + "</li>";
        }
        listTerbatasB += "</ol>";

        tabelItbx += '<div class="table-responsive">\n' +
            '<table class="table table-striped table-bordered text-left" id="dataTabelPrint" border="1" cellpadding="3">\n' +
            '            <thead>\n' +
            '            <tr>\n' +
            '                <th scope="col">#</th>\n' +
            '                <th scope="col">Kriteria</th>\n' +
            '                <th scope="col">Data Anda</th>\n' +
            '            </tr>\n' +
            '            </thead>' +
            '            <tbody>' +
            '<tr>' +
            '<th scope="row">1</th>\n' +
            '<td>Bangunan Diizinkan</td>\n' +
            '<td><div id="accBuilding" class="ml-3">' + listSyarat + '</div></td>\n' +
            '</tr>\n' +
            '<tr>\n' +
            '<th scope="row">2</th>\n' +
            '<td>Bangunan Bersyarat</td>\n' +
            '<td><div id="CondBuilding" class="ml-3">' + listIzinkan + '</div></td>\n' +
            '</tr>\n' +
            '<tr>\n' +
            '<th scope="row">3</th>\n' +
            '<td>Bangunan Terbatas</td>\n' +
            '<td><div id="limitedBuilding" class="ml-3">' + listTerbatas + '</div></td>\n' +
            '</tr>\n' +
            '<tr>\n' +
            '<th scope="row">4</th>\n' +
            '<td>Bangunan Terbatas Bersyarat</td>\n' +
            '<td><div id="limitcondBuilding" class="ml-3">' + listTerbatasB + '</div></td>\n' +
            '</tr>' +
            '            </tbody>\n' +
            '        </table>' +
            '                            </div>\n';

        $('#itbxPeta').html(tabelItbx);

        getDataKondisi();

        Swal.fire({
            title: "KONFIRMASI",
            text: "Apakah data yang anda masukan sudah sesuai?",
            showCancelButton: true,
            confirmButtonColor: '#ff6704',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, sudah',
            confirmButtonClass: 'next',
            cancelButtonText: 'Belum'
        }).then((result) => {
            if (result.value) {
                localStorage.setItem('step', 'secondStep');
                localStorage.setItem('prevStep', 'secondStep');
                customNextStep($('#secondStep'), $('#thirdStep'));
            }
        });
    });

    $('#btnToPlea').on('click', function () {
        savePermohonanVerifikasi();

        localStorage.setItem("step", "sixthStep");
        localStorage.setItem("prevStep", "fifthStep");
        customNextStep($('#fifthStep'), $('#sixthStep'));
    });

    $(".previous").on('click', function () {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'left': left
                });
                previous_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'opacity': opacity
                });
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });
});

// Login
$('#loginAct').on('click', function () {
    var email_ = $('#email_login').val(),
        pass_ = $('#password_login').val(),
        token_ = $('#tokenReq').text(),
        data_ = localStorage.getItem('dataReqPermission'),
        urlApi = 'https://imbpaket.perizinan-dev.com/Permohonan/saveLogin/' + btoa(email_ + ',' + pass_, ',' + localStorage.getItem('kode_izin'), ',' + token_ + ',' + localStorage.getItem('namaizin_')) + '?uri=true';

    if (email_ != '' && pass_ != '') {
        window.location.href = urlApi;

        // $.ajax({
        //     url: 'https://imbpaket.perizinan-dev.com/Permohonan/saveLogin/'+btoa('email='+email_+'&password='+pass_),
        //     type: 'POST',
        //     dataType: 'json',
        //     data: {
        //         email: email_,
        //         password: pass_,
        //         token: token_,
        //         kode_izin: localStorage.getItem('kode_izin'),

        //     },
        //     beforeSend: function () {},
        //     success: function (data) {
        //         // debugger;

        //         if (data.success) {
        //             if (data.rowCount > 0) {
        //                 Swal.fire(
        //                     'Berhasil',
        //                     'Anda akan diarahkan ke dashboard',
        //                     'success'
        //                 )
        //             } else {
        //                 Swal.fire({
        //                     type: 'error',
        //                     title: 'Akun tidak ditemukan...',
        //                     showConfirmButton: false
        //                 })
        //             }
        //         } else {
        //             Swal.fire({
        //                 type: 'error',
        //                 title: 'Terjadi Kesalahan',
        //                 text: 'Data login anda tidak ditemukan'
        //             })
        //         }
        //     }
        // });
    } else {
        Swal.fire(
            'Gagal',
            'Harap isi semua formulir',
            'error'
        )
    }

});

// Regiter
$('#registAct').on('click', function () {
    var email_ = $('#email_reg').val(),
        pass_ = $('#password_reg').val(),
        pass2_ = $('#password2_reg').val(),
        token_ = $('#tokenReq').text(),
        nama_ = $('#nama_reg').val(),
        // urlApi = 'http://192.168.100.16/jakrevo/Permohonan/saveRegister/'+btoa(email_+','+pass_+','+nama_+','+localStorage.getItem('kode_izin')+','+token_+','+localStorage.getItem('namaizin_'));
        urlApi = 'https://imbpaket.perizinan-dev.com/Permohonan/saveRegister/' + btoa(email_ + ',' + pass_ + ',' + nama_ + ',' + localStorage.getItem('kode_izin') + ',' + token_ + ',' + localStorage.getItem('namaizin_')) + '?uri=true',

        urlnew = urlApi.replace(/=/g, "");

    if (email_ != '' && pass_ != '' && pass2_ != '' && nama_ != '') {
        window.location.href = urlnew;

        // $.ajax({
        //     url: api_url + 'register',
        //     type: 'POST',
        //     dataType: 'json',
        //     data: {
        //         email: email_,
        //         password: pass_,
        //         nama_lengkap: nama_
        //     },
        //     beforeSend: function () {},
        //     success: function (data) {
        //         // debugger;
        //         if (data.success) {
        //             if (data.rowCount > 0) {
        //                 Swal.fire(
        //                     'Berhasil',
        //                     'Anda akan diarahkan ke dashboard',
        //                     'success'
        //                 )
        //             } else {
        //                 Swal.fire({
        //                     type: 'error',
        //                     title: 'Pendaftaran gagal...',
        //                     showConfirmButton: false
        //                 })
        //             }
        //         } else {
        //             Swal.fire({
        //                 type: 'error',
        //                 title: 'Terjadi Kesalahan',
        //                 text: 'Data pendaftaran anda tidak valid'
        //             })
        //         }
        //     }
        // });
    } else {
        Swal.fire(
            'Gagal',
            'Harap isi semua formulir',
            'error'
        )
    }
});

//PRINT FUNCTION
$('#printOutDetail').on('click', function () {
    var divToPrint = document.getElementById('izinContents');
    var htmlToPrint = '' +
        '<style type="text/css">' +
        'table {\n' +
        '    width: 100%;\n' +
        '    margin-bottom: 1rem;\n' +
        '    color: #212529;\n' +
        '}' +
        'table th, table td {' +
        '    padding: .75rem;\n' +
        '    vertical-align: top;\n' +
        '    border-top: 1px solid #dee2e6;' +
        '}' +
        'table thead th {\n' +
        '    vertical-align: bottom;\n' +
        '    border-bottom: 2px solid #dee2e6;\n' +
        '}' +
        'table tbody tr:nth-of-type(odd) {\n' +
        '    background-color: rgba(0,0,0,.05);\n' +
        '}' +
        'th {\n' +
        '    text-align: inherit;\n' +
        '}' +
        '</style>' +
        '<div style="text-align: center;color:#333;">' +
        '<h2 id="textTitlePrint">Detail Permohonan Izin</h2></div>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("", 'blank_');
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
});



function savePermohonanVerifikasi() {
    var type = localStorage.getItem('typeStart'),
        dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        dataArahan = JSON.parse(localStorage.getItem('dataArahan'));

    $.ajax({
        url: api_url + 'matriksCtrl/savePermohonanIzin',
        type: 'POST',
        dataType: 'json',
        data: {
            subzona: localStorage.getItem('subzona'),
            dataRegist: localStorage.getItem('dataReqPermission'),
            izinDataDetail: localStorage.getItem('izinDataDetail'),
            klb: localStorage.getItem('klb'),
            kdb: localStorage.getItem('kdb'),
            kdh: localStorage.getItem('kdh'),
            idArahan: localStorage.getItem('idArahan')
        },
        beforeSend: function () {
            // Swal.showLoading();
        },
        success: function (data) {
            $('#backPemohon').show();
            finalContent = "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email " + dataUser[0].email + "</div>" +
                "<button type='button' class='btn rounded-0 btn--main' onclick='directLoginNew();'>Selesai</button>";
            sendEmailNew(data.token, data.npwp);

            $('#finalContent').html(finalContent);
        }
    });
}

function getDataArahan() {
    $.ajax({
        url: api_url + 'matriksCtrl/getArahanRekomendasi',
        // url: api_url + 'cronjobs/cekKondisi',
        type: 'POST',
        dataType: 'json',
        data: {
            dataRegist: localStorage.getItem('dataReqPermission')
        },
        beforeSend: function () {
            // Swal.showLoading();
        },
        success: function (data) {
            // debugger;
            if (data.success) {
                // Swal.clear();
                if (data.rowCount > 0) {
                    var datas = data.row,
                        columncard = $('#cardRec'),
                        contents = "",
                        idarahanArray = [];

                    localStorage.setItem("newToken", data.token);
                    localStorage.setItem("dataArahan", JSON.stringify(datas));

                    if (datas.length == '1') {
                        localStorage.setItem("idArahan", datas[0].id);
                    } else {
                        $.each(datas, function (i, item) {
                            idarahanArray.push(item.id);
                        });

                        var idarahanJoin = idarahanArray.join();
                        localStorage.setItem("idArahan", idarahanJoin);
                    }

                    getAjaxCustom(data.token);

                    // $('#imbTitle').html(datas.arahan);
                } else {
                    Swal.fire({
                        type: 'error',
                        title: 'Data tidak ada...',
                        showConfirmButton: false
                    })
                }
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Terjadi Kesalahan',
                    text: 'Data arahan tidak ditemukan'
                })
            }
        }
    });
}

function getDataKondisi() {
    var dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
        luasTanah = parseFloat(dataUser[0].luas_tanah),
        klbPeta = parseFloat(localStorage.getItem('klb')),
        kdbPeta = parseFloat(localStorage.getItem('kdb'));
    //Hitung Rumus
    // KDB = Luas Tanah x KDB yang ditentukan (%)
    var kdb = luasTanah * (kdbPeta / 100);

    // KLB = Luas Tanah x KLB yang ditentukan
    var klb = luasTanah * klbPeta;

    // Jml Lantai
    // Di Ambil Angka depannya saja, walapun 4,9 tetap di ambil 4
    var jmlLantai = klb / kdb;
    jmlLantai = jmlLantai.toString();
    splitLantai = jmlLantai.split(".");

    if (splitLantai.length > 0) {
        jmlLantai = splitLantai[0];
    }
    var ctArahan = "Dengan Luas Tanah <b>" + luasTanah + "m</b> pada lokasi yang dipilih hanya bisa maksimal melakukan pembangunan gedung <b>" + jmlLantai + "</b> lantai.";

    if (isNaN(jmlLantai)) {
        ctArahan = "Tidak dapat membangun bangunan pada lokasi yang dipilih";
        $('#recList').html('<div class="text-center mx-auto font-weight-bold">Data yang Anda masukkan tidak cocok dengan kriteria Pemanfaatan Lahan dan Ketataruangan </div>');
        return
    }

    $.ajax({
        // url: api_url + 'CronJobs/cekKondisiKawasan',
        url: api_url + 'Perizinan/getNewMatriks',
        type: 'GET',
        dataType: 'json',
        data: {
            kondisi: localStorage.getItem('dataReqPermission'),
            subzona: localStorage.getItem('subzona'),
            idIzin: 1
        },
        beforeSend: function () {
            // Swal.showLoading();
        },
        success: function (data) {
            var dataHTML = '';
            var classCol;
            if (data.success) {
                var classCol;
                if (data.success == true) {
                    var classCol;
                    if (data.row.length == 1) {
                        classCol = "col-md-12";
                    } else if (data.row.length == "2") {
                        classCol = "col-md-6";
                    } else if (data.row.length == "3") {
                        classCol = "col-md-4";
                    } else if (data.row.length == "4" || data.row.length > "4") {
                        classCol = "col-md-4";
                    }
                    var izinDataDetail = [];
                    for (var i = 0; i < data.row.length; i++) {
                        var a = data.row[i];
                        tahapIzinStat = a.tahapanIzin;
                        var abc = a.itbx_detail;
                        if (abc != null) {

                            var cde = abc.split(":");
                        }
                        izinDataDetail.push({
                            idIzin: data.row.idIzin,
                            itbx: a.itbx
                        });
                        dataITBX = '',
                            itbxKet = '',
                            ITBX = '';

                        if (a.itbx == 'I') {
                            ITBX = 'I : Diizinkan';
                        } else if (a.itbx == 'T') {
                            ITBX = 'T : Diizinkan Terbatas';
                        } else if (a.itbx == 'B') {
                            ITBX = 'B : Diizinkan Bersyarat';
                        } else if (a.itbx == 'TB') {
                            ITBX = 'TB : Diizinkan Terbatas Bersyarat';
                        } else if (a.itbx == 'X') {
                            ITBX = 'X : Tidak Diizinkan';
                        }
                        if (abc == null) {
                            dataITBX += '<li class="licardITBX">' + ITBX + '</li>';
                        } else {

                            if (cde.length > 1) {

                                var itbxSingleSpasial = "<li class='licardITBX'>" + cde[0].substr(0, 1) + " :" + cde[1].substr(0, cde[1].length - 3) + "</li>";
                                if (cde.length == 2) {
                                    itbxSingleSpasial = "<li class='licardITBX'>" + cde[0].substr(0, 1) + " :" + cde[1].substr(0, cde[1].length) + "</li>";
                                }
                                var itbxDoubleSpasial = "<li class='licardITBX'>" + cde[1].substr(cde[1].length - 2, 1) + " :" + cde[2] + "</li>";
                            }

                            if (cde.length == 1) {
                                dataITBX += '<li class="licardITBX">' + ITBX + '</li>';
                            } else if (cde.length == 2) {
                                dataITBX += itbxSingleSpasial;
                            } else if (cde.length == 3) {
                                dataITBX += itbxSingleSpasial +
                                    itbxDoubleSpasial;
                            } else if (cde.length == 4) {
                                dataITBX += itbxSingleSpasial +
                                    itbxDoubleSpasial +
                                    "<li class='licardITBX'>" + cde[2].substr(cde[2].length - 2, 1) + " :" + cde[3] + "</li>";
                            }
                        }

                        dataHTML += '<div class="' + classCol + ' mb-3">' +
                            '<div class="card text-center" style="height:100%;">\n' +
                            // '  <div class="card-header font-weight-bold">\n' + ITBX + '</div>\n' +
                            // '  <div class="card-header font-weight-bold" onclick="clickKeprofesian('+a.idIzin+')">' +
                            // <input type="checkbox" class="selIzinNew" value="'+a.idIzin+'">
                            // 'Izin Keprofesian</div>\n' +
                            '  <div class="card-body text-left">\n' +
                            '     <b>Jenis Izin</b> <br><p class="card-text text-center"><ul class="ulcardITBX"></b>' + a.nama_izin_terbit + '</b></ul></p>\n';
                        // '     <b>Perizinan berdasarkan Zona '+localStorage.getItem('subzona')+'</b>' +
                        // '<br><p class="card-text text-center"><ul class="ulcardITBX">'+dataITBX+'</ul></p>\n';


                        // if(a.itbx=="X"){
                        //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Keterangan : Profesi ini tidak dapat diajukan di zona yang dipilih</b></li></ul></p>\n';
                        // }else{
                        //     dataHTML += '<p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Profesi ini dapat diajukan di zona yang dipilih</li></ul></p>\n';
                        // }
                        dataHTML += "</div><div class='card-footer'><div class='text-center'><button class='btn btn-info btnModalInfo' data-idIzin='" + a.idjenisizin + "' data-title='" + a.nama_izin_terbit + "' data-desc='" + a.deskripsi_izin + "' data-lokizin='" + a.lokasi_kewenangan + "' data-kesesuaian='" + a.kesesuaian_citata + "' data-jam='" + a.total_waktu + "' data-biaya='" + a.biaya + "' data-itbx='" + ITBX + "' data-idmatriks='" + a.idmatriks + "'>Lihat Detail</button> | <button class='btn btn-warning btnDraftInfo' data-idIzin='" + a.idjenisizin + "' data-title='" + a.nama_izin_terbit + "' data-desc='" + a.deskripsi_izin + "' data-lokizin='" + a.lokasi_kewenangan + "' data-kesesuaian='" + a.kesesuaian_citata + "' data-jam='" + a.total_waktu + "' data-biaya='" + a.biaya + "' data-itbx='" + ITBX + "' data-idmatriks='" + a.idmatriks + "' data-code='" + a.kode_izin + "'>Ajukan Izin</button></div>";
                        // '    <p class="card-text text-left"><ul class="ulcardITBX"><li class="licardITBX">Perizinan : '+a.jenis_perizinan+'</li></ul></p>\n' +

                        dataHTML += '  </div>\n' +
                            '</div>' +
                            '</div>';
                    }

                    localStorage.setItem('izinDataDetail', JSON.stringify(izinDataDetail));

                    $('#recList').html(dataHTML);

                    $(".btnModalInfo").click(function (e) {
                        e.preventDefault();
                        $('#textTitlePrint').html($(this).data('title'));

                        var bt = $(this),
                            idPerizinan = bt.data('idizin'),
                            title = bt.data('title'),
                            desc = bt.data('desc').replace("[|]", ","),
                            lok_kewenangan = bt.data('lokizin'),
                            kesesuaian_citata = bt.data('kesesuaian'),
                            jam = bt.data('jam'),
                            biaya = bt.data('biaya'),
                            itbx,
                            zona = localStorage.getItem('subzona'),
                            stat;
                        $.ajax({
                            url: api_url + 'MatriksCtrl/findById',
                            data: {
                                id: idPerizinan,
                                subzona: localStorage.getItem('subzona'),
                                jenis_izin: 1
                            },
                            method: 'GET',
                            dataType: 'json',
                            success: function (data) {
                                if (data.rowCount != 0) {
                                    var el = data.row[0];

                                    idmatriks = bt.data('idmatriks'),
                                        idPerizinan = el.idIzin;
                                    othername = el.nama_lain_perizinan;
                                    itbx = el.itbx;
                                    var ITBX;
                                    var bangunanizin = [];
                                    if (itbx == 'I') {
                                        ITBX = 'I : Diizinkan';
                                    } else if (itbx == 'T') {
                                        ITBX = 'T : Diizinkan Terbatas';
                                    } else if (itbx == 'B') {
                                        ITBX = 'B : Diizinkan Bersyarat';
                                    } else if (itbx == 'TB') {
                                        ITBX = 'TB : Diizinkan Terbatas Bersyarat';
                                    } else if (itbx == 'X') {
                                        ITBX = 'X : Tidak Diizinkan';
                                    }

                                    $.ajax({
                                        url: api_url + 'Perizinan/itbxSpasial',
                                        data: {
                                            idizin: idPerizinan,
                                            subzona: localStorage.getItem('subzona'),
                                            jenis_izin: 1
                                        },
                                        method: 'GET',
                                        dataType: 'json',
                                        success: function (data) {
                                            var dats = data.row;
                                            var it = [],
                                                t = [],
                                                b = [];
                                            for (var i = 0; i < dats.length; i++) {
                                                // debugger;
                                                // console.log(dats[i]);
                                                // bangunanizin = dats[i].itbx_detail;
                                                if (dats[i].itbx == 'B') {
                                                    b.push(dats[i].itbx_detail);
                                                }
                                                if (dats[i].itbx == 'I') {
                                                    it.push(dats[i].itbx_detail);
                                                }
                                                if (dats[i].itbx == 'T') {
                                                    t.push(dats[i].itbx_detail);
                                                }
                                            }
                                            // modalOpenDetail(idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya,'anjing',);
                                            getTahapan(idmatriks, idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya, b, it, t);
                                        }
                                    });
                                } else {
                                    swal.fire({
                                        type: 'error',
                                        title: "Oops",
                                        text: 'Cannot fetch data detail'
                                    });
                                }
                            }
                        })
                    });


                    $('.btnDraftInfo').on('click', function () {
                        if (dataUser[0].idIzin == 1) {
                            swal.fire({
                                type: 'info',
                                title: "Perhatian",
                                text: 'Perizinan ini akan segera hadir...'
                            });
                        } else {
                            Swal.fire({
                                text: "Pengajuan izin anda disimpan",
                                showCancelButton: false,
                                confirmButtonColor: '#ff6704',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Selanjutnya',
                                confirmButtonClass: 'next',
                            }).then((result) => {
                                if (result.value) {
                                    localStorage.setItem('namaizin_', $(this).data('title'));
                                    $('#namaIzinReq').html($(this).data('title'));

                                    $.ajax({
                                        url: 'https://imbpaket.perizinan-dev.com/Permohonan/getKodeIzin/' + $(this).data('code'),
                                        dataType: 'html',
                                        method: 'GET',
                                        data: localStorage.getItem('dataReqPermission'),
                                        success: function (data) {
                                            $('#tokenReq').html(data);

                                            localStorage.setItem('token_izin', data);
                                            localStorage.setItem('kode_izin', data.split('/')[0]);
                                            localStorage.setItem('step', 'fourthStep');
                                            localStorage.setItem('prevStep', 'thirdStep');
                                            customNextStep($('#thirdStep'), $('#fourthStep'));
                                        }
                                    })
                                }
                            })
                        }

                    });

                } else {
                    $("#recList").html('<div class="card text-center w-100">' +
                        '<div class="card-body text-center">' +
                        'Data yang Anda masukkan tidak cocok dengan kriteria Pemanfaatan Lahan dan Ketataruangan </div>');
                }
            }
        }
    });
}

function getTahapan(id, idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya, b, it, t) {
    var getLocalData = JSON.parse(localStorage.getItem('dataReqPermission'));
    $.ajax({
        url: api_url + 'Perizinan/getTahapanIzin?idmatriks=' + id,
        dataType: 'json',
        method: 'GET',
        data: {
            idmatriks: id,
            luas_tanah: getLocalData[0].luas_tanah,
            luas_tanah_renc: getLocalData[0].luas_tanah_renc,
            idIzin: getLocalData[0].idIzin
        },
        success: function (data) {
            modalOpenDetail(idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya, data.data, b, it, t);
        }
    })
}

function detailTahapIzin(kode, title, desc, jam, lok_kewenangan, kesesuaian_citata, biaya, idmatriks, jenisnya) {
    $.ajax({
        url: api_url + 'MatriksCtrl/findById',
        data: {
            code: kode,
            subzona: localStorage.getItem('subzona'),
            jenis_izin: jenisnya
        },
        method: 'GET',
        async: false,
        dataType: 'json',
        success: function (data) {
            if (data.rowCount != 0) {
                var el = data.row[0];
                idPerizinan = el.idIzin;
                othername = el.nama_lain_perizinan;
                zona = localStorage.getItem('subzona');
                itbx = el.itbx;
                othername = '';
                var ITBX;
                if (itbx == 'I') {
                    ITBX = 'I : Diizinkan';
                } else if (itbx == 'T') {
                    ITBX = 'T : Diizinkan Terbatas';
                } else if (itbx == 'B') {
                    ITBX = 'B : Diizinkan Bersyarat';
                } else if (itbx == 'TB') {
                    ITBX = 'TB : Diizinkan Terbatas Bersyarat';
                } else if (itbx == 'X') {
                    ITBX = 'X : Tidak Diizinkan';
                }

                $.ajax({
                    url: api_url + 'Perizinan/itbxSpasial',
                    data: {
                        idizin: idPerizinan,
                        subzona: localStorage.getItem('subzona'),
                        jenis_izin: 1
                    },
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        var dats = data.row;
                        var it = [],
                            t = [],
                            b = [];
                        for (var i = 0; i < dats.length; i++) {
                            // debugger;
                            // console.log(dats[i]);
                            // bangunanizin = dats[i].itbx_detail;
                            if (dats[i].itbx == 'B') {
                                b.push(dats[i].itbx_detail);
                            }
                            if (dats[i].itbx == 'I') {
                                it.push(dats[i].itbx_detail);
                            }
                            if (dats[i].itbx == 'T') {
                                t.push(dats[i].itbx_detail);
                            }
                            getTahapan(idmatriks, idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya, '', b, it, t);
                        }
                    }
                });
            } else {
                swal.fire({
                    type: 'error',
                    title: "Oops",
                    text: 'Cannot fetch data detail'
                });
            }
        }
    })
}

function modalOpenDetail(idPerizinan, title, othername, desc, jam, lok_kewenangan, kesesuaian_citata, ITBX, zona, biaya, tahapIzinNya, itb, it, t) {
    // debugger
    var content = '',
        ctTable = '',
        // jam = 'Informasi belum tersedia',
        textTitleWaktu = '';

    $.ajax({
        url: api_url + 'cronJobs/getPersyaratan?idIzin=' + idPerizinan,
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var tableTr = "";
            var no = 1;
            for (var i in data) {
                // nonew = (data[i].child.length > 0)?no:nochild;
                if (data[i].child.length > 0) {
                    nonya = "<td></td>";
                } else {
                    nonya = "<td>" + no + "</td>";
                }
                tableTr += "<tr>" + nonya + "<td>" + data[i].persyaratan;
                if (data[i].child.length > 0) {
                    tableTr += " : ";
                    for (var ch in data[i].child) {
                        var substrsymbol = data[i].child[ch].persyaratan.replace("?", "•");
                        var substrsymbolnew = substrsymbol.replace("#", "•");
                        tableTr += "<br>" + substrsymbolnew + "<br>";
                    }
                    // no = '';
                } else {
                    no += 1;
                }
                tableTr += "</td></tr>";
            }
            var contentTahapIzin = '',
                tahapBeluma = [];
            tahapBelums = [];
            // debugger;
            if (tahapIzinNya != 'anjing') {
                for (var i in tahapIzinNya) {
                    var datTahapIzin = tahapIzinNya[i];
                    if (datTahapIzin.status_tahap == 1) {
                        tahapBelums.push(datTahapIzin);
                    } else if (datTahapIzin.status_tahap == 0) {
                        tahapBeluma.push(datTahapIzin);
                    }

                }
            } else {
                for (var i in tahapIzinStat) {
                    var datTahapIzin = tahapIzinStat[i];
                    if (datTahapIzin.status_tahap == 1) {
                        tahapBelums.push(datTahapIzin);
                    } else if (datTahapIzin.status_tahap == 0) {
                        tahapBeluma.push(datTahapIzin);
                    }

                }
            }
            contentTahapIzin += '<div class="row mt-3"><div class="col-md-6 mb-4"><div class="isiTxtIzin mb-2">Izin tahap sebelumnya yg perlu dilengkapi</div><ul class="list-group list-group-flush">\n';
            // debugger;
            for (var xx in tahapBeluma) {
                if (tahapBeluma[xx].data.length > 0) {

                    contentTahapIzin += '  <button type="button" class="list-group-item list-group-item-action" onclick="detailTahapIzin(\'' + tahapBeluma[xx].data[0].kode_izin + '\',\'' + tahapBeluma[xx].data[0].nama_izin_terbit + '\',\'' + tahapBeluma[xx].data[0].deskripsi_izin + '\',\'' + tahapBeluma[xx].data[0].total_waktu + '\',\'' + tahapBeluma[xx].data[0].lokasi_kewenangan + '\',\'' + tahapBeluma[xx].data[0].kesesuaian_citata + '\',\'' + tahapBeluma[xx].data[0].biaya + '\',\'' + tahapBeluma[xx].data[0].idmatriks + '\',\'' + tahapBeluma[xx].data[0].id_tujuan_izin + '\')">' + tahapBeluma[xx].nama_izin_tahap + '</button>\n';
                } else {
                    contentTahapIzin += '  <button type="button" class="list-group-item list-group-item-action" >' + tahapBeluma[xx].nama_izin_tahap + '</button>\n';
                }
            }
            contentTahapIzin += '</ul></div>';

            contentTahapIzin += '<div class="col-md-6 mb-4"><div class="isiTxtIzin mb-2">Izin yang dapat dilakukan selanjutnya</div><ul class="list-group list-group-flush">\n';
            for (var x in tahapBelums) {
                if (tahapBelums[x].data.length > 0) {

                    contentTahapIzin += '  <button type="button" class="list-group-item list-group-item-action" onclick="detailTahapIzin(\'' + tahapBelums[x].data[0].kode_izin + '\',\'' + tahapBelums[x].data[0].nama_izin_terbit + '\',\'' + tahapBelums[x].data[0].deskripsi_izin + '\',\'' + tahapBelums[x].data[0].total_waktu + '\',\'' + tahapBelums[x].data[0].lokasi_kewenangan + '\',\'' + tahapBelums[x].data[0].kesesuaian_citata + '\',\'' + tahapBelums[x].data[0].biaya + '\',\'' + tahapBelums[x].data[0].idmatriks + '\',\'' + tahapBelums[x].data[0].id_tujuan_izin + '\')">' + tahapBelums[x].nama_izin_tahap + '</button>\n';
                } else {
                    contentTahapIzin += '  <button type="button" class="list-group-item list-group-item-action" >' + tahapBelums[x].nama_izin_tahap + '</button>\n';
                }
            }
            contentTahapIzin += '</ul></div></div>';

            if (othername != null || othername != '') {
                var imb = othername.split(' '),
                    imbfix = imb[0].toLowerCase();

                $('#imgAlurIzin').attr('src', 'assets/img/alur/' + imbfix + '/' + imbfix + '.png');
                $('#urlImgnya').attr('href', 'assets/img/alur/' + imbfix + '/' + imbfix + '.png');
            } else {
                $('#imgAlurIzin').html('<div>Gambar tidak tersedia.</div>');
            }
            // var imb = '',imbfix='',othername.split(' '), imbfix = imb[0].toLowerCase()
            var getLocalItem = JSON.parse(localStorage.getItem('dataReqPermission')),
                perubahanlt = getLocalItem[0].luas_tanah_renc - getLocalItem[0].luas_tanah;
            kondisi_lahan = getLocalItem[0].kondisi_lahan;
            hak_guna = getLocalItem[0].hak_guna;
            hak_guna_renc = getLocalItem[0].hak_guna_renc;
            fungsi = getLocalItem[0].fungsi;
            fungsi_renc = getLocalItem[0].fungsi_renc;
            jml_lantai = getLocalItem[0].jml_lantai;
            jml_lantai_renc = getLocalItem[0].jml_lantai_renc;
            jumlah_bangunan = getLocalItem[0].jumlah_bangunan;
            jumlah_bangunan_renc = getLocalItem[0].jumlah_bangunan_renc;
            luas_bangunan = getLocalItem[0].luas_bangunan;
            luas_bangunan_renc = getLocalItem[0].luas_bangunan_renc;
            kategori_fungsi = getLocalItem[0].kategori_fungsi;
            kategori_fungsi_renc = getLocalItem[0].kategori_fungsi_renc;
            perubahan = "";
            perubahan += '<p class="isiTxtIzin"><span>Terdapat Perubahan Luas Sebesar : ' + perubahanlt + 'm<sup>2</sup></span></p>';
            if (fungsi_renc != fungsi) {
                if (fungsi != undefined) {
                    perubahan += '<p class="isiTxtIzin"><span>Terdapat Perubahan Fungsi dari : ' + getRencanaFungsiText(fungsi) + ', menjadi ' + getRencanaFungsiText(fungsi_renc) + '</span></p>'
                }
            }
            if (jml_lantai_renc != jml_lantai) {
                if (jml_lantai != undefined) {
                    perubahan += '<p class="isiTxtIzin"><span>Terdapat Perubahan Jumlah Lantai dari : ' + (jml_lantai) + ' lantai menjadi ' + (jml_lantai_renc) + ' lantai</span></p>'
                }
            }
            if (luas_bangunan_renc != luas_bangunan) {
                if (luas_bangunan != undefined) {
                    perubahan += '<p class="isiTxtIzin"><span>Terdapat Perubahan Luas Bangunan dari : ' + (luas_bangunan) + 'm<sup>2</sup></span> menjadi ' + (luas_bangunan_renc) + 'm<sup>2</sup></span></span></p>'
                }
            }
            if (jumlah_bangunan != jumlah_bangunan_renc) {
                if (jumlah_bangunan != undefined) {
                    perubahan += '<p class="isiTxtIzin"><span>Terdapat Perubahan Jumlah Bangunan dari : ' + (jumlah_bangunan) + ' bangunan menjadi ' + (jumlah_bangunan_renc) + ' bangunan</span></p>'
                }
            }

            if (kategori_fungsi != kategori_fungsi_renc) {
                if (kategori_fungsi != undefined) {
                    perubahan += '<p class="isiTxtIzin"><span>Terdapat Perubahan Jenis Kegiatan dari : ' + getKategoriFungsi(kategori_fungsi) + ' bangunan menjadi ' + getKategoriFungsi(kategori_fungsi_renc) + ' bangunan</span></p>'
                }
            }
            if (hak_guna_renc != hak_guna) {
                if (hak_guna != undefined) {
                    perubahan += '<p class="isiTxtIzin"><span>Terdapat Perubahan Hak guna dari : ' + hak_guna + ', menjadi : ' + hak_guna_renc + '</span></p>';
                }
            }



            ctTable += tableTr;
            if (kesesuaian_citata == 'Y') {
                kesCitata = "Sesuai";
            } else {
                kesCitata = "Tidak Sesuai";
            }

            var bangunanizin = '';
            var bangunanB = 'Bangunan Bersyarat : ';
            var bangunanI = 'Bangunan Diizinkan : ';
            var bangunanT = 'Bangunan Terbatas Bersyarat : ';

            for (var i in itb) {
                bangunanB += itb[i] + ',';

            }
            bangunanB.substr(0, bangunanB.length - 1);

            for (var i in it) {
                bangunanI += it[i] + ',';

            }
            bangunanI.substr(0, bangunanI.length - 1);

            for (var i in t) {
                bangunanT += t[i] + ',';

            }
            bangunanT.substr(0, bangunanT.length - 1);

            if (t.length < 1) {
                bangunanT += " - ";
            }
            if (itb.length < 1) {
                bangunanB += " - ";
            }
            if (it.length < 1) {
                bangunanI += " - ";
            }
            bangunanizin += bangunanI + "<br>";
            bangunanizin += bangunanT + "<br>";
            bangunanizin += bangunanB + "<br>";


            content += '<div class="row">\n' +
                '    <div class="col-md-12">\n' +
                '        <div class="accordion" id="accordionExample">\n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingOne">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">\n' +
                '                            Deskripsi Izin\n' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseOne" class="collapse printAct" aria-labelledby="headingOne" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center border\n' +
                '                                p-2">\n' +
                '                                   <p class="isiTxtIzin"><span>' + desc + '</span></p>' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            \n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingTwo">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">\n' +
                '                            Lokasi Pengurusan Izin\n' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseTwo" class="collapse printAct" aria-labelledby="headingTwo" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center border\n' +
                '                                p-2">\n' +
                '                                   <p class="isiTxtIzin"><span>Terdapat di ' + lok_kewenangan + '</span></p>' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            \n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingThree">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">\n' +
                '                            Persyaratan\n' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseThree" class="collapse printAct" aria-labelledby="headingThree" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center">\n' +
                '<table class="table table-striped table-bordered text-left" id="dataTabelPrint" border="1" cellpadding="3">' +
                '<thead>' +
                '<tr>' +
                '<th scope="col">No.</th>\n' +
                '<th scope="col">Persyaratan</th>\n' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                ctTable +
                '</tbody>' +
                '</table>' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            \n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingFour">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">\n' +
                '                            Perubahan Fungsi/Rencana\n' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseFour" class="collapse printAct" aria-labelledby="headingFour" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center border\n' +
                '                                p-2">\n' +
                perubahan +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            \n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingSix">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">\n' +
                '                            Kegiatan dalam ITBX yang sesuai dengan "' + title + '"' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseSix" class="collapse printAct" aria-labelledby="headingSix" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center border\n' +
                '                                p-2">\n' +
                '                                   <div class="isiTxtIzin"><span>Perizinan berdasarkan Zona : ' + zona + '</span>' +
                // <span class="d-block">'+ITBX+'</span>
                '<div><span id="listDetailITBX">' + bangunanizin + '</span></div></div>' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingEight">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">\n' +
                '                            Tahapan Izin\n' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseEight" class="collapse printAct" aria-labelledby="headingEight" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center border p-2">\n' +
                '                                   <div class="mb-2 isiTxtIzin">Alur Proses Buat Perizinan</div>' +
                '                                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imgTahapIzin">Lihat gambar</button>' +
                contentTahapIzin +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            \n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingNine">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">\n' +
                '                            Durasi & Biaya\n' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseNine" class="collapse printAct" aria-labelledby="headingNine" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center border\n' +
                '                                p-2">\n' +
                '                                   <p class="isiTxtIzin"><span>Waktu Penyelesaian : ' + jam + '</span><span class="d-block">Biaya yang diperlukan : ' + biaya + '</span></p>' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            \n' +
                '            <div class="card">\n' +
                '                <div class="card-header" id="headingTen">\n' +
                '                    <h2 class="mb-0">\n' +
                '                        <button class="btn btn-link font-weight-bold text-dark" style="letter-spacing:1px;" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="true" aria-controls="collapseTen">\n' +
                '                            Kontak Kami\n' +
                '                        </button>\n' +
                '                    </h2>\n' +
                '                </div>\n' +
                '                <div id="collapseTen" class="collapse printAct" aria-labelledby="headingTen" data-parent="#accordionExample">\n' +
                '                    <div class="card-body">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-12">\n' +
                '                                <div class="text-center border\n' +
                '                                p-2">\n' +
                '                                   <p class="isiTxtIzin"><span>SEGERA HADIR</span></p>' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>';



            $('#izinContents').html(content);
            $('#izinTitles').html(title);

            // chartIzin(title);
            $('#modalDetailIzin').modal('show');
        }
    });
}

function clickKeprofesian(id) {
    var izinChecked = $('.selIzinNew').map(function () {
            if (this.checked)
                return this.value;
        }).get(),
        dataReq = JSON.parse(localStorage.getItem('dataReqPermission'));

    dataReq[0].izinData = izinChecked;
    localStorage.setItem("dataReqPermission", JSON.stringify(dataReq));
}

function getAjaxCustom(token) {
    $.ajax({
        url: api_url + 'users/cekPermohonan?token=' + token,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            // var izinStat = "";
            // if (data.row[0].status_kepengurusan == 1){
            //     izinStat = "Mengurus Sendiri";
            // } else{
            //     izinStat = "Tidak Mengurus Sendiri";
            // }

            // localStorage.setItem('idPermohonan', data.row[0].id);

            // $('#reqName').html(data.row[0].nama_pemohon);
            // $('#reqToken').html(data.row[0].token);
            // $('#reqId').html(data.row[0].no_identitas);
            // $('#reqEmail').html(data.row[0].email);
            // $('#reqCreate').html(data.row[0].create_date);
            // $('#reqStat').html(izinStat);
        }
    })
}

function chartIzin(izin) {
    $.ajax({
        url: api_url + 'CronJobs/cekTahapan?typeizin=1&nama=' + izin,
        type: 'GET',
        dataType: 'json',
        beforeSend: function () {},
        success: function (data) {
            console.log(data);
            if (data.length < 1) {
                $("#tahapanIzinGraph").html("<p class='isiTxtIzin'><span>Tidak ada tahapan izin yang harus diselesaikan sebelumnya</span></p>");
            } else {
                $("#keteranganTahapIzin").html("<p class='isiTxtIzin'><span>Warna Hijau : izin yang harus diselesaikan terlebih dahulu</span></p>");
            }
            // genchart(data);

            // $("#tahapIzinUL").append("<li>"+data[0].tahapanfinal+"</li>");
            // var ht = "<li>Izin yang harus diselesaikan sebelumnya <ul>";
            // for(var i in data[0].tahapanul){
            //     const el = data[0].tahapanul[i];
            //     ht += "<li>"+el+"</li>";
            // }
            // ht += "</ul></li>";
            // $("#tahapIzinUL").append(ht);
        }
    })
}

function genchart(data) {
    var pushNodes = [{
        id: data[0].tahapanfinal,
        title: data[0].tahapanfinal,
        name: data[0].tahapanfinal,
        // image: 'https://wp-assets.highcharts.com/www-highcharts-com/blog/wp-content/uploads/2018/11/12132317/Grethe.jpg'
    }];

    for (var i in data[0].tahapanul) {
        const el = data[0].tahapanul[i];

        var obj = {};
        obj['id'] = el;
        obj['name'] = el;
        obj['title'] = el;
        obj['layout'] = 'hanging';
        obj['column'] = data[0].tahapanul.length;
        pushNodes.push(obj);
    }
    Highcharts.chart('tahapanIzinGraph', {

        chart: {
            height: 300,
            inverted: true
        },

        title: {
            text: 'Tahapan ' + data[0].tahapanfinal
        },

        series: [{
            type: 'organization',
            name: 'Tahapan Izin',
            keys: ['from', 'to'],
            data: data[0].tahapannew,
            levels: [{
                level: 0,
                color: '#980104',
                dataLabels: {
                    //   color: 'black'
                    fontSize: 12,
                },
                height: 25
            }, {
                level: 1,
                color: '#359154',
                dataLabels: {
                    fontSize: 12,
                    //   color: 'black'
                },
                height: 25
            }, {
                level: 2,
                color: '#980104'
            }, {
                level: 4,
                color: '#359154'
            }],
            nodes: [pushNodes],
            colorByPoint: false,
            color: '#007ad0',
            dataLabels: {
                fontSize: 12,
                color: 'white'
            },
            borderColor: 'white',
            nodeWidth: 65
        }],
        tooltip: {
            outside: false
        },
        exporting: {
            allowHTML: true,
            sourceWidth: 800,
            sourceHeight: 600
        }

    });
}

function print() {
    printJS({
        printable: 'izinContents',
        type: 'html',
        targetStyles: ['*']
    })
}