var api_url = 'http://perizinan.jakarta.go.id/api/rest-api/',
// var api_url = 'http://localhost/rest-api/',
// var api_url = 'http://192.168.100.7/rest-api/',
    full_base_name = 'http://rest-api.pkkmart.com/';

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

var loader = "<div class='text-center'><div class='lds-ellipsis'>" +
    "<div></div>" +
    "<div></div>" +
    "<div></div>" +
    "<div></div>" +
    "</div></div>";
var btnCheck = $('#onCheckToken'),
    persyaratanUmum = [],
    bkprd = '';

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    const $goLogin = document.querySelector("#go-login"),
        $goRegister = document.querySelector("#go-register"),
        $container = document.querySelector(".container"),
        $overlayContainer = document.querySelector(".overlay-container");

    _toggleForm = () => {
        if ($container.classList.contains("go-register")) {
            $container.classList.remove("go-register");
            $container.classList.add("go-login");
            $overlayContainer.classList.add("animateWidth");
            $overlayContainer.addEventListener("webkitTransitionEnd", () =>
                $overlayContainer.classList.remove("animateWidth")
            );
        } else {
            $container.classList.remove("go-login");
            $container.classList.add("go-register");
            $overlayContainer.classList.add("animateWidth");
            $overlayContainer.addEventListener("webkitTransitionEnd", () =>
                $overlayContainer.classList.remove("animateWidth")
            );
        }
    };

    $goLogin.addEventListener("click", _toggleForm);
    $goRegister.addEventListener("click", _toggleForm);
});