$(document).ready(function(){

    $('#btnConfSumm').on('click', function () {
        var type = localStorage.getItem('typeStart'),
            dataUser = JSON.parse(localStorage.getItem('dataReqPermission')),
            dataArahan = JSON.parse(localStorage.getItem('dataArahan')),
            idarahanSpasial =[],
            datas = dataArahan,
            npwp = dataUser[0].no_identitas,
            token = localStorage.getItem('newToken');
            contents = '',
            recList = '',
            idPemohon = dataUser[0].idPemohon,
            arahancard = $('#arahanRumus'),
            columncard = $('#cardRec'),
            columnlist = $('#recList'),
            luasTanah = parseFloat(dataUser[0].luas_tanah),
            klbPeta = parseFloat(localStorage.getItem('klb')),
            kdbPeta = parseFloat(localStorage.getItem('kdb')),
            finalContent = "";
        localStorage.setItem("prevStep", "ninthStep");

        dataArahan.map(function(el,i){
            idarahanSpasial.push(el.arahan);
            idarahanSpasial.join();
        })

        //Hitung Rumus
        // KDB = Luas Tanah x KDB yang ditentukan (%)
        var kdb = luasTanah * (kdbPeta / 100);

        // KLB = Luas Tanah x KLB yang ditentukan
        var klb = luasTanah * klbPeta;

        // Jml Lantai
        // Di Ambil Angka depannya saja, walapun 4,9 tetap di ambil 4
        var jmlLantai = klb / kdb;
        jmlLantai = jmlLantai.toString();
        splitLantai = jmlLantai.split(".");

        if(splitLantai.length>0){
            jmlLantai = splitLantai[0];
        }
        var ctArahan = "Dengan Luas Tanah <b>" + luasTanah + "m</b> pada lokasi yang dipilih hanya bisa maksimal melakukan pembangunan gedung <b>" +  jmlLantai + "</b> lantai.";

        if(isNaN(jmlLantai)){
            ctArahan = "Tidak dapat membangun bangunan pada lokasi yang dipilih";
        }
        

        arahancard.html(ctArahan);
        // if (type == 'buat_izin') {
            // finalContent += "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email "+ dataUser[0].email +"</div>" +
            //     "<button type='button' class='btn rounded-0 btn--main' onclick='directLogin();'>Selesai</button>";
            // $.ajax({
            //     url: api_url + 'matriksCtrl/savePermohonan',
            //     type: 'POST',
            //     dataType: 'json',
            //     data: {dataRegist: localStorage.getItem('dataReqPermission'),idArahan:localStorage.getItem('idArahan')},
            //     beforeSend:function() {
            //         Swal.showLoading();
            //     },
            //     success:function(data) {
            //         $('#backPemohon').show();
            //         finalContent += "<div>Informasi data permohonan dan data login sudah berhasil dikirim ke email "+ dataUser[0].email +"</div>" +
            //             "<input type='button' class='previous action-button-previous' value='Batalkan'/>" +
            //             "<button type='button' class='btn rounded-0 btn--main' onclick='directLogin();'>Selesai</button>";
            //         sendEmail(data.token, data.npwp);
            //     }
            // });
        //     localStorage.setItem("step", "twelfthStep");
        //     customNextStep($('#ninthStep'), $('#twelfthStep'));
        // }else{
            $('#backPemohon').hide();
            var dataArahanSpasial = [];
            $.ajax({
                url: api_url + 'MatriksCtrl/spasialCek',
                method: 'GET',
                dataType: 'json',
                data: {idarahan: idarahanSpasial,subzona:localStorage.getItem('subzona')},
                success:function(data){
                    dataArahanSpasial.push(data);
                    var labelITBX = "Data Spasial Tidak Ada", classITBX;
                    if (datas.length == '1'){

                        localStorage.setItem("arahan", datas[0].arahan);
                        localStorage.setItem("idArahan", datas[0].id);
                        if(dataArahanSpasial[0][0].nama_lain_perizinan==datas[0].arahan){
                            if(dataArahanSpasial[0][0].status_itbx=="X"){
                                classITBX = "text-decoration:line-through";
                            }

                            labelITBX = "<label class='label label-default'>"+dataArahanSpasial[0][0].status_itbx+"</label>\n"

                        }
                        recList += "<div class='col-md-12'>\n" +labelITBX +
                            "                            <div data-arahan='"+datas[0].arahan+"' style='cursor: pointer;'>\n" +
                            "                                <span style='"+classITBX+"' class='d-block text-dark font-weight-bold h2 mb-0' id='imbTitle'>"+ datas[0].arahan +"</span>\n" +
                            "                            </div>\n" +
                            "                        </div>";

                        var ar = datas[0].arahan.replace(/\s/g, '');
                        contents += "<div class=\"card\">" +
                            "<div class=\"card-header\" id=\"heading"+ar+"\">\n" +
                            "<h2 class=\"mb-0\">\n" +
                            "<button class=\"btn permissionCheck\" data-arahan='"+datas[0].arahan+"' type=\"button\" data-toggle=\"collapse\" data-target='#"+ar+"' aria-expanded=\"true\" aria-controls=\"collapseOne\">Daftar Persyaratan Untuk<span class='d-block font-weight-bold font-size-lg'>"+datas[0].arahan+"</span></button>\n" +
                            "</h2>\n" +
                            "</div>\n" +
                            "<div id='"+ar+"' class=\"collapse\" aria-labelledby=\"headingOne"+ar+"\" data-parent=\"#cardRec\">\n" +
                            "<div class=\"card-body contentPersyaratan\">\n" +
                            "</div>\n" +
                            "</div>\n" +
                            "</div>";
                        $.ajax({
                            url: api_url + 'users/attachPdf',
                            type: 'GET',
                            dataType: 'json',
                            data:{idarahan:datas[0].id},
                            beforeSend:function() {
                                // Swal.showLoading();
                            },
                            success:function(data) {
                                // console.log(data);
                            }
                        });

                    }else{
                        var idarahanArray = [];
                        $.each(datas, function (i, item) {
                            idarahanArray.push(item.id);
                            if(dataArahanSpasial[0][i]!=null){
                                
                            if(dataArahanSpasial[0][i].nama_lain_perizinan==datas[i].arahan){
                                if(dataArahanSpasial[0][i].status_itbx=="X"){
                                    classITBX = "text-decoration:line-through";
                                }
            
                                labelITBX = "<label class='label label-default'>"+dataArahanSpasial[0][i].status_itbx+"</label>\n"
            
                            }

                            }
                            recList += "\n" +
                                "                        <div class='col-md-6'>\n"  +labelITBX +
                                "                            <div data-arahan='"+item.arahan+"' style='cursor: pointer;'>\n" +
                                "                                <span style='"+classITBX+"' class='d-block text-dark font-weight-bold h2 mb-0' id='imbTitle'>"+ item.arahan +"</span>\n" +
                                "                            </div>" +
                                "                        </div>";

                            var ar = item.arahan.replace(/\s/g, '');
                            contents += "<div class=\"card\">" +
                                "<div class=\"card-header\" id=\"heading"+ar+"\">\n" +
                                "<h2 class=\"mb-0\">\n" +
                                "<button class=\"btn permissionCheck\" data-arahan='"+item.arahan+"' type=\"button\" data-toggle=\"collapse\" data-target='#"+ar+"' aria-expanded=\"true\" aria-controls=\"collapseOne\">Daftar Persyaratan Untuk<span class='d-block font-weight-bold font-size-lg'>"+item.arahan+"</span></button>\n" +
                                "</h2>\n" +
                                "</div>\n" +
                                "<div id='"+ar+"' class=\"collapse\" aria-labelledby=\"headingOne"+ar+"\" data-parent=\"#cardRec\">\n" +
                                "<div class=\"card-body contentPersyaratan\">\n" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>";
                        });
                        var idarahanJoin = idarahanArray.join();
                        localStorage.setItem("idArahan", idarahanJoin);

                        var splitArahan = idarahanJoin.split(',');

                        for(var i = 0; i < splitArahan.length; i++)
                        {
                            $.ajax({
                                url: api_url + 'users/attachPdf',
                                type: 'GET',
                                dataType: 'json',
                                data:{idarahan:splitArahan[i]},
                                beforeSend:function() {
                                    // Swal.showLoading();
                                },
                                success:function(data) {
                                    // console.log(data);
                                }
                            });
                        }
                    }

                    columncard.html(contents);
                    columnlist.html(recList);


                    $(".permissionCheck").click(function(e){
                        e.preventDefault();
                        var arahan = $(this).data('arahan');
                        checkPersyaratan(arahan);
                    });
                }
            });

            finalContent += "<div>Apakah anda ingin melanjutkan permohonan izin ini?</div>" +
                "<input type='button' class='previous action-button-previous resetDataLocal' value='Batalkan'/>" ;
            if(type=="buat_izin"){
                finalContent += "<button type='button' class='btn rounded-0 btn--main' onclick='savePermohonanVerifikasi();'>Lanjutkan</button>";
            }else{

                finalContent += "<button type='button' class='btn rounded-0 btn--main' onclick='directPemohon();'>Lanjutkan</button>";
            }

            localStorage.setItem("step", "tenthStep");
            customNextStep($('#ninthStep'), $('#tenthStep'));
        // }
        $('#finalContent').html(finalContent);
    });
})